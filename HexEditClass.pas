unit HexEditClass;

interface

uses
  CustomDataEditClass, FunctionsClass, ResourcesClass,
  Classes, Controls, Forms, StdCtrls;

type

  { THexEdit }

  THexEdit = class(TCustomDataEdit)
  private
    pChange, pCancel: TButton;
    pHexEdit: TEdit;
    pHexLabel: TLabel;
    pData: Pointer;
    pSize: Integer;
    procedure pHexChange(Sender: TObject); virtual;
    procedure pOnShow(Sender: TObject); virtual;
  public
    constructor Create(AOwner: TComponent); override;
    function ShowBox(Input: Pointer; Size: Integer): Boolean; override;
  end;

implementation

// ************************************************************************** //
// *                        THexEdit implementation                         * //
// ************************************************************************** //

  procedure THexEdit.pHexChange(Sender: TObject);
  begin
    CustomHexToData(pHexEdit.Text, pData, pSize);
  end;

  procedure THexEdit.pOnShow(Sender: TObject);
  begin
    pHexEdit.Text := CustomDataToHex(pData, pSize);
    pHexEdit.SelectAll;
    pHexEdit.SetFocus;
  end;

  constructor THexEdit.Create(AOwner: TComponent);
  begin
    inherited Create(AOwner);
    ClientWidth := 231;
    ClientHeight := 65;
    BorderStyle := bsDialog;
    OnShow := pOnShow;
    pChange := TButton.Create(Self);
    with pChange do
    begin
      Parent := Self;
      Caption := FL_EDIT_OK;
      Left := 77;
      Top := 41;
      Width := 72;
      Height := 20;
      ModalResult := mrOk;
      Default := True;
    end;
    pCancel := TButton.Create(Self);
    with pCancel do
    begin
      Parent := Self;
      Caption := FL_EDIT_CANCEL;
      Left := 153;
      Top := 41;
      Width := 72;
      Height := 20;
      ModalResult := mrCancel;
      Cancel := True;
    end;
    pHexEdit := TEdit.Create(Self);
    with pHexEdit do
    begin
      Parent := Self;
      Left := 41;
      Top := 8;
      Width := 186;
      Height := 8;
      OnChange := pHexChange;
    end;
    pHexLabel := TLabel.Create(Self);
    with pHexLabel do
    begin
      Parent := Self;
      Caption := FL_EDIT_HEX;
      Left := 9;
      Top := 11;
      Width := 309;
      Height := 21;
    end;
  end;

  function THexEdit.ShowBox(Input: Pointer; Size: Integer): Boolean;
  begin
    pData := Input;
    pSize := Size;
    Result := (ShowModal = mrOk);
  end;

end.

