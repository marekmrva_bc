unit CustomViewClass;

interface

uses
  ConstantsClass, CustomStringGridClass, HardwareClass, ResourcesClass,
  TypesClass,
  Classes, Controls, Graphics, Grids, StdCtrls, Types;

type

  { TCustomView }

  TCustomView = class(TCustomStringGrid)
  private
    pChanges: array of array of Boolean;
    pColorTheme: TViewTheme;
    pHardware: THardware;
    pOldOnOperand, pOldOnState, pOnOperand, pOnState: TChangeEvent;
    function pGetChanges(ACol, ARow: Integer): Boolean; virtual;
    function pGetColCount: Longint; virtual;
    function pGetRowCount: Longint; virtual;
    procedure pCustomDblClick(Sender: TObject); virtual;
    procedure pCustomDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState); virtual;
    procedure pCustomMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer); virtual;
    procedure pDrawSingleCell(Canvas: TCanvas; ACol, ARow: Integer; Rect: TRect; Colors: TColors); virtual;
    procedure pOnOperandProc(Sender: TObject); virtual;
    procedure pOnSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean); virtual;
    procedure pOnStateProc(Sender: TObject); virtual;
    procedure pSetChanges(ACol, ARow: Integer; Change: Boolean); virtual;
    procedure pSetColorTheme(ColorTheme: TViewTheme); virtual;
    procedure pSetColCount(Count: Integer); virtual;
    procedure pSetHardware(Input: THardware); virtual;
    procedure pSetRowCount(Count: Integer); virtual;
  public
    constructor CreateView(AOwner: TComponent; Hw: THardware); virtual;
    destructor Destroy; override;
    property Changes[ACol, ARow: Integer]: Boolean read pGetChanges write pSetChanges;
    property ColorTheme: TViewTheme read pColorTheme write pSetColorTheme;
    property ColCount: Longint read pGetColCount write pSetColCount;
    property Hardware: THardware read pHardware write pSetHardware;
    property OnOperand: TChangeEvent read pOnOperand write pOnOperand;
    property OnState: TChangeEvent read pOnState write pOnState;
    property RowCount: Longint read pGetRowCount write pSetRowCount;
  end;

implementation

// ************************************************************************** //
// *                       TCustomView implementation                       * //
// ************************************************************************** //

  function TCustomView.pGetChanges(ACol, ARow: Integer): Boolean;
  begin
    Result := False;
    if (ACol < 0) or not(ACol < ColCount) then Exit;
    if (ARow < 0) or not(ARow < RowCount) then Exit;
    Result := pChanges[ACol, ARow];
  end;

  function TCustomView.pGetColCount: Longint;
  begin
    Result := inherited ColCount;
  end;

  function TCustomView.pGetRowCount: Longint;
  begin
    Result := inherited RowCount;
  end;

  procedure TCustomView.pCustomDblClick(Sender: TObject);
  var
    lkey: Word;
  begin
    lkey := KEY_RETURN;
    OnKeyDown(Self, lkey, []);
  end;

  procedure TCustomView.pCustomDrawCell(Sender: TObject; ACol, ARow: Integer;
    Rect: TRect; State: TGridDrawState);
  begin
    if (gdSelected in State) and Selected then
      pDrawSingleCell(Canvas, ACol, ARow, Rect, ColorTheme.Line.Selected)
    else
      pDrawSingleCell(Canvas, ACol, ARow, Rect, ColorTheme.Line.None)
  end;

  procedure TCustomView.pCustomMouseDown(Sender: TObject; Button: TMouseButton;
    Shift: TShiftState; X, Y: Integer);
  var
    lcol, lrow: Integer;
    lselect: TGridRect;
  begin
    MouseToCell(X, Y, lcol, lrow);
    if SelectCell(lcol, lrow) then
    {$IFDEF UNIX}
      SetColRow(1, 0);
    // Lazarus is really nice, by having incompatible calls :-(
    {$ELSE}
    begin
      lselect.Top := lrow;
      lselect.Bottom := lrow;
      lselect.Left := lcol;
      lselect.Right := lcol;
      Selection := lselect;
    end;
    {$ENDIF}
  end;

  procedure TCustomView.pDrawSingleCell(Canvas: TCanvas; ACol, ARow: Integer;
    Rect: TRect; Colors: TColors);
  var
    lsize: TSize;
    lx, ly: Integer;
  begin
    with Rect do
    begin
      Canvas.Brush.Color := Colors.BG;
      if Changes[ACol, ARow] then Canvas.Font.Color := ColorTheme.Change
      else Canvas.Font.Color := Colors.FG;
      lsize := Canvas.TextExtent(Cells[ACol, ARow]);
      lx := (Right - Left - lsize.cx) div 2 + Left;
      ly := (Bottom - Top - lsize.cy) div 2 + Top;
      Canvas.FillRect(Rect);
      Canvas.TextOut(lx, ly, Cells[ACol, ARow]);
    end;
  end;

  procedure TCustomView.pOnOperandProc(Sender: TObject);
  begin
    if not(@pOnOperand = nil) then pOnOperand(Sender);
    Repaint;
    if not(@pOldOnOperand = nil) then pOldOnOperand(Sender);
  end;

  procedure TCustomView.pOnSelectCell(Sender: TObject; ACol, ARow: Integer;
    var CanSelect: Boolean);
  begin
    CanSelect := not(ACol = 0);
  end;

  procedure TCustomView.pOnStateProc(Sender: TObject);
  begin
    if not(@pOnState = nil) then pOnState(Sender);
    Repaint;
    if not(@pOldOnState = nil) then pOldOnState(Sender);
  end;

  procedure TCustomView.pSetChanges(ACol, ARow: Integer; Change: Boolean);
  begin
    if (ACol < 0) or not(ACol < ColCount) then Exit;
    if (ARow < 0) or not(ARow < RowCount) then Exit;
    pChanges[ACol, ARow] := Change;
  end;

  procedure TCustomView.pSetColorTheme(ColorTheme: TViewTheme);
  begin
    pColorTheme := ColorTheme;
    Color := ColorTheme.Line.None.BG;
  end;

  procedure TCustomView.pSetColCount(Count: Integer);
  var
    i: Integer;
  begin
    inherited ColCount := Count;
    SetLength(pChanges, inherited ColCount);
    for i := 0 to (ColCount - 1) do
        SetLength(pChanges[i], inherited RowCount);
  end;

  procedure TCustomView.pSetHardware(Input: THardware);
  begin
    if not(pHardware = nil) then pHardware.OnState := pOldOnState;
    pHardware := Input;
    if not(Input = nil) then
    begin
      pOldOnOperand := Input.OnOperand;
      pOldOnState := Input.OnState;
      Input.OnOperand := pOnOperandProc;
      Input.OnState := pOnStateProc;
    end;
  end;

  procedure TCustomView.pSetRowCount(Count: Integer);
  var
    i: Integer;
  begin
    inherited RowCount := Count;
    SetLength(pChanges, inherited ColCount);
    for i := 0 to (ColCount - 1) do
        SetLength(pChanges[i], inherited RowCount);
  end;

  constructor TCustomView.CreateView(AOwner: TComponent; Hw: THardware);
  begin
    inherited Create(AOwner);
    ColorTheme := THEME_VIEW_DEFAULT;
    RowCount := RowCount;
    ScrollBars := ssNone;
    {$IFDEF UNIX}
    Options := [goRangeSelect, goThumbTracking];
    // Behavior of goRangeSelect differ in Lazarus :-(
    {$ELSE}
    Options := [goThumbTracking];
    {$ENDIF}
    DefaultDrawing := False;
    OnDrawCell := pCustomDrawCell;
    OnDblClick := pCustomDblClick;
    OnMouseDown := pCustomMouseDown;
    OnSelectCell := pOnSelectCell;
    Hardware := Hw;
  end;

  destructor TCustomView.Destroy;
  begin
    Hardware := nil;
    inherited Destroy;
  end;

end.
