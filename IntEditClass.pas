unit IntEditClass;

interface

uses
  CustomDataEditClass, FunctionsClass, ResourcesClass,
  Classes, Controls, Forms, Graphics, StdCtrls;

type

  { TIntEdit }

  TIntEdit = class(TCustomDataEdit)
  private
    pChange, pCancel: TButton;
    pHexEdit, pSIntEdit, pUIntEdit: TEdit;
    pHexLabel, pSIntLabel, pUIntLabel: TLabel;
    pInt: Pointer;
    pSize: Integer;
    pUserChange: Boolean;
    procedure pHexChange(Sender: TObject); virtual;
    procedure pSIntChange(Sender: TObject); virtual;
    procedure pUIntChange(Sender: TObject); virtual;
    procedure pOnShow(Sender: TObject); virtual;
  public
    constructor Create(AOwner: TComponent); override;
    function ShowBox(Input: Pointer; Size: Integer): Boolean; override;
  end;

implementation

// ************************************************************************** //
// *                        TIntEdit implementation                         * //
// ************************************************************************** //

  procedure TIntEdit.pHexChange(Sender: TObject);
  begin
    if not pUserChange then Exit;
    pUserChange := False;
    CustomHexToData(pHexEdit.Text, pInt, pSize);
    pSIntEdit.Text := CustomSIntToStr(pInt, pSize);
    pUIntEdit.Text := CustomUIntToStr(pInt, pSize);
    pUserChange := True;
  end;

  procedure TIntEdit.pSIntChange(Sender: TObject);
  begin
    if not pUserChange then Exit;
    pUserChange := False;
    CustomStrToSInt(pSIntEdit.Text, pInt, pSize);
    pHexEdit.Text := CustomDataToHex(pInt, pSize);
    pUIntEdit.Text := CustomUIntToStr(pInt, pSize);
    pUserChange := True;
  end;

  procedure TIntEdit.pUIntChange(Sender: TObject);
  begin
    if not pUserChange then Exit;
    pUserChange := False;
    CustomStrToUInt(pUIntEdit.Text, pInt, pSize);
    pHexEdit.Text := CustomDataToHex(pInt, pSize);
    pSIntEdit.Text := CustomSIntToStr(pInt, pSize);
    pUserChange := True;
  end;

  procedure TIntEdit.pOnShow(Sender: TObject);
  begin
    pUserChange := False;
    pHexEdit.Text := CustomDataToHex(pInt, pSize);
    pSIntEdit.Text := CustomSIntToStr(pInt, pSize);
    pUIntEdit.Text := CustomUIntToStr(pInt, pSize);
    pSIntEdit.SelectAll;
    pSIntEdit.SetFocus;
    pUserChange := True;
  end;

  constructor TIntEdit.Create(AOwner: TComponent);
  begin
    inherited Create(AOwner);
    ClientWidth := 221;
    ClientHeight := 110;
    BorderStyle := bsDialog;
    OnShow := pOnShow;
    pSIntEdit := TEdit.Create(Self);
    with pSIntEdit do
    begin
      Parent := Self;
      Left := 60;
      Top := 7;
      Width := 157;
      Height := 20;
      OnChange := pSIntChange;
    end;
    pUIntEdit := TEdit.Create(Self);
    with pUIntEdit do
    begin
      Parent := Self;
      Left := 60;
      Top := 31;
      Width := 157;
      Height := 20;
      OnChange := pUIntChange;
    end;
    pHexEdit := TEdit.Create(Self);
    with pHexEdit do
    begin
      Parent := Self;
      Left := 60;
      Top := 55;
      Width := 157;
      Height := 20;
      OnChange := pHexChange;
    end;
    pChange := TButton.Create(Self);
    with pChange do
    begin
      Parent := Self;
      Caption := IN_EDIT_OK;
      Left := 67;
      Top := 85;
      Width := 72;
      Height := 20;
      ModalResult := mrOk;
      Default := True;
    end;
    pCancel := TButton.Create(Self);
    with pCancel do
    begin
      Parent := Self;
      Caption := IN_EDIT_CANCEL;
      Left := 143;
      Top := 85;
      Width := 72;
      Height := 20;
      ModalResult := mrCancel;
      Cancel := True;
    end;
    pHexLabel := TLabel.Create(Self);
    with pHexLabel do
    begin
      Parent := Self;
      Caption := IN_EDIT_HEX;
      Left := 9;
      Top := 58;
      Width := 309;
      Height := 21;
    end;
    pSIntLabel := TLabel.Create(Self);
    with pSIntLabel do
    begin
      Parent := Self;
      Caption := IN_EDIT_INT_S;
      Left := 9;
      Top := 10;
      Width := 309;
      Height := 21;
    end;
    pUIntLabel := TLabel.Create(Self);
    with pUIntLabel do
    begin
      Parent := Self;
      Caption := IN_EDIT_INT_U;
      Left := 9;
      Top := 34;
      Width := 309;
      Height := 21;
    end;
    pUserChange := True;
  end;

  function TIntEdit.ShowBox(Input: Pointer; Size: Integer): Boolean;
  begin
    Result := False;
    if not(Size in [1, 2, 4, 8]) then Exit;
    pInt := Input;
    pSize := Size;
    if (ShowModal = mrOk) then Result := True;
  end;

end.
