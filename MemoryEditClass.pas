unit MemoryEditClass;

interface

uses
  BCDEditClass, ConstantsClass, CustomDataEditClass, FloatEditClass,
  FunctionsClass, HexEditClass, IntEditClass, TypesClass,
  Classes;

type

  { TMemoryEdit }

  TMemoryEdit = class
  private
    pEdit: TCustomDataEdit;
    pOperand: POperandRecord;
    function pGetName: String; virtual;
    function pGetValue: String; virtual;
  public
    constructor Create(AOwner: TComponent; Memory: POperandRecord); virtual;
    procedure Change; virtual;
    property Edit: TCustomDataEdit read pEdit write pEdit;
    property Name: String read pGetName;
    property Operand: POperandRecord read pOperand write pOperand;
    property Value: String read pGetValue;
  end;

implementation

// ************************************************************************** //
// *                       TMemoryEdit implementation                       * //
// ************************************************************************** //

  function TMemoryEdit.pGetName: String;
  begin
    Result := pOperand^.Name;
  end;

  function TMemoryEdit.pGetValue: String;
  begin
    with pOperand^ do
      case OperandType of
        FPU_OPERAND_M16_INT: Result := CustomSIntToStr(Data, Length(Default));
        FPU_OPERAND_M32_INT: Result := CustomSIntToStr(Data, Length(Default));
        FPU_OPERAND_M64_INT: Result := CustomSIntToStr(Data, Length(Default));
        FPU_OPERAND_M80_BCD: Result := CustomBCDToStr(Data, Length(Default));
        FPU_OPERAND_M32_FP: Result := CustomFloatToStr(Data, Length(Default));
        FPU_OPERAND_M64_FP: Result := CustomFloatToStr(Data, Length(Default));
        FPU_OPERAND_M80_FP: Result := CustomFloatToStr(Data, Length(Default));
        else Result := '';
      end;
  end;

  constructor TMemoryEdit.Create(AOwner: TComponent; Memory: POperandRecord);
  begin
    pOperand := Memory;
    case pOperand^.OperandType of
      FPU_OPERAND_M16_INT: pEdit := TIntEdit.Create(AOwner);
      FPU_OPERAND_M32_INT: pEdit := TIntEdit.Create(AOwner);
      FPU_OPERAND_M64_INT: pEdit := TIntEdit.Create(AOwner);
      FPU_OPERAND_M80_BCD: pEdit := TBCDEdit.Create(AOwner);
      FPU_OPERAND_M32_FP: pEdit := TFLoatEdit.Create(AOwner);
      FPU_OPERAND_M64_FP: pEdit := TFLoatEdit.Create(AOwner);
      FPU_OPERAND_M80_FP: pEdit := TFLoatEdit.Create(AOwner);
      else pEdit := nil;
    end;
  end;

  procedure TMemoryEdit.Change;
  begin
    if not(pEdit = nil) then
      pEdit.ShowBox(pOperand.Data, Length(pOperand.Default))
  end;

end.
