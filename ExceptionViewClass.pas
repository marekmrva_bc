unit ExceptionViewClass;

interface

uses
  ConstantsClass, CustomViewClass, HardwareClass, ResourcesClass, TypesClass,
  Classes, Grids, Menus;

type

  { TExceptionView }

  TExceptionView = class(TCustomView)
  private
    function pGetWidth: Integer; virtual;
    procedure pCustomKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState); virtual;
    procedure pOnSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean); virtual;
    procedure pOnState(Sender: TObject); virtual;
    procedure pSetWidth(Width: Integer); virtual;
  public
    constructor CreateExceptionView(AOwner: TComponent; Hw: THardware); virtual;
    procedure ToggleException(Line: Integer); virtual;
    procedure ToggleMask(Line: Integer); virtual;
    property Width read pGetWidth write pSetWidth;
  end;

implementation

// ************************************************************************** //
// *                     TExceptionView implementation                      * //
// ************************************************************************** //

  function TExceptionView.pGetWidth: Integer;
  begin
    Result := inherited Width;
  end;

  procedure TExceptionView.pCustomKeyDown(Sender: TObject; var Key: Word;
    Shift: TShiftState);
  var
    lselect: TGridCoord;
  begin
    lselect := Selection.TopLeft;
    case Key of
      KEY_RETURN, KEY_SPACE:
        case lselect.X of
          1: ToggleException(lselect.Y);
          2: ToggleMask(lselect.Y);
        end;
    end;
  end;

  procedure TExceptionView.pOnSelectCell(Sender: TObject; ACol, ARow: Integer;
    var CanSelect: Boolean);
  begin
    CanSelect := False;
    if (ACol = 0) or (ARow = 0) then Exit;
    if (ACol = 2) and ((ARow = 1) or (ARow = 2)) then Exit;
    CanSelect := True;
  end;

  procedure TExceptionView.pOnState(Sender: TObject);
  var
    i: Integer;
  begin
    Cells[0, 0] := EXC_VIEW_TYPE;
    Cells[1, 0] := EXC_VIEW_EXC;
    Cells[2, 0] := EXC_VIEW_MASK;
    for i := 1 to 8 do
    begin
      Changes[1, i] := not CompareStateException(Hardware, 8 - i);
      Changes[2, i] := not CompareStateMask(Hardware, 8 - i);
      Cells[0, i] := DESC_EXCE[i - 1];
      if GetException(Hardware.State, 8 - i) then Cells[1, i] := EXC_VIEW_TRUE
      else Cells[1, i] := EXC_VIEW_FALSE;
      if (i > 2) then
        if GetMask(Hardware.State, 8 - i) then Cells[2, i] := EXC_VIEW_TRUE
        else Cells[2, i] := EXC_VIEW_FALSE
      else Cells[2, i] := '';
    end;
  end;

  procedure TExceptionView.pSetWidth(Width: Integer);
  begin
    inherited Width := Width;
    ColWidths[1] := Canvas.TextWidth(EXC_VIEW_EXC + ' ');
    ColWidths[2] := Canvas.TextWidth(EXC_VIEW_MASK + ' ');
    ColWidths[0] := ClientWidth - ColWidths[1] - ColWidths[2];
  end;

  constructor TExceptionView.CreateExceptionView(AOwner: TComponent;
    Hw: THardware);
  begin
    inherited CreateView(AOwner, Hw);
    RowCount := 9;
    ColCount := 3;
    OnKeyDown := pCustomKeyDown;
    OnState := pOnState;
    OnSelectCell := pOnSelectCell;
    PopupMenu := TPopupMenu.Create(Self);
    with PopupMenu do
    begin
      Items.Add(TMenuItem.Create(PopupMenu));
      with Items[Items.Count - 1] do
      begin
        Caption := EXC_VIEW_TOGGLE;
        OnClick := OnDblClick;
        Default := True;
      end;
    end;
    pOnState(Self);
  end;

  procedure TExceptionView.ToggleException(Line: Integer);
  var
    lstate: THardwareState;
  begin
    if not(Line > 0) then Exit;
    Line := 8 - Line;
    lstate := Hardware.State;
    SetException(lstate, Line, not GetException(lstate, Line));
    Hardware.State := lstate;
  end;

  procedure TExceptionView.ToggleMask(Line: Integer);
  var
    lstate: THardwareState;
  begin
    if not(Line > 2) then Exit;
    Line := 8 - Line;
    lstate := Hardware.State;
    SetMask(lstate, Line, not GetMask(lstate, Line));
    Hardware.State := lstate;
  end;

end.
