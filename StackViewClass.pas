unit StackViewClass;

interface

uses
  ConstantsClass, CustomViewClass, FloatEditClass, FunctionsClass,
  HardwareClass, ResourcesClass, TagEditClass, TypesClass,
  Classes, Grids, Menus;

type

  { TStackView }

  TStackView = class(TCustomView)
  private
    pFloatEdit: TFLoatEdit;
    pTagEdit: TTagEdit;
    function pGetWidth: Integer; virtual;
    procedure pCustomKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState); virtual;
    procedure pOnState(Sender: TObject); virtual;
    procedure pSetWidth(Width: Integer); virtual;
  public
    constructor CreateStackView(AOwner: TComponent; Hw: THardware); virtual;
    procedure ChangeStack(Line: Integer); virtual;
    procedure ChangeTag(Line: Integer); virtual;
    property Width read pGetWidth write pSetWidth;
  end;

implementation

// ************************************************************************** //
// *                       TStackView implementation                        * //
// ************************************************************************** //

  function TStackView.pGetWidth: Integer;
  begin
    Result := inherited Width;
  end;

  procedure TStackView.pCustomKeyDown(Sender: TObject; var Key: Word;
    Shift: TShiftState);
  var
    lselect: TGridCoord;
  begin
    lselect := Selection.TopLeft;
    case Key of
      KEY_RETURN, KEY_SPACE:
        case lselect.X of
          1: ChangeTag(lselect.Y);
          2: ChangeStack(lselect.Y);
        end;
    end;
  end;

  procedure TStackView.pOnState(Sender: TObject);
  var
    i: Integer;
  begin
    with Hardware.State.FPUState do
      for i := 0 to 7 do
      begin
        Changes[1, i] := not CompareStateTag(Hardware, i);
        Changes[2, i] := not CompareStateStack(Hardware, i);
        Changes[0, i] := Changes[1, i] or Changes[2, i];
        Cells[0, i] := DESC_STACKS[i];
        Cells[1, i] := DESC_TAG[GetTag(Hardware.State, i)];
        Cells[2, i] := CustomFloatToStr(@ST[i], SizeOf(ST[i]));
      end;
  end;

  procedure TStackView.pSetWidth(Width: Integer);
  begin
    inherited Width := Width;
    ColWidths[0] := Canvas.TextWidth(Cells[0, 0] + ' ');
    ColWidths[1] := Canvas.TextWidth(DESC_TAG[DESC_TAG_LONGEST] + ' ');
    ColWidths[2] := ClientWidth - ColWidths[1] - ColWidths[0];
  end;

  constructor TStackView.CreateStackView(AOwner: TComponent; Hw: THardware);
  begin
    inherited CreateView(AOwner, Hw);
    RowCount := 8;
    ColCount := 3;
    OnKeyDown := pCustomKeyDown;
    OnState := pOnState;
    pFloatEdit := TFLoatEdit.Create(AOwner);
    pTagEdit := TTagEdit.Create(AOwner);
    PopupMenu := TPopupMenu.Create(Self);
    with PopupMenu do
    begin
      Items.Add(TMenuItem.Create(PopupMenu));
      with Items[Items.Count - 1] do
      begin
        Caption := FL_EDIT_MODIFY;
        OnClick := OnDblClick;
        Default := True;
      end;
    end;
    pOnState(Self);
  end;

  procedure TStackView.ChangeStack(Line: Integer);
  var
    lcheck: Boolean;
    lstate: THardwareState;
  begin
    lcheck := True;
    lstate := Hardware.State;
    pFloatEdit.Caption := FL_EDIT_MODIFY + DESC_STACKS[Line];
    with lstate.FPUState do
      if pFloatEdit.ShowBox(@ST[Line], SizeOf(ST[Line]), lcheck) then
      begin
        if lcheck then
          case FloatType(@ST[Line], SizeOf(ST[Line])) of
            -1: SetTag(lstate, Line, 2);
            0: SetTag(lstate, Line, 1);
            1: SetTag(lstate, Line, 0);
          end;
        Hardware.State := lstate;
      end;
  end;

  procedure TStackView.ChangeTag(Line: Integer);
  var
    ltag: Integer;
    lstate: THardwareState;
  begin
    lstate := Hardware.State;
    pTagEdit.Caption := FL_EDIT_MODIFY + DESC_STACKS[Line] + ' Tag';
    ltag := GetTag(lstate, Line);
    if pTagEdit.ShowBox(ltag) then
    begin
      SetTag(lstate, Line, ltag);
      Hardware.State := lstate;
    end;
  end;

end.
