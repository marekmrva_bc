unit FPUFormClass;

interface

uses
  AsmFileIOClass, AsmGridClass, CPUViewClass, ConstantsClass,
  ExceptionViewClass, FunctionsClass, HardwareClass, LogViewClass,
  MemoryViewClass, MiscViewClass, PrefixTreeClass, ResourcesClass,
  StackViewClass, SteppingClass, TypesClass,
  Classes, ComCtrls, Controls, Dialogs, Forms, Menus, StdCtrls;

type
  TViewFPU = class(TForm)
    pMainMenu: TMainMenu;
    pProgram: TMenuItem;
    pExit: TMenuItem;
    Pages: TPageControl;
    HardwarePage: TTabSheet;
    InstructionsPage: TTabSheet;
    InstQuery: TEdit;
    InstSearch: TButton;
    InstList: TListBox;
    InstDescription: TListBox;
    pOpenDialog: TOpenDialog;
    pSaveDialog: TSaveDialog;
    pFile: TMenuItem;
    pNew: TMenuItem;
    pOpen: TMenuItem;
    pSave: TMenuItem;
    pDelim_1: TMenuItem;
    pDelim_0: TMenuItem;
    pReset: TMenuItem;
    pDelim_2: TMenuItem;
    pStep: TMenuItem;
    pDelim_3: TMenuItem;
    pOperand: TMenuItem;
    pOperandAdd: TMenuItem;
    pOperandRemove: TMenuItem;
    pEditor: TMenuItem;
    pTheme: TMenuItem;
    pDefault: TMenuItem;
    pDark: TMenuItem;
    pLog: TMenuItem;
    pClear: TMenuItem;
    pHelp: TMenuItem;
    pAbout: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    // -----------------------------------------
    procedure InstSearchClick(Sender: TObject);
    procedure InstQueryChange(Sender: TObject);
    procedure InstQueryKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure InstructionsPageShow(Sender: TObject);
    procedure InstListDblClick(Sender: TObject);
    procedure InstListKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    // -----------------------------------------
    procedure pNewClick(Sender: TObject);
    procedure pOpenClick(Sender: TObject);
    procedure pSaveClick(Sender: TObject);
    procedure pExitClick(Sender: TObject);
    procedure pResetClick(Sender: TObject);
    procedure pStepClick(Sender: TObject);
    procedure pOperandAddClick(Sender: TObject);
    procedure pDefaultClick(Sender: TObject);
    procedure pDarkClick(Sender: TObject);
    procedure pClearClick(Sender: TObject);
    procedure pAboutClick(Sender: TObject);
  private
    pAsmFileIO: TAsmFileIO;
    pAsmGrid: TAsmGrid;
    pCPUView: TCPUView;
    pDontChange: Boolean;
    pExceptionView: TExceptionView;
    pHardware: THardware;
    pLogView: TLogView;
    pMemoryView: TMemoryView;
    pMiscView: TMiscView;
    pOldOnOperand: TChangeEvent;
    pPosition: Integer;
    pStackView: TStackView;
    pStepping: TStepping;
    pText: String;
    procedure pCustomOnClick(Sender: TObject); virtual;
    procedure pOnOperand(Sender: TObject); virtual;
  public
    { Public declarations }
  end;

var
  ViewFPU: TViewFPU;

implementation

{$R *.dfm}

procedure TViewFPU.FormCreate(Sender: TObject);
begin
  Caption := APP_VERSION;
  Application.Title := APP_VERSION;
  pHardware := THardware.Create;
  pOldOnOperand := pHardware.OnOperand;
  pHardware.OnOperand := pOnOperand;
  pLogView := TLogView.CreateLogView(Self, pHardware);
  pLogView.Parent := HardwarePage;
  pStepping := TStepping.Create;
  pStepping.Hardware := pHardware;
  pAsmFileIO := TAsmFileIO.Create;
  pAsmGrid := TAsmGrid.CreateGrid(Self, pStepping);
  pAsmGrid.Parent := HardwarePage;
  pCPUView := TCPUView.CreateCPUView(Self, pHardware);
  pCPUView.Parent := HardwarePage;
  pExceptionView := TExceptionView.CreateExceptionView(Self, pHardware);
  pExceptionView.Parent := HardwarePage;
  pMemoryView := TMemoryView.CreateMemoryView(Self, pHardware);
  pMemoryView.Parent := HardwarePage;
  pMiscView := TMiscView.CreateMiscView(Self, pHardware);
  pMiscView.Parent := HardwarePage;
  pStackView := TStackView.CreateStackView(Self, pHardware);
  pStackView.Parent := HardwarePage;
  Pages.Width := ClientWidth;
  Pages.Height := ClientHeight;
  Constraints.MinWidth := LAY_MINWIDTH + (Width - HardwarePage.Width);
  Constraints.MinHeight := LAY_MINHEIGHT + (Height - HardwarePage.Height);
  pPosition := -1;
  InstSearch.Click;
end;

procedure TViewFPU.FormResize(Sender: TObject);
var
  lwidth, lheight: Extended;
begin
  Pages.Width := ClientWidth;
  Pages.Height := ClientHeight;
  // HardwarePage layout
  lwidth := (HardwarePage.Width - LAY_MINWIDTH) / 6;
  lheight := (HardwarePage.Height - LAY_MINHEIGHT) / 10;
  pAsmGrid.Left := LAY_PADDING;
  pAsmGrid.Top := LAY_PADDING;
  pAsmGrid.Width := ASM_MINWIDTH + Round(4 * lwidth);
  pAsmGrid.Height := ASM_MINHEIGHT + Round(7 * lheight);
  pMemoryView.Left := pAsmGrid.Left;
  pMemoryView.Top := pAsmGrid.Top + pAsmGrid.Height + LAY_PADDING;
  pMemoryView.Width := MEM_MINWIDTH + Round(6 * lwidth);
  pMemoryView.Height := MEM_MINHEIGHT + Round(lheight);
  pLogView.Left := pMemoryView.Left;
  pLogView.Top := pMemoryView.Top + pMemoryView.Height + LAY_PADDING;
  pLogView.Width := pMemoryView.Width;
  pLogView.Height := LOG_MINHEIGHT + Round(2 * lheight);
  pStackView.Left := pAsmGrid.Left + pAsmGrid.Width + LAY_PADDING;
  pStackView.Top := pAsmGrid.Top;
  pStackView.Width := STA_MINWIDTH + Round(2 * lwidth);
  pStackView.Height := STA_MINHEIGHT + Round(4 * lheight);
  pExceptionView.Left := pStackView.Left;
  pExceptionView.Top := pStackView.Top + pStackView.Height + LAY_PADDING;
  pExceptionView.Width := EXC_MINWIDTH + Round(lwidth);
  pExceptionView.Height := EXC_MINHEIGHT + Round(3 * lheight);
  pMiscView.Left := pExceptionView.Left + pExceptionView.Width + LAY_PADDING;
  pMiscView.Top := pExceptionView.Top;
  pMiscView.Width := MSC_MINWIDTH + Round(lwidth);
  pMiscView.Height := MSC_MINHEIGHT + Round(2 * lheight);
  pCPUView.Left := pMiscView.Left;
  pCPUView.Top := pMiscView.Top + pMiscView.Height + LAY_PADDING;
  pCPUView.Width := CPU_MINWIDTH + Round(lwidth);
  pCPUView.Height := CPU_MINHEIGHT + Round(2 * lheight);
  // InstructionsPage layout
  lwidth := InstructionsPage.Width - (3 * LAY_PADDING);
  lwidth := (lwidth - INS_MINWIDTH - InstSearch.Width) / 15;
  lheight := InstructionsPage.Height - (3 * LAY_PADDING);
  InstQuery.Left := LAY_PADDING;
  InstQuery.Top := LAY_PADDING;
  InstQuery.Width := INS_MINWIDTH + Round(lwidth);
  InstSearch.Left := InstQuery.Left + InstQuery.Width + LAY_PADDING;
  InstSearch.Top := LAY_PADDING;
  InstList.Left := InstQuery.Left;
  InstList.Top := InstQuery.Top + InstQuery.Height + LAY_PADDING;
  InstList.Width := InstQuery.Width + LAY_PADDING + InstSearch.Width;
  InstList.Height := Round(lheight) - InstQuery.Height;
  InstDescription.Left := InstList.Left + InstList.Width + LAY_PADDING;
  InstDescription.Top := InstList.Top;
  InstDescription.Width := Round(14 * lwidth);
  InstDescription.Height := InstList.Height;
end;

procedure TViewFPU.InstSearchClick(Sender: TObject);
var
  lstrings, ldescriptions: TypesClass.TStrings;
  i: Integer;
  ltext: String;
begin
  if pDontChange then Exit;
  SetLength(lstrings, 0);
  SetLength(ldescriptions, 0);
  with InstQuery do
  begin
    if not(pText = Text) or not(pPosition = SelStart) then
    begin
      ltext := StringBefore(Text, SelStart);
      lstrings := pHardware.InstructionsByPrefix(ltext);
      ldescriptions := pHardware.DescriptionsByPrefix(ltext);
      InstDescription.Clear;
      for i := 0 to (Length(ldescriptions) - 1) do
        InstDescription.Items.Add(ldescriptions[i]);
      InstDescription.TopIndex := 0;
      InstList.Clear;
      if (Length(lstrings) = 0) then
      begin
        pHardware.ValidateInstruction(ltext);
        InstList.Items.Add(pHardware.LastError);
      end
      else
      begin
        for i := 0 to (Length(lstrings) - 1) do
          InstList.Items.Add(lstrings[i]);
        InstList.ItemIndex := 0;
      end;
    end;
    pText := Text;
    pPosition := SelStart;
  end;
end;

procedure TViewFPU.InstQueryChange(Sender: TObject);
begin
  InstSearch.Click;
end;

procedure TViewFPU.InstQueryKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  lkey: Word;
begin
  lkey := Key;
  Key := 0;
  case lkey of
    KEY_RETURN: InstList.OnDblClick(Self);
    KEY_HOME: InstQuery.SelStart := 0;
    KEY_END: InstQuery.SelStart := Length(InstQuery.Text);
    KEY_LEFT: InstQuery.SelStart := InstQuery.SelStart - 1;
    KEY_UP: InstList.ItemIndex := MaxOf(InstList.ItemIndex - 1, 0);
    KEY_RIGHT: InstQuery.SelStart := InstQuery.SelStart + 1;
    KEY_DOWN: InstList.ItemIndex := InstList.ItemIndex + 1;
    else Key := lkey;
  end;
  InstSearch.Click;
end;

procedure TViewFPU.InstructionsPageShow(Sender: TObject);
begin
  InstQuery.SetFocus;
  InstQuery.SelectAll;
end;

procedure TViewFPU.InstListDblClick(Sender: TObject);
var
  lkey: Word;
begin
  lkey := KEY_RETURN;
  InstListKeyDown(Sender, lkey, []);
end;

procedure TViewFPU.InstListKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
    KEY_RETURN:
      if (InstList.Count > 0) and not(InstList.ItemIndex < 0) then
      begin
        pDontChange := True;
        InstQuery.Text := InstList.Items[InstList.ItemIndex];
        InstQuery.SelStart := Length(InstQuery.Text);
        pDontChange := False;
        InstSearch.Click;
      end;
  end;
end;

// ************************************************************************** //
// *                       Menu items implementation                        * //
// ************************************************************************** //

procedure TViewFPU.pNewClick(Sender: TObject);
begin
  pStepping.InitializeBlocks;
  pHardware.InitializeOperands;
  pHardware.InitializeState;
  pAsmGrid.ReloadInstructions;
end;

procedure TViewFPU.pOpenClick(Sender: TObject);
begin
  if pOpenDialog.Execute then
  begin
    pAsmFileIO.LoadFromFile(pOpenDialog.FileName, pStepping);
    pAsmGrid.ReloadInstructions;
  end;
end;

procedure TViewFPU.pSaveClick(Sender: TObject);
begin
  if pSaveDialog.Execute then
    pAsmFileIO.SaveToFile(pSaveDialog.FileName, pStepping);
end;

procedure TViewFPU.pExitClick(Sender: TObject);
begin
  Application.Terminate;
end;

procedure TViewFPU.pResetClick(Sender: TObject);
begin
  pAsmGrid.ProgramReset;
end;

procedure TViewFPU.pStepClick(Sender: TObject);
begin
  pAsmGrid.ProgramStep;
end;

procedure TViewFPU.pOperandAddClick(Sender: TObject);
begin
  pMemoryView.OperandAdd;
end;

procedure TViewFPU.pCustomOnClick(Sender: TObject);
begin
  pHardware.OperandRemove(TMenuItem(Sender).Hint);
end;

procedure TViewFPU.pOnOperand(Sender: TObject);
var
  i: Integer;
  ltrees: TPrefixTrees;
  loper: POperandRecord;
begin
  pOperandRemove.Clear;
  ltrees := pHardware.Operands.GetAllDescendants;
  for i := 0 to (Length(ltrees) - 1) do
  begin
    loper := POperandRecord(ltrees[i].Data);
    if (loper^.Default = '') then Continue;
    pOperandRemove.Add(TMenuItem.Create(pOperandRemove));
    with pOperandRemove.Items[pOperandRemove.Count - 1] do
    begin
      Caption := loper^.Name;
      Hint := loper^.Name;
      OnClick := pCustomOnClick;
      Tag := Integer(loper);
    end;
  end;
  pOperandRemove.Enabled := not(pOperandRemove.Count = 0);
  if not(@pOldOnOperand = nil) then pOldOnOperand(Sender);
end;

procedure TViewFPU.pDefaultClick(Sender: TObject);
begin
  pDefault.Checked := True;
  pAsmGrid.ColorTheme := THEME_GRID_DEFAULT;
  pStackView.ColorTheme := THEME_VIEW_DEFAULT;
  pExceptionView.ColorTheme := THEME_VIEW_DEFAULT;
  pMiscView.ColorTheme := THEME_VIEW_DEFAULT;
  pCPUView.ColorTheme := THEME_VIEW_DEFAULT;
  pMemoryView.ColorTheme := THEME_VIEW_DEFAULT;
  pLogView.ColorTheme := THEME_VIEW_DEFAULT;
end;

procedure TViewFPU.pDarkClick(Sender: TObject);
begin
  pDark.Checked := True;
  pAsmGrid.ColorTheme := THEME_GRID_DARK;
  pStackView.ColorTheme := THEME_VIEW_DARK;
  pExceptionView.ColorTheme := THEME_VIEW_DARK;
  pMiscView.ColorTheme := THEME_VIEW_DARK;
  pCPUView.ColorTheme := THEME_VIEW_DARK;
  pMemoryView.ColorTheme := THEME_VIEW_DARK;
  pLogView.ColorTheme := THEME_VIEW_DARK;
end;

procedure TViewFPU.pClearClick(Sender: TObject);
begin
  pLogView.Clear(Sender);
end;

procedure TViewFPU.pAboutClick(Sender: TObject);
begin
  ShowMessage(ABO_TEXT);
end;

end.
