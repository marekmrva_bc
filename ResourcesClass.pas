unit ResourcesClass;

interface

uses
  ConstantsClass, FunctionsClass, TypesClass;

const

// ************************************************************************** //
// *                             GUI Resources                              * //
// ************************************************************************** //

  { Fonts used }

  FONT_DEFAULT_NAME = 'Lucida Console';
  FONT_DEFAULT_SIZE = 10;
  FONT_DEFAULT_HEIGHT = 15;

  { Layout Constants }

  LAY_PADDING = 3;

  CPU_MINWIDTH = 112;
  CPU_MINHEIGHT = 64;
  MSC_MINWIDTH = CPU_MINWIDTH;
  MSC_MINHEIGHT = 124;
  EXC_MINWIDTH = 120;
  EXC_MINHEIGHT = MSC_MINHEIGHT + LAY_PADDING + CPU_MINHEIGHT;
  STA_MINWIDTH = EXC_MINWIDTH + LAY_PADDING + CPU_MINWIDTH;
  STA_MINHEIGHT = 124;
  ASM_MINWIDTH = 264;
  ASM_MINHEIGHT = STA_MINHEIGHT + LAY_PADDING + EXC_MINHEIGHT;
  MEM_MINWIDTH = ASM_MINWIDTH + LAY_PADDING + STA_MINWIDTH;
  MEM_MINHEIGHT = 67;
  LOG_MINHEIGHT = 67;

  LAY_MINWIDTH = MEM_MINWIDTH + (2 * LAY_PADDING);
  LAY_MINHEIGHT = ASM_MINHEIGHT + MEM_MINHEIGHT + LOG_MINHEIGHT + (4 * LAY_PADDING);

  INS_MINWIDTH = 100;

  { Dialog box texts }

  TEXT_RESET = 'Source has been modified. Reset?';

  { Popup menu texts }

  POP_ASM_CHANGE = 'Change';
  POP_ASM_INSERT = 'Insert';
  POP_ASM_REMOVE = 'Remove';
  POP_ASM_STEP = 'Single Step';
  POP_ASM_GET_ORIGIN = 'Go to Origin';
  POP_ASM_SET_ORIGIN = 'New Origin here';
  POP_ASM_RESET = 'Reset Program';
  POP_ASM_CHECKING = 'Checking program...';
  POP_ASM_DONE_RUNNING = 'Done. Running...';
  POP_ASM_ERRORS_FOUND = 'Errors were found!';
  POP_ASM_EXEC_STOPPED = 'Execution stopped.';

  { Misc. Captions, Texts, Buttons, etc. }

  ABO_TEXT = 'Created by Marek Mrva (c) 2010';

  LOG_VIEW_CLEAR = 'Clear';

  CPU_VIEW_AX = 'AX';
  CPU_VIEW_CHANGE = 'Change';
  CPU_VIEW_TRUE = '1';
  CPU_VIEW_FALSE = '0';
  CPU_VIEW_CHG_AX = CPU_VIEW_CHANGE + ' AX';

  MEM_VIEW_NAME = 'Name';
  MEM_VIEW_TYPE = 'Type';
  MEM_VIEW_VALUE = 'Value';
  MEM_VIEW_EDIT = 'Edit';
  MEM_VIEW_ADD = 'Add operand';
  MEM_VIEW_REMOVE = 'Remove operand';

  MSC_VIEW_FSW = 'FSW';
  MSC_VIEW_FCW = 'FCW';
  MSC_VIEW_PREC = 'Prec.';
  MSC_VIEW_ROUND = 'Round';
  MSC_VIEW_CHANGE = 'Change';
  MSC_VIEW_TRUE = '1';
  MSC_VIEW_FALSE = '0';
  MSC_VIEW_CHG_SW = MSC_VIEW_CHANGE + ' FPU Status Word';
  MSC_VIEW_CHG_CW = MSC_VIEW_CHANGE + ' FPU Control Word';

  EXC_VIEW_TYPE = '';
  EXC_VIEW_EXC = 'Exc';
  EXC_VIEW_MASK = 'Msk';
  EXC_VIEW_TOGGLE = 'Toggle';
  EXC_VIEW_TRUE = '1';
  EXC_VIEW_FALSE = '0';

  OP_EDIT_OK = 'OK';
  OP_EDIT_CANCEL = 'Cancel';
  OP_EDIT_CAPTION = 'Add new operand';
  OP_EDIT_DESCRIPTION = 'Format of operand - NAME:TYPE';

  BC_EDIT_OK = 'OK';
  BC_EDIT_CANCEL = 'Cancel';
  BC_EDIT_BCD = 'BCD';
  BC_EDIT_HEX = 'Hex';

  IN_EDIT_OK = 'OK';
  IN_EDIT_CANCEL = 'Cancel';
  IN_EDIT_MODIFY = 'Modify ';
  IN_EDIT_INT_S = 'Signed';
  IN_EDIT_INT_U = 'Unsigned';
  IN_EDIT_HEX = 'Hex';

  FL_EDIT_OK = 'OK';
  FL_EDIT_CANCEL = 'Cancel';
  FL_EDIT_MODIFY = 'Modify ';
  FL_EDIT_FLOAT = 'Float';
  FL_EDIT_HEX = 'Hex';
  FL_EDIT_TAG = ' tag';
  FL_EDIT_MODIFY_TAG = FL_EDIT_MODIFY + 'associated' + FL_EDIT_TAG;

  ASM_INPUT_BUTTON_ASSEMBLE = 'Assemble';
  ASM_INPUT_BUTTON_CANCEL = 'Cancel';
  ASM_INPUT_CAPTION_INSERT = 'Insert instruction at address';
  ASM_INPUT_CAPTION_CHANGE = 'Change instruction at address';

  { Preprogrammed Color Themes }

  THEME_GRID_DEFAULT: TGridTheme = (
    Address: (None: (FG: $404040; BG: $FFFFFF);
              Selected: (FG: $202020; BG: $E0E0E0));
    Assembly: (None: (FG: $202020; BG: $FFFFFF);
              Selected: (FG: $000000; BG: $E0E0E0));
    Highlight: (FG: $000000; BG: $00FFFF);
    Step: (FG: $000000; BG: $FFA080);
  );

  THEME_GRID_DARK: TGridTheme = (
    Address: (None: (FG: $C0C0C0; BG: $000000);
              Selected: (FG: $E0E0E0; BG: $505050));
    Assembly: (None: (FG: $FFFFFF; BG: $000000);
              Selected: (FG: $FFFFFF; BG: $505050));
    Highlight: (FG: $000000; BG: $00FFFF);
    Step: (FG: $FFFFFF; BG: $A01010);
  );

  THEME_VIEW_DEFAULT: TViewTheme = (
    Line: (None: (FG: $404040; BG: $FFFFFF);
           Selected: (FG: $202020; BG: $E0E0E0));
    Change: $0000FF;
  );

  THEME_VIEW_DARK: TViewTheme = (
    Line: (None: (FG: $FFFFFF; BG: $000000);
           Selected: (FG: $FFFFFF; BG: $505050));
    Change: $0000FF;
  );

// ************************************************************************** //
// *                             API Resources                              * //
// ************************************************************************** //

  { Error Messages }

  GLOB_NO_ERROR = '';

  FIO_SYSTEM_NOT_FOUND = 'Invalid Asm File';
  FIO_LOADING = 'Loading file: ';
  FIO_ERROR_LOADING = 'Errors found when loading file!';
  FIO_LOADED = 'Loaded.';
  FIO_SAVING = 'Saving file: ';
  FIO_SAVED = 'Saved.';

  INST_CHAR_INVALID = 'Invalid character';
  INST_NONE = 'No instruction specified';
  INST_INST_UNKNOWN = 'Unrecognized instruction';
  INST_INST_UNSUPPORTED = 'Instruction not supported on physical CPU';
  INST_OPER_INVALID = 'Instruction does not support given operands';
  INST_OPER_NOT_ENOUGH = 'Not enough operands';
  INST_OPER_TOO_MANY = 'Too many operands';
  INST_OPER_UNKNOWN = 'Unrecognized operand';
  INST_OPER_TYPE_UNKNOWN = 'Unrecognized operand type';
  INST_OPER_TYPE_INVALID = 'Invalid operand';
  INST_OPER_NAME_EXISTS = 'Operand name already exists';
  INST_OPER_NAME_UNKNOWN = 'Unrecognized operand name';

  STEP_BRANCH_OUT_OF_RANGE = 'Branch address out of range';
  STEP_NO_CALL_FUNCTION = 'Call function not specified';
  STEP_POSITION_OUT_OF_RANGE = 'Position out of range';

  { Instructions Descriptions }

  DESC_CO = ', ';
  DESC_COA = ', and ';
  DESC_DO = '. ';
  DESC_AF = ' after ';
  DESC_AN = ' and ';
  DESC_BY = ' by ';
  DESC_FO = ' for ';
  DESC_FR = ' from ';
  DESC_IF = ' if ';
  DESC_IN = ' in ';
  DESC_INT = ' into ';
  DESC_IT = 'its ';
  DESC_OF = ' of ';
  DESC_ONT = ' onto ';
  DESC_OR = ' or ';
  DESC_TH = 'the ';
  DESC_TO = ' to ';
  DESC_WI = ' with ';
  DESC_WO = ' without ';

  DESC_ADD = 'Add ';
  DESC_CHK = 'Check ';
  DESC_CLA = 'Classify ';
  DESC_CLR = 'Clear ';
  DESC_CMP = 'Compare ';
  DESC_CNV = 'Convert ';
  DESC_COM = 'Complements ';
  DESC_COP = 'Copy ';
  DESC_CPT = 'Compute ';
  DESC_DEC = 'Decrement ';
  DESC_DIV = 'Divide ';
  DESC_INC = 'Increment ';
  DESC_INI = 'Initialize ';
  DESC_JM = 'Jump';
  DESC_LOA = 'Load ';
  DESC_MOV = 'Move ';
  DESC_MUL = 'Multiply ';
  DESC_PSH = 'Push ';
  DESC_REP = 'Replace ';
  DESC_RND = 'Round ';
  DESC_SCA = 'Scale ';
  DESC_SET = 'Sets' + FL_EDIT_TAG;
  DESC_SPR = 'Separate ';
  DESC_STR = 'Store ';
  DESC_SUB = 'Subtract ';
  DESC_XCH = 'Exchange ';

  DESC_TAG: array [0..3] of String = (
    'Valid',
    'Zero',
    'Invalid',
    'Empty'
  );
  DESC_TAG_LONGEST = 2;

  DESC_EXCE: array [0..7] of String = (
    'Error',
    'Stack',
    'Precis',
    'Underf',
    'Overf',
    'ZerDiv',
    'Denorm',
    'Inv.Op'
  );

  DESC_CONDITION = 'C';
  DESC_COND: array [0..3] of String = (
    DESC_CONDITION + '0',
    DESC_CONDITION + '1',
    DESC_CONDITION + '2',
    DESC_CONDITION + '3'
  );

  DESC_PREC_BIT = 'b';
  DESC_PREC: array [0..3] of String = (
    '24' + DESC_PREC_BIT,
    '???',
    '53' + DESC_PREC_BIT,
    '64' + DESC_PREC_BIT
  );

  DESC_ROUN: array [0..3] of String = (
    'NEAR',
    'DOWN',
    'UP',
    'ZERO'
  );
  DESC_ROUN_LONGEST = 0;

  DESC_FLAG: array [0..2] of String = (
    'CF',
    'PF',
    'ZF'
  );

  DESC_STACK = 'ST';
  DESC_STACKS: array [0..7] of String = (
    DESC_STACK + '(0)', DESC_STACK + '(1)',
    DESC_STACK + '(2)', DESC_STACK + '(3)',
    DESC_STACK + '(4)', DESC_STACK + '(5)',
    DESC_STACK + '(6)', DESC_STACK + '(7)'
  );

  DESC_ST_0 = DESC_STACK + '(0)';
  DESC_ST_1 = DESC_STACK + '(1)';
  DESC_ST_I = DESC_STACK + '(i)';
  DESC_AX = 'AX';
  DESC_WORD = 'WORD';
  DESC_DWORD = 'DWORD';
  DESC_QWORD = 'QWORD';
  DESC_ADDR = 'ADDRESS';
  DESC_BCD = 'BCD';
  DESC_SINGLE = 'SINGLE';
  DESC_DOUBLE = 'DOUBLE';
  DESC_EXTEND = 'EXTENDED';
  DESC_CW = 'CONTROL_WORD';
  DESC_SW = 'STATUS_WORD';
  DESC_ENV_14 = 'ENVIR_14B';
  DESC_ENV_28 = 'ENVIR_28B';
  DESC_STAT_94 = 'STATE_94B';
  DESC_STAT_108  = 'STATE_108B';

  DESC_EFL = 'EFLAGS';
  DESC_FPU = 'FPU';
  DESC_FPU_S = 'the ' + DESC_FPU + ' stack';
  DESC_FSW = DESC_FPU + ' status word';
  DESC_FCW = DESC_FPU + ' control word';
  DESC_FEN = DESC_FPU + ' environment';
  DESC_STA = DESC_FPU + ' state';
  DESC_SIN = 'sine';
  DESC_COS = 'cosine';
  DESC_SQRT = 'square root';
  DESC_I_SIN = DESC_IT + DESC_SIN;
  DESC_I_COS = DESC_IT + DESC_COS;
  DESC_STO = 'store ';
  DESC_STORE = DESC_STO + 'result' + DESC_IN;
  DESC_STORE_ST_0 = DESC_STORE + DESC_ST_0;
  DESC_STORE_ST_1 = DESC_STORE + DESC_ST_1;
  DESC_STORE_ST_I = DESC_STORE + DESC_ST_I;
  DESC_STORE_EFL = DESC_STORE + DESC_EFL;
  DESC_STORE_FSW = DESC_STORE + DESC_FSW;
  DESC_VAL = 'value';
  DESC_EXP = 'exponent';
  DESC_SIG = 'significand';
  DESC_POP = 'pop ';
  DESC_POP_FPU = DESC_POP + DESC_FPU_S;
  DESC_PUSH = 'push';
  DESC_PUSH_FPU = DESC_PUSH + DESC_ONT + DESC_FPU_S;
  DESC_FP_EXC = DESC_FPU + ' exception flags';
  DESC_FP_E = DESC_FPU + ' exceptions';
  DESC_CHECK_O = 'check for ordered values';
  DESC_PU_FP_E = 'pending unmasked ' + DESC_FP_E;
  DESC_CHECK_PUE = 'checking' + DESC_FO + DESC_PU_FP_E;
  DESC_REINIT = '. Then re-initialize the ' + DESC_FPU;
  DESC_MASK = '. Then mask all ' + DESC_FP_E;
  DESC_REM_D = 'remainder obtained from dividing ';
  DESC_CON = DESC_TH + 'contents' + DESC_OF;
  DESC_2X = ' twice';
  DESC_TOP_F = DESC_TH + 'TOP field';
  DESC_A = 'above';
  DESC_B = 'below';
  DESC_C = 'carry';
  DESC_E = 'equal';
  DESC_G = 'greater';
  DESC_L = 'less';
  DESC_N = 'not ';
  DESC_O = 'overflow';
  DESC_P = 'parity';
  DESC_U = 'unordered';
  DESC_EVEN = ' even';
  DESC_ODD = ' odd';
  DESC_SIGN = 'sign';
  DESC_ZERO = 'zero';
  DESC_EMPTY = 'empty';
  DESC_TRUNC = 'truncation';
  DESC_A_L = DESC_IF + DESC_A + ' (CF = 0 and ZF = 0)';
  DESC_AE_L = DESC_IF + DESC_A + DESC_OR + DESC_E + ' (CF = 0)';
  DESC_B_L = DESC_IF + DESC_B + ' (CF = 1)';
  DESC_BE_L = DESC_IF + DESC_B + DESC_OR + DESC_E + ' (CF = 1 or ZF = 1)';
  DESC_C_L = DESC_IF + DESC_C + ' (CF = 1)';
  DESC_E_L = DESC_IF + DESC_E + ' (ZF = 1)';
  DESC_G_L = DESC_IF + DESC_G + ' (ZF = 0 and SF = OF)';
  DESC_GE_L = DESC_IF + DESC_G + DESC_OR + DESC_E + ' (SF = OF)';
  DESC_L_L = DESC_IF + DESC_L + ' (SF <> OF)';
  DESC_LE_L = DESC_IF + DESC_L + DESC_OR + DESC_E + ' (ZF = 1 or SF <> OF)';
  DESC_NA_L = DESC_IF + DESC_N + DESC_A + ' (CF = 1 or ZF = 1)';
  DESC_NAE_L = DESC_IF + DESC_N + DESC_A + DESC_OR + DESC_E + ' (CF = 1)';
  DESC_NB_L =  DESC_IF + DESC_N + DESC_B + ' (CF = 0)';
  DESC_NBE_L = DESC_IF + DESC_N + DESC_B + DESC_OR + DESC_E + ' (CF = 0 and ZF = 0)';
  DESC_NC_L = DESC_IF + DESC_N + DESC_C + ' (CF = 0)';
  DESC_NE_L = DESC_IF + DESC_N + DESC_E + ' (ZF = 0)';
  DESC_NG_L = DESC_IF + DESC_N + DESC_G + ' (ZF = 1 or SF <> OF)';
  DESC_NGE_L = DESC_IF + DESC_N + DESC_G + DESC_OR + DESC_E + ' (SF <> OF)';
  DESC_NL_L = DESC_IF + DESC_N + DESC_L + ' (SF = OF)';
  DESC_NLE_L = DESC_IF + DESC_N + DESC_L + DESC_OR + DESC_E + ' (ZF = 0 and SF = OF)';
  DESC_NO_L = DESC_IF + DESC_N + DESC_O + ' (OF = 0)';
  DESC_NP_L = DESC_IF + DESC_N + DESC_P + ' (PF = 0)';
  DESC_NS_L = DESC_IF + DESC_N + DESC_SIGN + ' (SF = 0)';
  DESC_NU_L = DESC_IF + DESC_N + DESC_U + ' (PF = 0)';
  DESC_NZ_L = DESC_IF + DESC_N + DESC_ZERO + ' (ZF = 0)';
  DESC_O_L = DESC_IF + DESC_O + ' (OF = 1)';
  DESC_P_L = DESC_IF + DESC_P + ' (PF = 1)';
  DESC_PE_L = DESC_IF + DESC_P + DESC_EVEN + ' (PF = 1)';
  DESC_PO_L = DESC_IF + DESC_P + DESC_ODD + ' (PF = 0)';
  DESC_S_L = DESC_IF + DESC_SIGN + ' (SF = 1)';
  DESC_U_L = DESC_IF + DESC_U + ' (PF = 1)';
  DESC_Z_L = DESC_IF + DESC_ZERO + ' (ZF = 1)';

  DESC_F2XM1 = DESC_REP + DESC_ST_0 + DESC_WI + '(2^(ST(0)) - 1)';
  DESC_FABS = DESC_REP + DESC_ST_0 + DESC_WI + DESC_IT + 'absolute value';
  DESC_FADD_S = DESC_ADD + DESC_SINGLE + DESC_TO + DESC_ST_0 + DESC_AN + DESC_STORE_ST_0;
  DESC_FADD_D = DESC_ADD + DESC_DOUBLE + DESC_TO + DESC_ST_0 + DESC_AN + DESC_STORE_ST_0;
  DESC_FADD_T_I = DESC_ADD + DESC_ST_0 + DESC_TO + DESC_ST_I + DESC_AN + DESC_STORE_ST_0;
  DESC_FADD_I_T = DESC_ADD + DESC_ST_I + DESC_TO + DESC_ST_0 + DESC_AN + DESC_STORE_ST_I;
  DESC_FADDP_I_T = DESC_ADD + DESC_ST_0 + DESC_TO + DESC_ST_I + DESC_CO + DESC_STORE_ST_I + DESC_COA + DESC_POP_FPU;
  DESC_FADDP = DESC_ADD + DESC_ST_0 + DESC_TO + DESC_ST_1 + DESC_CO + DESC_STORE_ST_1 + DESC_COA + DESC_POP_FPU;
  DESC_FBLD_0 = DESC_CNV + DESC_BCD + DESC_TO + DESC_EXTEND + DESC_AN + DESC_PUSH_FPU;
  DESC_FBSTP_0 = DESC_STR + DESC_ST_0 + DESC_IN + DESC_BCD + DESC_AN + DESC_POP_FPU;
  DESC_FCHS = DESC_COM + DESC_SIGN + DESC_OF + DESC_ST_0;
  DESC_FCLEX = DESC_CLR + DESC_FP_EXC + DESC_AF + DESC_CHECK_PUE;
  DESC_FCMOVB_T_I = DESC_MOV + DESC_ST_I + DESC_TO + DESC_ST_0 + DESC_B_L;
  DESC_FCMOVBE_T_I = DESC_MOV + DESC_ST_I + DESC_TO + DESC_ST_0 + DESC_BE_L;
  DESC_FCMOVE_T_I = DESC_MOV + DESC_ST_I + DESC_TO + DESC_ST_0 + DESC_E_L;
  DESC_FCMOVNB_T_I = DESC_MOV + DESC_ST_I + DESC_TO + DESC_ST_0 + DESC_NB_L;
  DESC_FCMOVNBE_T_I = DESC_MOV + DESC_ST_I + DESC_TO + DESC_ST_0 + DESC_NBE_L;
  DESC_FCMOVNE_T_I = DESC_MOV + DESC_ST_I + DESC_TO + DESC_ST_0 + DESC_NE_L;
  DESC_FCMOVNU_T_I = DESC_MOV + DESC_ST_I + DESC_TO + DESC_ST_0 + DESC_NU_L;
  DESC_FCMOVU_T_I = DESC_MOV + DESC_ST_I + DESC_TO + DESC_ST_0 + DESC_U_L;
  DESC_FCOM = DESC_CMP + DESC_ST_0 + DESC_WI + DESC_ST_1 + DESC_AN + DESC_STORE_FSW;
  DESC_FCOM_S = DESC_CMP + DESC_ST_0 + DESC_WI + DESC_SINGLE + DESC_AN + DESC_STORE_FSW;
  DESC_FCOM_D = DESC_CMP + DESC_ST_0 + DESC_WI + DESC_DOUBLE + DESC_AN + DESC_STORE_FSW;
  DESC_FCOM_I = DESC_CMP + DESC_ST_0 + DESC_WI + DESC_ST_I + DESC_AN + DESC_STORE_FSW;
  DESC_FCOMI_T_I = DESC_CMP + DESC_ST_0 + DESC_WI + DESC_ST_I + DESC_AN + DESC_STORE_EFL;
  DESC_FCOMIP_T_I = DESC_CMP + DESC_ST_0 + DESC_WI + DESC_ST_I + DESC_CO + DESC_STORE_EFL + DESC_COA + DESC_POP_FPU;
  DESC_FCOMP = DESC_CMP + DESC_ST_0 + DESC_WI + DESC_ST_1 + DESC_CO + DESC_STORE_FSW + DESC_COA + DESC_POP_FPU;
  DESC_FCOMP_S = DESC_CMP + DESC_ST_0 + DESC_WI + DESC_SINGLE + DESC_CO + DESC_STORE_FSW + DESC_COA + DESC_POP_FPU;
  DESC_FCOMP_D = DESC_CMP + DESC_ST_0 + DESC_WI + DESC_DOUBLE + DESC_CO + DESC_STORE_FSW + DESC_COA + DESC_POP_FPU;
  DESC_FCOMP_I = DESC_CMP + DESC_ST_0 + DESC_WI + DESC_ST_I + DESC_CO + DESC_STORE_FSW + DESC_COA + DESC_POP_FPU;
  DESC_FCOMPP = DESC_CMP + DESC_ST_0 + DESC_WI + DESC_ST_1 + DESC_CO + DESC_STORE_FSW + DESC_COA + DESC_POP_FPU + DESC_2X;
  DESC_FCOS = DESC_REP + DESC_ST_0 + DESC_WI + DESC_I_COS;
  DESC_FDECSTP = DESC_DEC + DESC_TOP_F + DESC_IN + DESC_FSW;
  DESC_FDIV_S = DESC_DIV + DESC_ST_0 + DESC_BY + DESC_SINGLE + DESC_AN + DESC_STORE_ST_0;
  DESC_FDIV_D = DESC_DIV + DESC_ST_0 + DESC_BY + DESC_DOUBLE + DESC_AN + DESC_STORE_ST_0;
  DESC_FDIV_T_I = DESC_DIV + DESC_ST_0 + DESC_BY + DESC_ST_I + DESC_AN + DESC_STORE_ST_0;
  DESC_FDIV_I_T = DESC_DIV + DESC_ST_I + DESC_BY + DESC_ST_0 + DESC_AN + DESC_STORE_ST_I;
  DESC_FDIVP = DESC_DIV + DESC_ST_1 + DESC_BY + DESC_ST_0 + DESC_CO + DESC_STORE_ST_1 + DESC_COA + DESC_POP_FPU;
  DESC_FDIVP_I_T = DESC_DIV + DESC_ST_I + DESC_BY + DESC_ST_0 + DESC_CO + DESC_STORE_ST_I + DESC_COA + DESC_POP_FPU;
  DESC_FDIVR_S = DESC_DIV + DESC_SINGLE + DESC_BY + DESC_ST_0 + DESC_AN + DESC_STORE_ST_0;
  DESC_FDIVR_D = DESC_DIV + DESC_DOUBLE + DESC_BY + DESC_ST_0 + DESC_AN + DESC_STORE_ST_0;
  DESC_FDIVR_T_I = DESC_DIV + DESC_ST_I + DESC_BY + DESC_ST_0 + DESC_AN + DESC_STORE_ST_0;
  DESC_FDIVR_I_T = DESC_DIV + DESC_ST_0 + DESC_BY + DESC_ST_I + DESC_AN + DESC_STORE_ST_I;
  DESC_FDIVRP = DESC_DIV + DESC_ST_0 + DESC_BY + DESC_ST_1 + DESC_CO + DESC_STORE_ST_1 + DESC_COA + DESC_POP_FPU;
  DESC_FDIVRP_I_T = DESC_DIV + DESC_ST_0 + DESC_BY + DESC_ST_I + DESC_CO + DESC_STORE_ST_I + DESC_COA + DESC_POP_FPU;
  DESC_FFREE_I = DESC_SET + DESC_FO + DESC_ST_I + DESC_TO + DESC_EMPTY;
  DESC_FIADD_4 = DESC_ADD + DESC_DWORD + DESC_TO + DESC_ST_0 + DESC_AN + DESC_STORE_ST_0;
  DESC_FIADD_2 = DESC_ADD + DESC_WORD + DESC_TO + DESC_ST_0 + DESC_AN + DESC_STORE_ST_0;
  DESC_FICOM_2 = DESC_CMP + DESC_ST_0 + DESC_WI + DESC_WORD + DESC_AN + DESC_STORE_FSW;
  DESC_FICOM_4 = DESC_CMP + DESC_ST_0 + DESC_WI + DESC_DWORD + DESC_AN + DESC_STORE_FSW;
  DESC_FICOMP_2 = DESC_CMP + DESC_ST_0 + DESC_WI + DESC_WORD + DESC_CO + DESC_STORE_FSW + DESC_COA + DESC_POP_FPU;
  DESC_FICOMP_4 = DESC_CMP + DESC_ST_0 + DESC_WI + DESC_DWORD + DESC_CO + DESC_STORE_FSW + DESC_COA + DESC_POP_FPU;
  DESC_FIDIV_2 = DESC_DIV + DESC_ST_0 + DESC_BY + DESC_WORD + DESC_AN + DESC_STORE_ST_0;
  DESC_FIDIV_4 = DESC_DIV + DESC_ST_0 + DESC_BY + DESC_DWORD + DESC_AN + DESC_STORE_ST_0;
  DESC_FIDIVR_2 = DESC_DIV + DESC_WORD + DESC_BY + DESC_ST_0 + DESC_AN + DESC_STORE_ST_0;
  DESC_FIDIVR_4 = DESC_DIV + DESC_DWORD + DESC_BY + DESC_ST_0 + DESC_AN + DESC_STORE_ST_0;
  DESC_FILD_2 = DESC_PSH + DESC_WORD + DESC_ONT + DESC_FPU_S;
  DESC_FILD_4 = DESC_PSH + DESC_DWORD + DESC_ONT + DESC_FPU_S;
  DESC_FILD_8 = DESC_PSH + DESC_QWORD + DESC_ONT + DESC_FPU_S;
  DESC_FIMUL_2 = DESC_MUL + DESC_ST_0 + DESC_BY + DESC_WORD + DESC_AN + DESC_STORE_ST_0;
  DESC_FIMUL_4 = DESC_MUL + DESC_ST_0 + DESC_BY + DESC_DWORD + DESC_AN + DESC_STORE_ST_0;
  DESC_FINCSTP = DESC_INC + DESC_TOP_F + DESC_IN + DESC_FSW;
  DESC_FINIT = DESC_INI + DESC_FPU + DESC_AF + DESC_CHECK_PUE;
  DESC_FIST_2 = DESC_STR + DESC_ST_0 + DESC_IN + DESC_WORD;
  DESC_FIST_4 = DESC_STR + DESC_ST_0 + DESC_IN + DESC_DWORD;
  DESC_FISTP_2 = DESC_STR + DESC_ST_0 + DESC_IN + DESC_WORD + DESC_AN + DESC_POP_FPU;
  DESC_FISTP_4 = DESC_STR + DESC_ST_0 + DESC_IN + DESC_DWORD + DESC_AN + DESC_POP_FPU;
  DESC_FISTP_8 = DESC_STR + DESC_ST_0 + DESC_IN + DESC_QWORD + DESC_AN + DESC_POP_FPU;
  DESC_FISTTP_2 = DESC_STR + DESC_ST_0 + DESC_IN + DESC_WORD + DESC_WI + DESC_TRUNC + DESC_AN + DESC_POP_FPU;
  DESC_FISTTP_4 = DESC_STR + DESC_ST_0 + DESC_IN + DESC_DWORD + DESC_WI + DESC_TRUNC + DESC_AN + DESC_POP_FPU;
  DESC_FISTTP_8 = DESC_STR + DESC_ST_0 + DESC_IN + DESC_QWORD + DESC_WI + DESC_TRUNC + DESC_AN + DESC_POP_FPU;
  DESC_FISUB_2 = DESC_SUB + DESC_WORD + DESC_FR + DESC_ST_0 + DESC_AN + DESC_STORE_ST_0;
  DESC_FISUB_4 = DESC_SUB + DESC_DWORD + DESC_FR + DESC_ST_0 + DESC_AN + DESC_STORE_ST_0;
  DESC_FISUBR_2 = DESC_SUB + DESC_ST_0 + DESC_FR + DESC_WORD + DESC_AN + DESC_STORE_ST_0;
  DESC_FISUBR_4 = DESC_SUB + DESC_ST_0 + DESC_FR + DESC_DWORD + DESC_AN + DESC_STORE_ST_0;
  DESC_FLD_S = DESC_PSH + DESC_SINGLE + DESC_ONT + DESC_FPU_S;
  DESC_FLD_D = DESC_PSH + DESC_DOUBLE + DESC_ONT + DESC_FPU_S;
  DESC_FLD_E = DESC_PSH + DESC_EXTEND + DESC_ONT + DESC_FPU_S;
  DESC_FLD_I = DESC_PSH + DESC_ST_I + DESC_ONT + DESC_FPU_S;
  DESC_FLD1 = DESC_PSH + '+1.0' + DESC_ONT + DESC_FPU_S;
  DESC_FLDCW_C = DESC_LOA + DESC_FCW + DESC_FR + DESC_CW;
  DESC_FLDENV_14 = DESC_LOA + DESC_FEN + DESC_FR + DESC_ENV_14;
  DESC_FLDENV_28 = DESC_LOA + DESC_FEN + DESC_FR + DESC_ENV_28;
  DESC_FLDL2E = DESC_PSH + 'log_2(e)' + DESC_ONT + DESC_FPU_S;
  DESC_FLDL2T = DESC_PSH + 'log_2(10)' + DESC_ONT + DESC_FPU_S;
  DESC_FLDLG2 = DESC_PSH + 'log_10(2)' + DESC_ONT + DESC_FPU_S;
  DESC_FLDLN2 = DESC_PSH + 'log_e(2)' + DESC_ONT + DESC_FPU_S;
  DESC_FLDPI = DESC_PSH + 'Pi (3.141...)' + DESC_ONT + DESC_FPU_S;
  DESC_FLDZ = DESC_PSH + '+0.0' + DESC_ONT + DESC_FPU_S;
  DESC_FMUL_S = DESC_MUL + DESC_ST_0 + DESC_BY + DESC_SINGLE + DESC_AN + DESC_STORE_ST_0;
  DESC_FMUL_D = DESC_MUL + DESC_ST_0 + DESC_BY + DESC_DOUBLE + DESC_AN + DESC_STORE_ST_0;
  DESC_FMUL_T_I = DESC_MUL + DESC_ST_0 + DESC_BY + DESC_ST_I + DESC_AN + DESC_STORE_ST_0;
  DESC_FMUL_I_T = DESC_MUL + DESC_ST_I + DESC_BY + DESC_ST_0 + DESC_AN + DESC_STORE_ST_I;
  DESC_FMULP = DESC_MUL + DESC_ST_1 + DESC_BY + DESC_ST_0 + DESC_CO + DESC_STORE_ST_1 + DESC_COA + DESC_POP_FPU;
  DESC_FMULP_I_T = DESC_MUL + DESC_ST_I + DESC_BY + DESC_ST_0 + DESC_CO + DESC_STORE_ST_I + DESC_COA + DESC_POP_FPU;
  DESC_FNCLEX = DESC_CLR + DESC_FP_EXC + DESC_WO + DESC_CHECK_PUE;
  DESC_FNINIT = DESC_INI + DESC_FPU + DESC_WO + DESC_CHECK_PUE;
  DESC_FNOP = 'No operation is performed';
  DESC_FNSAVE_94 = DESC_STR + DESC_STA + DESC_TO + DESC_STAT_94 + DESC_WO + DESC_CHECK_PUE + DESC_REINIT;
  DESC_FNSAVE_108 = DESC_STR + DESC_STA + DESC_TO + DESC_STAT_108 + DESC_WO + DESC_CHECK_PUE + DESC_REINIT;
  DESC_FNSTCW_C = DESC_STR + DESC_FCW + DESC_TO + DESC_CW + DESC_WO + DESC_CHECK_PUE;
  DESC_FNSTENV_14 = DESC_STR + DESC_FEN + DESC_TO + DESC_ENV_14 + DESC_WO + DESC_CHECK_PUE + DESC_MASK;
  DESC_FNSTENV_28 = DESC_STR + DESC_FEN + DESC_TO + DESC_ENV_28 + DESC_WO + DESC_CHECK_PUE + DESC_MASK;
  DESC_FNSTSW_W = DESC_STR + DESC_FSW + DESC_TO + DESC_SW + DESC_WO + DESC_CHECK_PUE;
  DESC_FNSTSW_AX = DESC_STR + DESC_FSW + DESC_TO + DESC_AX + DESC_WO + DESC_CHECK_PUE;
  DESC_FPATAN = DESC_REP + DESC_ST_1 + DESC_WI + 'arctan(' + DESC_ST_1 + '/' + DESC_ST_0 + ')' + DESC_AN + DESC_POP_FPU;
  DESC_FPREM = DESC_REP + DESC_ST_0 + DESC_WI + DESC_TH + DESC_REM_D + DESC_ST_0 + DESC_BY + DESC_ST_1;
  DESC_FPREM1 = DESC_REP + DESC_ST_0 + DESC_WI + DESC_TH + 'IEEE ' + DESC_REM_D + DESC_ST_0 + DESC_BY + DESC_ST_1;
  DESC_FPTAN = DESC_REP + DESC_ST_0 + DESC_WI + 'its tangent' + DESC_AN + DESC_PUSH + ' 1' + DESC_ONT + DESC_FPU_S;
  DESC_FRNDINT = DESC_RND + DESC_ST_0 + DESC_TO + 'an integer';
  DESC_FRSTOR_94 = DESC_LOA + DESC_STA + DESC_FR + DESC_STAT_94;
  DESC_FRSTOR_108 = DESC_LOA + DESC_STA + DESC_FR + DESC_STAT_108;
  DESC_FSAVE_94 = DESC_STR + DESC_STA + DESC_TO + DESC_STAT_94 + DESC_AF + DESC_CHECK_PUE + DESC_REINIT;
  DESC_FSAVE_108 = DESC_STR + DESC_STA + DESC_TO + DESC_STAT_108 + DESC_AF + DESC_CHECK_PUE + DESC_REINIT;
  DESC_FSCALE = DESC_SCA + DESC_ST_0 + DESC_BY + DESC_ST_1;
  DESC_FSIN = DESC_REP + DESC_ST_0 + DESC_WI + DESC_I_SIN;
  DESC_FSINCOS = DESC_CPT + DESC_TH + DESC_SIN + DESC_AN + DESC_COS + DESC_OF + DESC_ST_0 + DESC_DO + DESC_REP + DESC_ST_0 + DESC_WI + DESC_TH + DESC_SIN + DESC_COA + DESC_PUSH + ' ' + DESC_TH + DESC_COS + DESC_ONT + DESC_FPU_S;
  DESC_FSQRT = DESC_CPT + DESC_SQRT + DESC_OF + DESC_ST_0 + DESC_AN + DESC_STORE_ST_0;
  DESC_FST_S = DESC_STR + DESC_ST_0 + DESC_TO + DESC_SINGLE;
  DESC_FST_D = DESC_STR + DESC_ST_0 + DESC_TO + DESC_DOUBLE;
  DESC_FST_I = DESC_COP + DESC_ST_0 + DESC_TO + DESC_ST_I;
  DESC_FSTCW_C = DESC_STR + DESC_FCW + DESC_TO + DESC_CW + DESC_AF + DESC_CHECK_PUE;
  DESC_FSTENV_14 = DESC_STR + DESC_FEN + DESC_TO + DESC_ENV_14 + DESC_AF + DESC_CHECK_PUE + DESC_MASK;
  DESC_FSTENV_28 = DESC_STR + DESC_FEN + DESC_TO + DESC_ENV_28 + DESC_AF + DESC_CHECK_PUE + DESC_MASK;
  DESC_FSTP_S = DESC_STR + DESC_ST_0 + DESC_TO + DESC_SINGLE + DESC_AN + DESC_POP_FPU;
  DESC_FSTP_D = DESC_STR + DESC_ST_0 + DESC_TO + DESC_DOUBLE + DESC_AN + DESC_POP_FPU;
  DESC_FSTP_E = DESC_STR + DESC_ST_0 + DESC_TO + DESC_EXTEND + DESC_AN + DESC_POP_FPU;
  DESC_FSTP_I = DESC_COP + DESC_ST_0 + DESC_TO + DESC_ST_I + DESC_AN + DESC_POP_FPU;
  DESC_FSTSW_W = DESC_STR + DESC_FSW + DESC_TO + DESC_SW + DESC_AF + DESC_CHECK_PUE;
  DESC_FSTSW_AX = DESC_STR + DESC_FSW + DESC_TO + DESC_AX + DESC_AF + DESC_CHECK_PUE;
  DESC_FSUB_S = DESC_SUB + DESC_SINGLE + DESC_FR + DESC_ST_0 + DESC_AN + DESC_STORE_ST_0;
  DESC_FSUB_D = DESC_SUB + DESC_DOUBLE + DESC_FR + DESC_ST_0 + DESC_AN + DESC_STORE_ST_0;
  DESC_FSUB_T_I = DESC_SUB + DESC_ST_I + DESC_FR + DESC_ST_0 + DESC_AN + DESC_STORE_ST_0;
  DESC_FSUB_I_T = DESC_SUB + DESC_ST_0 + DESC_FR + DESC_ST_I + DESC_AN + DESC_STORE_ST_I;
  DESC_FSUBP = DESC_SUB + DESC_ST_0 + DESC_FR + DESC_ST_1 + DESC_CO + DESC_STORE_ST_1 + DESC_COA + DESC_POP_FPU;
  DESC_FSUBP_I_T = DESC_SUB + DESC_ST_0 + DESC_FR + DESC_ST_I + DESC_CO + DESC_STORE_ST_I + DESC_COA + DESC_POP_FPU;
  DESC_FSUBR_S = DESC_SUB + DESC_ST_0 + DESC_FR + DESC_SINGLE + DESC_AN + DESC_STORE_ST_0;
  DESC_FSUBR_D = DESC_SUB + DESC_ST_0 + DESC_FR + DESC_DOUBLE + DESC_AN + DESC_STORE_ST_0;
  DESC_FSUBR_T_I = DESC_SUB + DESC_ST_0 + DESC_FR + DESC_ST_I + DESC_AN + DESC_STORE_ST_0;
  DESC_FSUBR_I_T = DESC_SUB + DESC_ST_I + DESC_FR + DESC_ST_0 + DESC_AN + DESC_STORE_ST_I;
  DESC_FSUBRP = DESC_SUB + DESC_ST_1 + DESC_FR + DESC_ST_0 + DESC_CO + DESC_STORE_ST_1 + DESC_COA + DESC_POP_FPU;
  DESC_FSUBRP_I_T = DESC_SUB + DESC_ST_I + DESC_FR + DESC_ST_0 + DESC_CO + DESC_STORE_ST_I + DESC_COA + DESC_POP_FPU;
  DESC_FTST = DESC_CMP + DESC_ST_0 + DESC_WI + '0.0' + DESC_AN + DESC_STORE_FSW;
  DESC_FUCOM = DESC_CMP + DESC_ST_0 + DESC_WI + DESC_ST_1 + DESC_CO + DESC_CHECK_O + DESC_COA + DESC_STORE_FSW;
  DESC_FUCOM_I = DESC_CMP + DESC_ST_0 + DESC_WI + DESC_ST_I + DESC_CO + DESC_CHECK_O + DESC_COA + DESC_STORE_FSW;
  DESC_FUCOMI_T_I = DESC_CMP + DESC_ST_0 + DESC_WI + DESC_ST_I + DESC_CO + DESC_CHECK_O + DESC_COA + DESC_STORE_EFL;
  DESC_FUCOMIP_T_I = DESC_CMP + DESC_ST_0 + DESC_WI + DESC_ST_I + DESC_CO + DESC_CHECK_O + DESC_CO + DESC_STORE_EFL + DESC_COA + DESC_POP_FPU;
  DESC_FUCOMP = DESC_CMP + DESC_ST_0 + DESC_WI + DESC_ST_1 + DESC_CO + DESC_CHECK_O + DESC_CO + DESC_STORE_FSW + DESC_COA + DESC_POP_FPU;
  DESC_FUCOMP_I = DESC_CMP + DESC_ST_0 + DESC_WI + DESC_ST_I + DESC_CO + DESC_CHECK_O + DESC_CO + DESC_STORE_FSW + DESC_COA + DESC_POP_FPU;
  DESC_FUCOMPP = DESC_CMP + DESC_ST_0 + DESC_WI + DESC_ST_1 + DESC_CO + DESC_CHECK_O + DESC_CO + DESC_STORE_FSW + DESC_COA + DESC_POP_FPU + DESC_2X;
  DESC_SAHF = DESC_STR + 'AH' + DESC_INT + DESC_EFL;
  DESC_FWAIT = DESC_CHK + DESC_PU_FP_E;
  DESC_FXAM = DESC_CLA + DESC_VAL + DESC_OR + 'number' + DESC_IN + DESC_ST_0;
  DESC_FXCH = DESC_XCH + DESC_CON + DESC_ST_0 + DESC_AN + DESC_ST_1;
  DESC_FXCH_I = DESC_XCH + DESC_CON + DESC_ST_0 + DESC_AN + DESC_ST_I;
  DESC_FXTRACT = DESC_SPR + DESC_VAL + DESC_IN + DESC_ST_0 + DESC_INT + DESC_EXP + DESC_AN + DESC_SIG + DESC_CO + DESC_STO + DESC_EXP + DESC_IN + DESC_ST_0 + DESC_COA + DESC_PUSH + ' ' + DESC_TH + DESC_SIG + DESC_ONT + DESC_FPU_S;
  DESC_FYL2X = DESC_REP + DESC_ST_1 + DESC_WI + '(' + DESC_ST_1 + ' * ' + 'log_2(' + DESC_ST_0 + '))' + DESC_AN + DESC_POP_FPU;
  DESC_FYL2XP1 = DESC_REP + DESC_ST_1 + DESC_WI + '(' + DESC_ST_1 + ' * ' + 'log_2(' + DESC_ST_0 + ' + 1.0))' + DESC_AN + DESC_POP_FPU;
  DESC_JMP = DESC_JM + DESC_TO + DESC_ADDR;
  DESC_JA = DESC_JMP + DESC_A_L;
  DESC_JAE = DESC_JMP + DESC_AE_L;
  DESC_JB = DESC_JMP + DESC_B_L;
  DESC_JBE = DESC_JMP + DESC_BE_L;
  DESC_JC = DESC_JMP + DESC_C_L;
  DESC_JE = DESC_JMP + DESC_E_L;
  DESC_JG = DESC_JMP + DESC_C_L;
  DESC_JGE = DESC_JMP + DESC_GE_L;
  DESC_JL = DESC_JMP + DESC_L_L;
  DESC_JLE = DESC_JMP + DESC_LE_L;
  DESC_JNA = DESC_JMP + DESC_NA_L;
  DESC_JNAE = DESC_JMP + DESC_NAE_L;
  DESC_JNB = DESC_JMP + DESC_NB_L;
  DESC_JNBE = DESC_JMP + DESC_NBE_L;
  DESC_JNC = DESC_JMP + DESC_NC_L;
  DESC_JNE = DESC_JMP + DESC_NE_L;
  DESC_JNG = DESC_JMP + DESC_NG_L;
  DESC_JNGE = DESC_JMP + DESC_NGE_L;
  DESC_JNL = DESC_JMP + DESC_NL_L;
  DESC_JNLE = DESC_JMP + DESC_NLE_L;
  DESC_JNO = DESC_JMP + DESC_NO_L;
  DESC_JNP = DESC_JMP + DESC_NP_L;
  DESC_JNS = DESC_JMP + DESC_NS_L;
  DESC_JNZ = DESC_JMP + DESC_NZ_L;
  DESC_JO = DESC_JMP + DESC_O_L;
  DESC_JP = DESC_JMP + DESC_P_L;
  DESC_JPE = DESC_JMP + DESC_PE_L;
  DESC_JPO = DESC_JMP + DESC_PO_L;
  DESC_JS = DESC_JMP + DESC_S_L;
  DESC_JZ = DESC_JMP + DESC_Z_L;

  { Operands Description }

  DESC_OPER_ERROR = 'Error';
  DESC_OPER_NONE = 'No Operands';
  DESC_OPER_ST_0 = DESC_ST_0;
  DESC_OPER_ST_I = DESC_ST_I;
  DESC_OPER_AX = DESC_AX;
  DESC_OPER_M16_INT = DESC_WORD;
  DESC_OPER_M32_INT = DESC_DWORD;
  DESC_OPER_M64_INT = DESC_QWORD;
  DESC_OPER_ADDR = DESC_ADDR;
  DESC_OPER_M80_BCD = DESC_BCD;
  DESC_OPER_M32_FP = DESC_SINGLE;
  DESC_OPER_M64_FP = DESC_DOUBLE;
  DESC_OPER_M80_FP = DESC_EXTEND;
  DESC_OPER_M2BYTE_CW = DESC_CW;
  DESC_OPER_M2BYTE_SW = DESC_SW;
  DESC_OPER_M14BYTE_ENV = DESC_ENV_14;
  DESC_OPER_M28BYTE_ENV = DESC_ENV_28;
  DESC_OPER_M94BYTE_STAT = DESC_STAT_94;
  DESC_OPER_M108BYTE_STAT = DESC_STAT_108;

  { Operands Default Values }

  DEFV_M16_INT = #$00#$00;
  DEFV_M32_INT = #$00#$00#$00#$00;
  DEFV_M64_INT = #$00#$00#$00#$00#$00#$00#$00#$00;
  DEFV_M80_BCD = #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00;
  DEFV_M32_FP = #$00#$00#$00#$00;
  DEFV_M64_FP = #$00#$00#$00#$00#$00#$00#$00#$00;
  DEFV_M80_FP = #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00;
  DEFV_M2BYTE_CW = #$00#$00;
  DEFV_M2BYTE_SW = #$00#$00;
  DEFV_M14BYTE_ENV = #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00;
  DEFV_M28BYTE_ENV = DEFV_M14BYTE_ENV + DEFV_M14BYTE_ENV;
  DEFV_M94BYTE_STAT = DEFV_M28BYTE_ENV + DEFV_M28BYTE_ENV + DEFV_M28BYTE_ENV + DEFV_M80_BCD;
  DEFV_M108BYTE_STAT = DEFV_M94BYTE_STAT + DEFV_M14BYTE_ENV;

  { Jump Instructions Code Constants }
  JMP_RESULT_1 = #$C7#$44#$24#$04#$01#$00#$00#$00;
  JCC_SHARED = #$02#$EB#$08 + JMP_RESULT_1;

var

  { Instructions Definition }

  sInstructions: array [0..222] of TInstructionRecord = (
    (Name: 'F2XM1' + FP_X; Code: #$D9#$F0; Description: DESC_F2XM1; Branch: BRANCH_NORMAL),
    (Name: 'FABS' + FP_X; Code: #$D9#$E1; Description: DESC_FABS; Branch: BRANCH_NORMAL),
    (Name: 'FADD' + FP_S; Code: #$D8#$00; Description: DESC_FADD_S; Branch: BRANCH_NORMAL),
    (Name: 'FADD' + FP_D; Code: #$DC#$00; Description: DESC_FADD_D; Branch: BRANCH_NORMAL),
    (Name: 'FADD' + FP_T + FP_T; Code: #$D8#$C0; Description: DESC_FADD_T_I; Branch: BRANCH_NORMAL),
    (Name: 'FADD' + FP_T + FP_I; Code: #$D8#$C0; Description: DESC_FADD_T_I; Branch: BRANCH_NORMAL),
    (Name: 'FADD' + FP_I + FP_T; Code: #$DC#$C0; Description: DESC_FADD_I_T; Branch: BRANCH_NORMAL),
    (Name: 'FADDP' + FP_X; Code: #$DE#$C1; Description: DESC_FADDP; Branch: BRANCH_NORMAL),
    (Name: 'FADDP' + FP_T + FP_T; Code: #$DE#$C0; Description: DESC_FADDP_I_T; Branch: BRANCH_NORMAL),
    (Name: 'FADDP' + FP_I + FP_T; Code: #$DE#$C0; Description: DESC_FADDP_I_T; Branch: BRANCH_NORMAL),
    (Name: 'FBLD' + FP_0; Code: #$DF#$20; Description: DESC_FBLD_0; Branch: BRANCH_NORMAL),
    (Name: 'FBSTP' + FP_0; Code: #$DF#$30; Description: DESC_FBSTP_0; Branch: BRANCH_NORMAL),
    (Name: 'FCHS' + FP_X; Code: #$D9#$E0; Description: DESC_FCHS; Branch: BRANCH_NORMAL),
    (Name: 'FCLEX' + FP_X; Code: #$9B#$DB#$E2; Description: DESC_FCLEX; Branch: BRANCH_NORMAL),
    (Name: 'FCMOVB' + FP_T + FP_T; Code: #$DA#$C0; Description: DESC_FCMOVB_T_I; Branch: BRANCH_NORMAL),
    (Name: 'FCMOVB' + FP_T + FP_I; Code: #$DA#$C0; Description: DESC_FCMOVB_T_I; Branch: BRANCH_NORMAL),
    (Name: 'FCMOVBE' + FP_T + FP_T; Code: #$DA#$D0; Description: DESC_FCMOVBE_T_I; Branch: BRANCH_NORMAL),
    (Name: 'FCMOVBE' + FP_T + FP_I; Code: #$DA#$D0; Description: DESC_FCMOVBE_T_I; Branch: BRANCH_NORMAL),
    (Name: 'FCMOVE' + FP_T + FP_T; Code: #$DA#$C8; Description: DESC_FCMOVE_T_I; Branch: BRANCH_NORMAL),
    (Name: 'FCMOVE' + FP_T + FP_I; Code: #$DA#$C8; Description: DESC_FCMOVE_T_I; Branch: BRANCH_NORMAL),
    (Name: 'FCMOVNB' + FP_T + FP_T; Code: #$DB#$C0; Description: DESC_FCMOVNB_T_I; Branch: BRANCH_NORMAL),
    (Name: 'FCMOVNB' + FP_T + FP_I; Code: #$DB#$C0; Description: DESC_FCMOVNB_T_I; Branch: BRANCH_NORMAL),
    (Name: 'FCMOVNBE' + FP_T + FP_T; Code: #$DB#$D0; Description: DESC_FCMOVNBE_T_I; Branch: BRANCH_NORMAL),
    (Name: 'FCMOVNBE' + FP_T + FP_I; Code: #$DB#$D0; Description: DESC_FCMOVNBE_T_I; Branch: BRANCH_NORMAL),
    (Name: 'FCMOVNE' + FP_T + FP_T; Code: #$DB#$C8; Description: DESC_FCMOVNE_T_I; Branch: BRANCH_NORMAL),
    (Name: 'FCMOVNE' + FP_T + FP_I; Code: #$DB#$C8; Description: DESC_FCMOVNE_T_I; Branch: BRANCH_NORMAL),
    (Name: 'FCMOVNU' + FP_T + FP_T; Code: #$DB#$D8; Description: DESC_FCMOVNU_T_I; Branch: BRANCH_NORMAL),
    (Name: 'FCMOVNU' + FP_T + FP_I; Code: #$DB#$D8; Description: DESC_FCMOVNU_T_I; Branch: BRANCH_NORMAL),
    (Name: 'FCMOVU' + FP_T + FP_T; Code: #$DA#$D8; Description: DESC_FCMOVU_T_I; Branch: BRANCH_NORMAL),
    (Name: 'FCMOVU' + FP_T + FP_I; Code: #$DA#$D8; Description: DESC_FCMOVU_T_I; Branch: BRANCH_NORMAL),
    (Name: 'FCOM' + FP_X; Code: #$D8#$D1; Description: DESC_FCOM; Branch: BRANCH_NORMAL),
    (Name: 'FCOM' + FP_S; Code: #$D8#$10; Description: DESC_FCOM_S; Branch: BRANCH_NORMAL),
    (Name: 'FCOM' + FP_D; Code: #$DC#$10; Description: DESC_FCOM_D; Branch: BRANCH_NORMAL),
    (Name: 'FCOM' + FP_T; Code: #$D8#$D0; Description: DESC_FCOM_I; Branch: BRANCH_NORMAL),
    (Name: 'FCOM' + FP_I; Code: #$D8#$D0; Description: DESC_FCOM_I; Branch: BRANCH_NORMAL),
    (Name: 'FCOMI' + FP_T + FP_T; Code: #$DB#$F0; Description: DESC_FCOMI_T_I; Branch: BRANCH_NORMAL),
    (Name: 'FCOMI' + FP_T + FP_I; Code: #$DB#$F0; Description: DESC_FCOMI_T_I; Branch: BRANCH_NORMAL),
    (Name: 'FCOMIP' + FP_T + FP_T; Code: #$DF#$F0; Description: DESC_FCOMIP_T_I; Branch: BRANCH_NORMAL),
    (Name: 'FCOMIP' + FP_T + FP_I; Code: #$DF#$F0; Description: DESC_FCOMIP_T_I; Branch: BRANCH_NORMAL),
    (Name: 'FCOMP' + FP_X; Code: #$D8#$D9; Description: DESC_FCOMP; Branch: BRANCH_NORMAL),
    (Name: 'FCOMP' + FP_S; Code: #$D8#$18; Description: DESC_FCOMP_S; Branch: BRANCH_NORMAL),
    (Name: 'FCOMP' + FP_D; Code: #$DC#$18; Description: DESC_FCOMP_D; Branch: BRANCH_NORMAL),
    (Name: 'FCOMP' + FP_T; Code: #$D8#$D8; Description: DESC_FCOMP_I; Branch: BRANCH_NORMAL),
    (Name: 'FCOMP' + FP_I; Code: #$D8#$D8; Description: DESC_FCOMP_I; Branch: BRANCH_NORMAL),
    (Name: 'FCOMPP' + FP_X; Code: #$DE#$D9; Description: DESC_FCOMPP; Branch: BRANCH_NORMAL),
    (Name: 'FCOS' + FP_X; Code: #$D9#$FF; Description: DESC_FCOS; Branch: BRANCH_NORMAL),
    (Name: 'FDECSTP' + FP_X; Code: #$D9#$F6; Description: DESC_FDECSTP; Branch: BRANCH_NORMAL),
    (Name: 'FDIV' + FP_S; Code: #$D8#$30; Description: DESC_FDIV_S; Branch: BRANCH_NORMAL),
    (Name: 'FDIV' + FP_D; Code: #$DC#$30; Description: DESC_FDIV_D; Branch: BRANCH_NORMAL),
    (Name: 'FDIV' + FP_T + FP_T; Code: #$D8#$F0; Description: DESC_FDIV_T_I; Branch: BRANCH_NORMAL),
    (Name: 'FDIV' + FP_T + FP_I; Code: #$D8#$F0; Description: DESC_FDIV_T_I; Branch: BRANCH_NORMAL),
    (Name: 'FDIV' + FP_I + FP_T; Code: #$DC#$F8; Description: DESC_FDIV_I_T; Branch: BRANCH_NORMAL),
    (Name: 'FDIVP' + FP_X; Code: #$DE#$F9; Description: DESC_FDIVP; Branch: BRANCH_NORMAL),
    (Name: 'FDIVP' + FP_T + FP_T; Code: #$DE#$F8; Description: DESC_FDIVP_I_T; Branch: BRANCH_NORMAL),
    (Name: 'FDIVP' + FP_I + FP_T; Code: #$DE#$F8; Description: DESC_FDIVP_I_T; Branch: BRANCH_NORMAL),
    (Name: 'FDIVR' + FP_S; Code: #$D8#$38; Description: DESC_FDIVR_S; Branch: BRANCH_NORMAL),
    (Name: 'FDIVR' + FP_D; Code: #$DC#$38; Description: DESC_FDIVR_D; Branch: BRANCH_NORMAL),
    (Name: 'FDIVR' + FP_T + FP_T; Code: #$D8#$F8; Description: DESC_FDIVR_T_I; Branch: BRANCH_NORMAL),
    (Name: 'FDIVR' + FP_T + FP_I; Code: #$D8#$F8; Description: DESC_FDIVR_T_I; Branch: BRANCH_NORMAL),
    (Name: 'FDIVR' + FP_I + FP_T; Code: #$DC#$F0; Description: DESC_FDIVR_I_T; Branch: BRANCH_NORMAL),
    (Name: 'FDIVRP' + FP_X; Code: #$DE#$F1; Description: DESC_FDIVRP; Branch: BRANCH_NORMAL),
    (Name: 'FDIVRP' + FP_T + FP_T; Code: #$DE#$F0; Description: DESC_FDIVRP_I_T; Branch: BRANCH_NORMAL),
    (Name: 'FDIVRP' + FP_I + FP_T; Code: #$DE#$F0; Description: DESC_FDIVRP_I_T; Branch: BRANCH_NORMAL),
    (Name: 'FFREE' + FP_T; Code: #$DD#$C0; Description: DESC_FFREE_I; Branch: BRANCH_NORMAL),
    (Name: 'FFREE' + FP_I; Code: #$DD#$C0; Description: DESC_FFREE_I; Branch: BRANCH_NORMAL),
    (Name: 'FIADD' + FP_2; Code: #$DE#$00; Description: DESC_FIADD_2; Branch: BRANCH_NORMAL),
    (Name: 'FIADD' + FP_4; Code: #$DA#$00; Description: DESC_FIADD_4; Branch: BRANCH_NORMAL),
    (Name: 'FICOM' + FP_2; Code: #$DE#$10; Description: DESC_FICOM_2; Branch: BRANCH_NORMAL),
    (Name: 'FICOM' + FP_4; Code: #$DA#$10; Description: DESC_FICOM_4; Branch: BRANCH_NORMAL),
    (Name: 'FICOMP' + FP_2; Code: #$DE#$18; Description: DESC_FICOMP_2; Branch: BRANCH_NORMAL),
    (Name: 'FICOMP' + FP_4; Code: #$DA#$18; Description: DESC_FICOMP_4; Branch: BRANCH_NORMAL),
    (Name: 'FIDIV' + FP_2; Code: #$DE#$30; Description: DESC_FIDIV_2; Branch: BRANCH_NORMAL),
    (Name: 'FIDIV' + FP_4; Code: #$DA#$30; Description: DESC_FIDIV_4; Branch: BRANCH_NORMAL),
    (Name: 'FIDIVR' + FP_2; Code: #$DE#$38; Description: DESC_FIDIVR_2; Branch: BRANCH_NORMAL),
    (Name: 'FIDIVR' + FP_4; Code: #$DA#$38; Description: DESC_FIDIVR_4; Branch: BRANCH_NORMAL),
    (Name: 'FILD' + FP_2; Code: #$DF#$00; Description: DESC_FILD_2; Branch: BRANCH_NORMAL),
    (Name: 'FILD' + FP_4; Code: #$DB#$00; Description: DESC_FILD_4; Branch: BRANCH_NORMAL),
    (Name: 'FILD' + FP_8; Code: #$DF#$28; Description: DESC_FILD_8; Branch: BRANCH_NORMAL),
    (Name: 'FINCSTP' + FP_X; Code: #$D9#$F7; Description: DESC_FINCSTP; Branch: BRANCH_NORMAL),
    (Name: 'FINIT' + FP_X; Code: #$9B#$DB#$E3; Description: DESC_FINIT; Branch: BRANCH_NORMAL),
    (Name: 'FIST' + FP_2; Code: #$DF#$10; Description: DESC_FIST_2; Branch: BRANCH_NORMAL),
    (Name: 'FIST' + FP_4; Code: #$DB#$10; Description: DESC_FIST_4; Branch: BRANCH_NORMAL),
    (Name: 'FISTP' + FP_2; Code: #$DF#$18; Description: DESC_FISTP_2; Branch: BRANCH_NORMAL),
    (Name: 'FISTP' + FP_4; Code: #$DB#$18; Description: DESC_FISTP_4; Branch: BRANCH_NORMAL),
    (Name: 'FISTP' + FP_8; Code: #$DF#$38; Description: DESC_FISTP_8; Branch: BRANCH_NORMAL),
    (Name: 'FISTTP' + FP_2; Code: #$DF#$08; Description: DESC_FISTTP_2; Branch: BRANCH_NORMAL),
    (Name: 'FISTTP' + FP_4; Code: #$DB#$08; Description: DESC_FISTTP_4; Branch: BRANCH_NORMAL),
    (Name: 'FISTTP' + FP_8; Code: #$DD#$08; Description: DESC_FISTTP_8; Branch: BRANCH_NORMAL),
    (Name: 'FISUB' + FP_2; Code: #$DE#$20; Description: DESC_FISUB_2; Branch: BRANCH_NORMAL),
    (Name: 'FISUB' + FP_4; Code: #$DA#$20; Description: DESC_FISUB_4; Branch: BRANCH_NORMAL),
    (Name: 'FISUBR' + FP_2; Code: #$DE#$28; Description: DESC_FISUBR_2; Branch: BRANCH_NORMAL),
    (Name: 'FISUBR' + FP_4; Code: #$DA#$28; Description: DESC_FISUBR_4; Branch: BRANCH_NORMAL),
    (Name: 'FLD' + FP_S; Code: #$D9#$00; Description: DESC_FLD_S; Branch: BRANCH_NORMAL),
    (Name: 'FLD' + FP_D; Code: #$DD#$00; Description: DESC_FLD_D; Branch: BRANCH_NORMAL),
    (Name: 'FLD' + FP_E; Code: #$DB#$28; Description: DESC_FLD_E; Branch: BRANCH_NORMAL),
    (Name: 'FLD' + FP_T; Code: #$D9#$C0; Description: DESC_FLD_I; Branch: BRANCH_NORMAL),
    (Name: 'FLD' + FP_I; Code: #$D9#$C0; Description: DESC_FLD_I; Branch: BRANCH_NORMAL),
    (Name: 'FLD1' + FP_X; Code: #$D9#$E8; Description: DESC_FLD1; Branch: BRANCH_NORMAL),
    (Name: 'FLDCW' + FP_C; Code: #$D9#$28; Description: DESC_FLDCW_C; Branch: BRANCH_NORMAL),
    (Name: 'FLDENV' + FP_14; Code: #$66#$D9#$20; Description: DESC_FLDENV_14; Branch: BRANCH_NORMAL),
    (Name: 'FLDENV' + FP_28; Code: #$D9#$20; Description: DESC_FLDENV_28; Branch: BRANCH_NORMAL),
    (Name: 'FLDL2E' + FP_X; Code: #$D9#$EA; Description: DESC_FLDL2E; Branch: BRANCH_NORMAL),
    (Name: 'FLDL2T' + FP_X; Code: #$D9#$E9; Description: DESC_FLDL2T; Branch: BRANCH_NORMAL),
    (Name: 'FLDLG2' + FP_X; Code: #$D9#$EC; Description: DESC_FLDLG2; Branch: BRANCH_NORMAL),
    (Name: 'FLDLN2' + FP_X; Code: #$D9#$ED; Description: DESC_FLDLN2; Branch: BRANCH_NORMAL),
    (Name: 'FLDPI' + FP_X; Code: #$D9#$EB; Description: DESC_FLDPI; Branch: BRANCH_NORMAL),
    (Name: 'FLDZ' + FP_X; Code: #$D9#$EE; Description: DESC_FLDZ; Branch: BRANCH_NORMAL),
    (Name: 'FMUL' + FP_S; Code: #$D8#$08; Description: DESC_FMUL_S; Branch: BRANCH_NORMAL),
    (Name: 'FMUL' + FP_D; Code: #$DC#$08; Description: DESC_FMUL_D; Branch: BRANCH_NORMAL),
    (Name: 'FMUL' + FP_T + FP_T; Code: #$D8#$C8; Description: DESC_FMUL_T_I; Branch: BRANCH_NORMAL),
    (Name: 'FMUL' + FP_T + FP_I; Code: #$D8#$C8; Description: DESC_FMUL_T_I; Branch: BRANCH_NORMAL),
    (Name: 'FMUL' + FP_I + FP_T; Code: #$DC#$C8; Description: DESC_FMUL_I_T; Branch: BRANCH_NORMAL),
    (Name: 'FMULP' + FP_T + FP_T; Code: #$DE#$C8; Description: DESC_FMULP_I_T; Branch: BRANCH_NORMAL),
    (Name: 'FMULP' + FP_I + FP_T; Code: #$DE#$C8; Description: DESC_FMULP_I_T; Branch: BRANCH_NORMAL),
    (Name: 'FMULP' + FP_X; Code: #$DE#$C9; Description: DESC_FMULP; Branch: BRANCH_NORMAL),
    (Name: 'FIMUL' + FP_2; Code: #$DE#$08; Description: DESC_FIMUL_2; Branch: BRANCH_NORMAL),
    (Name: 'FIMUL' + FP_4; Code: #$DA#$08; Description: DESC_FIMUL_4; Branch: BRANCH_NORMAL),
    (Name: 'FNCLEX' + FP_X; Code: #$DB#$E2; Description: DESC_FNCLEX; Branch: BRANCH_NORMAL),
    (Name: 'FNINIT' + FP_X; Code: #$DB#$E3; Description: DESC_FNINIT; Branch: BRANCH_NORMAL),
    (Name: 'FNOP' + FP_X; Code: #$D9#$D0; Description: DESC_FNOP; Branch: BRANCH_NORMAL),
    (Name: 'FNSAVE' + FP_94; Code: #$66#$DD#$30; Description: DESC_FNSAVE_94; Branch: BRANCH_NORMAL),
    (Name: 'FNSAVE' + FP_108; Code: #$DD#$30; Description: DESC_FNSAVE_108; Branch: BRANCH_NORMAL),
    (Name: 'FNSTCW' + FP_C; Code: #$D9#$38; Description: DESC_FNSTCW_C; Branch: BRANCH_NORMAL),
    (Name: 'FNSTENV' + FP_14; Code: #$66#$D9#$30; Description: DESC_FNSTENV_14; Branch: BRANCH_NORMAL),
    (Name: 'FNSTENV' + FP_28; Code: #$D9#$30; Description: DESC_FNSTENV_28; Branch: BRANCH_NORMAL),
    (Name: 'FNSTSW' + FP_W; Code: #$DD#$38; Description: DESC_FNSTSW_W; Branch: BRANCH_NORMAL),
    (Name: 'FNSTSW' + FP_AX; Code: #$DF#$E0; Description: DESC_FNSTSW_AX; Branch: BRANCH_NORMAL),
    (Name: 'FPATAN' + FP_X; Code: #$D9#$F3; Description: DESC_FPATAN; Branch: BRANCH_NORMAL),
    (Name: 'FPREM' + FP_X; Code: #$D9#$F8; Description: DESC_FPREM; Branch: BRANCH_NORMAL),
    (Name: 'FPREM1' + FP_X; Code: #$D9#$F5; Description: DESC_FPREM1; Branch: BRANCH_NORMAL),
    (Name: 'FPTAN' + FP_X; Code: #$D9#$F2; Description: DESC_FPTAN; Branch: BRANCH_NORMAL),
    (Name: 'FRNDINT' + FP_X; Code: #$D9#$FC; Description: DESC_FRNDINT; Branch: BRANCH_NORMAL),
    (Name: 'FRSTOR' + FP_94; Code: #$66#$DD#$20; Description: DESC_FRSTOR_94; Branch: BRANCH_NORMAL),
    (Name: 'FRSTOR' + FP_108; Code: #$DD#$20; Description: DESC_FRSTOR_108; Branch: BRANCH_NORMAL),
    (Name: 'FSAVE' + FP_94; Code: #$9B#$66#$DD#$30; Description: DESC_FSAVE_94; Branch: BRANCH_NORMAL),
    (Name: 'FSAVE' + FP_108; Code: #$9B#$DD#$30; Description: DESC_FSAVE_108; Branch: BRANCH_NORMAL),
    (Name: 'FSCALE' + FP_X; Code: #$D9#$FD; Description: DESC_FSCALE; Branch: BRANCH_NORMAL),
    (Name: 'FSIN' + FP_X; Code: #$D9#$FE; Description: DESC_FSIN; Branch: BRANCH_NORMAL),
    (Name: 'FSINCOS' + FP_X; Code: #$D9#$FB; Description: DESC_FSINCOS; Branch: BRANCH_NORMAL),
    (Name: 'FSQRT' + FP_X; Code: #$D9#$FA; Description: DESC_FSQRT; Branch: BRANCH_NORMAL),
    (Name: 'FST' + FP_S; Code: #$D9#$10; Description: DESC_FST_S; Branch: BRANCH_NORMAL),
    (Name: 'FST' + FP_D; Code: #$DD#$10; Description: DESC_FST_D; Branch: BRANCH_NORMAL),
    (Name: 'FST' + FP_T; Code: #$DD#$D0; Description: DESC_FST_I; Branch: BRANCH_NORMAL),
    (Name: 'FST' + FP_I; Code: #$DD#$D0; Description: DESC_FST_I; Branch: BRANCH_NORMAL),
    (Name: 'FSTCW' + FP_C; Code: #$9B#$D9#$38; Description: DESC_FSTCW_C; Branch: BRANCH_NORMAL),
    (Name: 'FSTENV' + FP_14; Code: #$9B#$66#$D9#$30; Description: DESC_FSTENV_14; Branch: BRANCH_NORMAL),
    (Name: 'FSTENV' + FP_28; Code: #$9B#$D9#$30; Description: DESC_FSTENV_28; Branch: BRANCH_NORMAL),
    (Name: 'FSTP' + FP_S; Code: #$D9#$18; Description: DESC_FSTP_S; Branch: BRANCH_NORMAL),
    (Name: 'FSTP' + FP_D; Code: #$DD#$18; Description: DESC_FSTP_D; Branch: BRANCH_NORMAL),
    (Name: 'FSTP' + FP_E; Code: #$DB#$38; Description: DESC_FSTP_E; Branch: BRANCH_NORMAL),
    (Name: 'FSTP' + FP_T; Code: #$DD#$D8; Description: DESC_FSTP_I; Branch: BRANCH_NORMAL),
    (Name: 'FSTP' + FP_I; Code: #$DD#$D8; Description: DESC_FSTP_I; Branch: BRANCH_NORMAL),
    (Name: 'FSTSW' + FP_W; Code: #$9B#$DD#$38; Description: DESC_FSTSW_W; Branch: BRANCH_NORMAL),
    (Name: 'FSTSW' + FP_AX; Code: #$9B#$DF#$E0; Description: DESC_FSTSW_AX; Branch: BRANCH_NORMAL),
    (Name: 'FSUB' + FP_S; Code: #$D8#$20; Description: DESC_FSUB_S; Branch: BRANCH_NORMAL),
    (Name: 'FSUB' + FP_D; Code: #$DC#$20; Description: DESC_FSUB_D; Branch: BRANCH_NORMAL),
    (Name: 'FSUB' + FP_T + FP_T; Code: #$D8#$E0; Description: DESC_FSUB_T_I; Branch: BRANCH_NORMAL),
    (Name: 'FSUB' + FP_T + FP_I; Code: #$D8#$E0; Description: DESC_FSUB_T_I; Branch: BRANCH_NORMAL),
    (Name: 'FSUB' + FP_I + FP_T; Code: #$DC#$E8; Description: DESC_FSUB_I_T; Branch: BRANCH_NORMAL),
    (Name: 'FSUBP' + FP_T + FP_T; Code: #$DE#$E8; Description: DESC_FSUBP_I_T; Branch: BRANCH_NORMAL),
    (Name: 'FSUBP' + FP_I + FP_T; Code: #$DE#$E8; Description: DESC_FSUBP_I_T; Branch: BRANCH_NORMAL),
    (Name: 'FSUBP' + FP_X; Code: #$DE#$E9; Description: DESC_FSUBP; Branch: BRANCH_NORMAL),
    (Name: 'FSUBR' + FP_S; Code: #$D8#$28; Description: DESC_FSUBR_S; Branch: BRANCH_NORMAL),
    (Name: 'FSUBR' + FP_D; Code: #$DC#$28; Description: DESC_FSUBR_D; Branch: BRANCH_NORMAL),
    (Name: 'FSUBR' + FP_T + FP_T; Code: #$D8#$E8; Description: DESC_FSUBR_T_I; Branch: BRANCH_NORMAL),
    (Name: 'FSUBR' + FP_T + FP_I; Code: #$D8#$E8; Description: DESC_FSUBR_T_I; Branch: BRANCH_NORMAL),
    (Name: 'FSUBR' + FP_I + FP_T; Code: #$DC#$E0; Description: DESC_FSUBR_I_T; Branch: BRANCH_NORMAL),
    (Name: 'FSUBRP' + FP_X; Code: #$DE#$E1; Description: DESC_FSUBRP; Branch: BRANCH_NORMAL),
    (Name: 'FSUBRP' + FP_T + FP_T; Code: #$DE#$E0; Description: DESC_FSUBRP_I_T; Branch: BRANCH_NORMAL),
    (Name: 'FSUBRP' + FP_I + FP_T; Code: #$DE#$E0; Description: DESC_FSUBRP_I_T; Branch: BRANCH_NORMAL),
    (Name: 'FTST' + FP_X; Code: #$D9#$E4; Description: DESC_FTST; Branch: BRANCH_NORMAL),
    (Name: 'FUCOM' + FP_X; Code: #$DD#$E1; Description: DESC_FUCOM; Branch: BRANCH_NORMAL),
    (Name: 'FUCOM' + FP_T; Code: #$DD#$E0; Description: DESC_FUCOM_I; Branch: BRANCH_NORMAL),
    (Name: 'FUCOM' + FP_I; Code: #$DD#$E0; Description: DESC_FUCOM_I; Branch: BRANCH_NORMAL),
    (Name: 'FUCOMI' + FP_T + FP_T; Code: #$DB#$E8; Description: DESC_FUCOMI_T_I; Branch: BRANCH_NORMAL),
    (Name: 'FUCOMI' + FP_T + FP_I; Code: #$DB#$E8; Description: DESC_FUCOMI_T_I; Branch: BRANCH_NORMAL),
    (Name: 'FUCOMIP' + FP_T + FP_T; Code: #$DF#$E8; Description: DESC_FUCOMIP_T_I; Branch: BRANCH_NORMAL),
    (Name: 'FUCOMIP' + FP_T + FP_I; Code: #$DF#$E8; Description: DESC_FUCOMIP_T_I; Branch: BRANCH_NORMAL),
    (Name: 'FUCOMP' + FP_X; Code: #$DD#$E9; Description: DESC_FUCOMP; Branch: BRANCH_NORMAL),
    (Name: 'FUCOMP' + FP_T; Code: #$DD#$E8; Description: DESC_FUCOMP_I; Branch: BRANCH_NORMAL),
    (Name: 'FUCOMP' + FP_I; Code: #$DD#$E8; Description: DESC_FUCOMP_I; Branch: BRANCH_NORMAL),
    (Name: 'FUCOMPP' + FP_X; Code: #$DA#$E9; Description: DESC_FUCOMPP; Branch: BRANCH_NORMAL),
    (Name: 'FWAIT' + FP_X; Code: #$9B; Description: DESC_FWAIT; Branch: BRANCH_NORMAL),
    (Name: 'FXAM' + FP_X; Code: #$D9#$E5; Description: DESC_FXAM; Branch: BRANCH_NORMAL),
    (Name: 'FXCH' + FP_X; Code: #$D9#$C9; Description: DESC_FXCH; Branch: BRANCH_NORMAL),
    (Name: 'FXCH' + FP_T; Code: #$D9#$C8; Description: DESC_FXCH_I; Branch: BRANCH_NORMAL),
    (Name: 'FXCH' + FP_I; Code: #$D9#$C8; Description: DESC_FXCH_I; Branch: BRANCH_NORMAL),
    (Name: 'FXTRACT' + FP_X; Code: #$D9#$F4; Description: DESC_FXTRACT; Branch: BRANCH_NORMAL),
    (Name: 'FYL2X' + FP_X; Code: #$D9#$F1; Description: DESC_FYL2X; Branch: BRANCH_NORMAL),
    (Name: 'FYL2XP1' + FP_X; Code: #$D9#$F9; Description: DESC_FYL2XP1; Branch: BRANCH_NORMAL),
    (Name: 'JA' + FP_ADDR; Code: #$77 + JCC_SHARED; Description: DESC_JA; Branch: BRANCH_BRANCH),
    (Name: 'JAE' + FP_ADDR; Code: #$73 + JCC_SHARED; Description: DESC_JAE; Branch: BRANCH_BRANCH),
    (Name: 'JB' + FP_ADDR; Code: #$72 + JCC_SHARED; Description: DESC_JB; Branch: BRANCH_BRANCH),
    (Name: 'JBE' + FP_ADDR; Code: #$76 + JCC_SHARED; Description: DESC_JBE; Branch: BRANCH_BRANCH),
    (Name: 'JC' + FP_ADDR; Code: #$72 + JCC_SHARED; Description: DESC_JC; Branch: BRANCH_BRANCH),
    (Name: 'JE' + FP_ADDR; Code: #$74 + JCC_SHARED; Description: DESC_JE; Branch: BRANCH_BRANCH),
    (Name: 'JG' + FP_ADDR; Code: #$7F + JCC_SHARED; Description: DESC_JG; Branch: BRANCH_BRANCH),
    (Name: 'JGE' + FP_ADDR; Code: #$7D + JCC_SHARED; Description: DESC_JGE; Branch: BRANCH_BRANCH),
    (Name: 'JL' + FP_ADDR; Code: #$7C + JCC_SHARED; Description: DESC_JL; Branch: BRANCH_BRANCH),
    (Name: 'JLE' + FP_ADDR; Code: #$7E + JCC_SHARED; Description: DESC_JLE; Branch: BRANCH_BRANCH),
    (Name: 'JMP' + FP_ADDR; Code: JMP_RESULT_1; Description: DESC_JMP; Branch: BRANCH_BRANCH),
    (Name: 'JNA' + FP_ADDR; Code: #$76 + JCC_SHARED; Description: DESC_JNA; Branch: BRANCH_BRANCH),
    (Name: 'JNAE' + FP_ADDR; Code: #$72 + JCC_SHARED; Description: DESC_JNAE; Branch: BRANCH_BRANCH),
    (Name: 'JNB' + FP_ADDR; Code: #$73 + JCC_SHARED; Description: DESC_JNB; Branch: BRANCH_BRANCH),
    (Name: 'JNBE' + FP_ADDR; Code: #$77 + JCC_SHARED; Description: DESC_JNBE; Branch: BRANCH_BRANCH),
    (Name: 'JNC' + FP_ADDR; Code: #$73 + JCC_SHARED; Description: DESC_JNC; Branch: BRANCH_BRANCH),
    (Name: 'JNE' + FP_ADDR; Code: #$75 + JCC_SHARED; Description: DESC_JNE; Branch: BRANCH_BRANCH),
    (Name: 'JNG' + FP_ADDR; Code: #$7E + JCC_SHARED; Description: DESC_JNG; Branch: BRANCH_BRANCH),
    (Name: 'JNGE' + FP_ADDR; Code: #$7C + JCC_SHARED; Description: DESC_JNGE; Branch: BRANCH_BRANCH),
    (Name: 'JNL' + FP_ADDR; Code: #$7D + JCC_SHARED; Description: DESC_JNL; Branch: BRANCH_BRANCH),
    (Name: 'JNLE' + FP_ADDR; Code: #$7F + JCC_SHARED; Description: DESC_JNLE; Branch: BRANCH_BRANCH),
    (Name: 'JNO' + FP_ADDR; Code: #$71 + JCC_SHARED; Description: DESC_JNO; Branch: BRANCH_BRANCH),
    (Name: 'JNP' + FP_ADDR; Code: #$7B + JCC_SHARED; Description: DESC_JNP; Branch: BRANCH_BRANCH),
    (Name: 'JNS' + FP_ADDR; Code: #$79 + JCC_SHARED; Description: DESC_JNS; Branch: BRANCH_BRANCH),
    (Name: 'JNZ' + FP_ADDR; Code: #$75 + JCC_SHARED; Description: DESC_JNZ; Branch: BRANCH_BRANCH),
    (Name: 'JO' + FP_ADDR; Code: #$70 + JCC_SHARED; Description: DESC_JO; Branch: BRANCH_BRANCH),
    (Name: 'JP' + FP_ADDR; Code: #$7A + JCC_SHARED; Description: DESC_JP; Branch: BRANCH_BRANCH),
    (Name: 'JPE' + FP_ADDR; Code: #$7A + JCC_SHARED; Description: DESC_JPE; Branch: BRANCH_BRANCH),
    (Name: 'JPO' + FP_ADDR; Code: #$7B + JCC_SHARED; Description: DESC_JPO; Branch: BRANCH_BRANCH),
    (Name: 'JS' + FP_ADDR; Code: #$78 + JCC_SHARED; Description: DESC_JS; Branch: BRANCH_BRANCH),
    (Name: 'JZ' + FP_ADDR; Code: #$74 + JCC_SHARED; Description: DESC_JZ; Branch: BRANCH_BRANCH),
    (Name: 'SAHF' + FP_X; Code: #$9E; Description: DESC_SAHF; Branch: BRANCH_NORMAL),
    (Name: 'WAIT' + FP_X; Code: #$9B; Description: DESC_FWAIT; Branch: BRANCH_NORMAL)
  );

  { Operands Definition }

  sOperands: array [0..9] of TOperandRecord = (
    (Name: 'ST'; Code: ''; OperandType: FPU_OPERAND_ST_0; Default: ''; Data: nil),
    (Name: 'ST(0)'; Code: ''; OperandType: FPU_OPERAND_ST_0; Default: ''; Data: nil),
    (Name: 'ST(1)'; Code: #$01; OperandType: FPU_OPERAND_ST_I; Default: ''; Data: nil),
    (Name: 'ST(2)'; Code: #$02; OperandType: FPU_OPERAND_ST_I; Default: ''; Data: nil),
    (Name: 'ST(3)'; Code: #$03; OperandType: FPU_OPERAND_ST_I; Default: ''; Data: nil),
    (Name: 'ST(4)'; Code: #$04; OperandType: FPU_OPERAND_ST_I; Default: ''; Data: nil),
    (Name: 'ST(5)'; Code: #$05; OperandType: FPU_OPERAND_ST_I; Default: ''; Data: nil),
    (Name: 'ST(6)'; Code: #$06; OperandType: FPU_OPERAND_ST_I; Default: ''; Data: nil),
    (Name: 'ST(7)'; Code: #$07; OperandType: FPU_OPERAND_ST_I; Default: ''; Data: nil),
    (Name: 'AX'; Code: ''; OperandType: FPU_OPERAND_AX; Default: ''; Data: nil)
  );

  { Operands Ordinals }

  sOperandOrdinals: array [0..12] of TOperandRecord = (
    (Name: DESC_OPER_M16_INT; Code: #$05; OperandType: FPU_OPERAND_M16_INT; Default: DEFV_M16_INT; Data: nil),
    (Name: DESC_OPER_M32_INT; Code: #$05; OperandType: FPU_OPERAND_M32_INT; Default: DEFV_M32_INT; Data: nil),
    (Name: DESC_OPER_M64_INT; Code: #$05; OperandType: FPU_OPERAND_M64_INT; Default: DEFV_M64_INT; Data: nil),
    (Name: DESC_OPER_M80_BCD; Code: #$05; OperandType: FPU_OPERAND_M80_BCD; Default: DEFV_M80_BCD; Data: nil),
    (Name: DESC_OPER_M32_FP; Code: #$05; OperandType: FPU_OPERAND_M32_FP; Default: DEFV_M32_FP; Data: nil),
    (Name: DESC_OPER_M64_FP; Code: #$05; OperandType: FPU_OPERAND_M64_FP; Default: DEFV_M64_FP; Data: nil),
    (Name: DESC_OPER_M80_FP; Code: #$05; OperandType: FPU_OPERAND_M80_FP; Default: DEFV_M80_FP; Data: nil),
    (Name: DESC_OPER_M2BYTE_CW; Code: #$05; OperandType: FPU_OPERAND_M2BYTE_CW; Default: DEFV_M2BYTE_CW; Data: nil),
    (Name: DESC_OPER_M2BYTE_SW; Code: #$05; OperandType: FPU_OPERAND_M2BYTE_SW; Default: DEFV_M2BYTE_SW; Data: nil),
    (Name: DESC_OPER_M14BYTE_ENV; Code: #$05; OperandType: FPU_OPERAND_M14BYTE_ENV; Default: DEFV_M14BYTE_ENV; Data: nil),
    (Name: DESC_OPER_M28BYTE_ENV; Code: #$05; OperandType: FPU_OPERAND_M28BYTE_ENV; Default: DEFV_M28BYTE_ENV; Data: nil),
    (Name: DESC_OPER_M94BYTE_STAT; Code: #$05; OperandType: FPU_OPERAND_M94BYTE_STAT; Default: DEFV_M94BYTE_STAT; Data: nil),
    (Name: DESC_OPER_M108BYTE_STAT; Code: #$05; OperandType: FPU_OPERAND_M108BYTE_STAT; Default: DEFV_M108BYTE_STAT; Data: nil)
  );

  { Operands Type Definition }

  sOperandTypes: array [0..18] of TOperandTypeRecord = (
    (OperandType: FPU_OPERAND_ERROR; Description: DESC_OPER_ERROR),
    (OperandType: FPU_OPERAND_NONE; Description: DESC_OPER_NONE),
    (OperandType: FPU_OPERAND_ST_0; Description: DESC_OPER_ST_0),
    (OperandType: FPU_OPERAND_ST_I; Description: DESC_OPER_ST_I),
    (OperandType: FPU_OPERAND_AX; Description: DESC_OPER_AX),
    (OperandType: FPU_OPERAND_M16_INT; Description: DESC_OPER_M16_INT),
    (OperandType: FPU_OPERAND_M32_INT; Description: DESC_OPER_M32_INT),
    (OperandType: FPU_OPERAND_M64_INT; Description: DESC_OPER_M64_INT),
    (OperandType: FPU_OPERAND_ADDR; Description: DESC_OPER_ADDR),
    (OperandType: FPU_OPERAND_M80_BCD; Description: DESC_OPER_M80_BCD),
    (OperandType: FPU_OPERAND_M32_FP; Description: DESC_OPER_M32_FP),
    (OperandType: FPU_OPERAND_M64_FP; Description: DESC_OPER_M64_FP),
    (OperandType: FPU_OPERAND_M80_FP; Description: DESC_OPER_M80_FP),
    (OperandType: FPU_OPERAND_M2BYTE_CW; Description: DESC_OPER_M2BYTE_CW),
    (OperandType: FPU_OPERAND_M2BYTE_SW; Description: DESC_OPER_M2BYTE_SW),
    (OperandType: FPU_OPERAND_M14BYTE_ENV; Description: DESC_OPER_M14BYTE_ENV),
    (OperandType: FPU_OPERAND_M28BYTE_ENV; Description: DESC_OPER_M28BYTE_ENV),
    (OperandType: FPU_OPERAND_M94BYTE_STAT; Description: DESC_OPER_M94BYTE_STAT),
    (OperandType: FPU_OPERAND_M108BYTE_STAT; Description: DESC_OPER_M108BYTE_STAT)
  );

  { CPU Features Flags }

  CPU_SSE3_PRESENT: Boolean;

implementation

  procedure CheckCPUFeatures;
  var
    i: Integer;
  begin
    asm
      mov eax, 1
      cpuid
      and ecx, 1
      mov CPU_SSE3_PRESENT, cl
    end;
    if not CPU_SSE3_PRESENT then
      for i := 0 to (Length(sInstructions) - 1) do
        if (ParseBeforeFirst(sInstructions[i].Name, FPU_SPACE) = 'FISTTP') then
          with sInstructions[i] do
          begin
            Description := Description + DESC_DO + INST_INST_UNSUPPORTED;
            Branch := BRANCH_UNSUPPORTED;
          end;
  end;

initialization
  CheckCPUFeatures;

end.
