unit FunctionsClass;

interface

uses
  ConstantsClass, TypesClass,
  SysUtils;

  function AddressToString(Address: Pointer): String;
  function CartesianOfStrings(First: String; Second: TStrings): TStrings;
  function CodeToHexadecimal(Input: String): String;
  function ContainsCharacter(Input: String; Character: Char): Boolean;
  function CustomBCDToStr(Input: PChar; Size: Integer): String;
  function CustomDataToHex(Input: PChar; Size: Integer): String;
  function CustomFloatToStr(Input: PChar; Size: Integer): String;
  function CustomSIntToStr(Input: PChar; Size: Integer): String;
  function CustomUIntToStr(Input: PChar; Size: Integer): String;
  function EqualBeforeFirstParser(First, Second: String; Parser: Char): Boolean;
  function IsAddress(Input: String): Boolean;
  function IsHexaChar(Input: Char): Boolean;
  function FloatType(Input: PChar; Size: Integer): Integer;
  function IsPrefixOf(Prefix, OfWhat: String): Boolean;
  function MaxOf(First, Second: Integer): Integer;
  function MergeStringTStrings(First: String; Second: TStrings): TStrings;
  function MergeTLinesLine(First: TLines; Second: TLine): TLines;
  function MergeTStringsString(First: TStrings; Second: String): TStrings;
  function MinOf(First, Second: Integer): Integer;
  function MirrorString(Input: String): String;
  function NeutralizeDoubles(Input: String; Character: Char): String;
  function OmmitAfter(Input: String; Character, What: Char): String;
  function OmmitBefore(Input: String; Character, What: Char): String;
  function OmmitEverywhere(Input: String; Character, What: Char): String;
  function ParseAfterFirst(Input: String; Parser: Char): String;
  function ParseAfterLast(Input: String; Parser: Char): String;
  function ParseBeforeFirst(Input: String; Parser: Char): String;
  function ParseBeforeLast(Input: String; Parser: Char): String;
  function ParseFirst(var Input: String; Parser: Char): String;
  function ParseToStrings(Input: PChar; Parser: Char): TStrings;
  function RemoveCharacter(Input: String): String;
  function RemoveExactString(From: TStrings; What: String): TStrings;
  function RemovePrefix(Prefix, From: String): String;
  function ReplaceCharacters(Input: String; Find, Replace: Char): String;
  function SeparateFloat(Input: PChar; Size: Integer): TFloatRecord;
  function StringToAddress(Input: String; Offset: Integer = 0): Integer;
  function StringBefore(Input: String; Before: Integer): String;
  function StringCompare(First, Second: PChar; Size: Integer): Boolean;
  function TrimCharacter(Input: String; Character: Char): String;
  function TrimCharacterLeft(Input: String; Character: Char): String;
  function TrimCharacterRight(Input: String; Character: Char): String;
  function ToTLine(Line: String; Number: Integer): TLine;
  function UpperCase(Input: String): String;
  function ZeroPaddedInteger(Input: Integer; Padding: Integer = 0): String;
  procedure ConstantToFloat(Input: String; Result: PChar; Size: Integer);
  procedure CustomHexToData(Input: String; Data: PChar; Size: Integer);
  procedure CustomStrToBCD(Input: String; Result: PChar; Size: Integer);
  procedure CustomStrToFloat(Input: String; Result: PChar; Size: Integer);
  procedure CustomStrToSInt(Input: String; Result: PChar; Size: Integer);
  procedure CustomStrToUInt(Input: String; Result: PChar; Size: Integer);
  procedure LogClear;
  procedure LogSystemTime;
  procedure LogWrite(Log: String; Error: Boolean = False);
  procedure InitializeMemory(Input: PChar; Size: Integer; Character: Char);

var
  Log_OnClear: TLogClearEvent;
  Log_OnWrite: TLogWriteEvent;
  Log_Sender: TObject;


implementation

  function AddressToString(Address: Pointer): String;
  begin
    SetLength(Result, SizeOf(Pointer));
    PInteger(Result)^ := Integer(Address);
  end;

  function CartesianOfStrings(First: String; Second: TStrings): TStrings;
  var
    i: Integer;
  begin
    SetLength(Result, 0);
    for i := 0 to (Length(Second) - 1) do
    begin
      SetLength(Result, Length(Result) + 1);
      Result[Length(Result) - 1] := First + Second[i];
    end;
  end;

  function CodeToHexadecimal(Input: String): String;
  var
    i: Integer;
    lbyte: Byte;
  begin
    Result := '';
    if (Input = '') then Exit;
    for i := 1 to Length(Input) do
    begin
      lbyte := Ord(Input[i]);
      Result := Result + CHARS_HEXA[lbyte shr 4] + CHARS_HEXA[lbyte and $F];
    end;
  end;

  function ContainsCharacter(Input: String; Character: Char): Boolean;
  var
    i: Integer;
  begin
    Result := False;
    if (Input = '') then Exit;
    for i := 1 to Length(Input) do
      if (Input[i] = Character) then
      begin
        Result := True;
        Break;
      end;
  end;

  function CustomBCDToStr(Input: PChar; Size: Integer): String;
  var
    i: Integer;
    lbyte: Byte;
  begin
    Result := '';
    if not(Size = 10) then Exit;
    with PBCDRecord(Input)^ do
    begin
      if (Bytes[9] = $FF) and (Bytes[8] = $FF) and (Bytes[7] = $C0) then
      begin
        Result := DESC_NAN;
        Exit;
      end;
      for i := 8 downto 0 do
      begin
        lbyte := Bytes[i] shr 4;
        if not(lbyte = 0) then Result := Result + CHARS_HEXA[lbyte];
        lbyte := Bytes[i] and $F;
        if not(lbyte = 0) then Result := Result + CHARS_HEXA[lbyte];
      end;
      if (Result = '') then Result := CHARS_HEXA[0];
      if not(Bytes[9] and $80 = 0) then Result := '-' + Result;
    end;
  end;

  function CustomDataToHex(Input: PChar; Size: Integer): String;
  var
    i: Integer;
    lbyte: Byte;
  begin
    Result := '';
    for i := (Size - 1) downto 0 do
    begin
      lbyte := Ord(Input[i]);
      Result := Result + CHARS_HEXA[lbyte shr 4] + CHARS_HEXA[lbyte and $F];
    end;
  end;

  function CustomFloatToStr(Input: PChar; Size: Integer): String;
  var
    linput: TFloatRecord;
    lcmp: TSeparateConstants;
  begin
    case Size of
      4: lcmp := SEP_FP_4;
      8: lcmp := SEP_FP_8;
      10: lcmp := SEP_FP_10;
      else
        Exit;
    end;
    linput := SeparateFloat(Input, Size);
    with linput do
    begin
      if (Exponent = lcmp.ExponCmp) then
        if (Significand = lcmp.SigniCmp) then
        begin
          if Sign then Result := DESC_NEG_INF
          else Result := DESC_POS_INF;
        end
        else
        begin
          if (Significand and lcmp.SigniAnd = 0) then
            Result := DESC_NAN_S
          else
            Result := DESC_NAN_Q;
          Result := Result + DESC_NAN;
        end
      else
        case Size of
          4: Result := FloatToStr(PSingle(Input)^);
          8: Result := FloatToStr(PDouble(Input)^);
          10: Result := FloatToStr(PExtended(Input)^);
        end;
    end;
  end;

  function CustomSIntToStr(Input: PChar; Size: Integer): String;
  begin
    Result := '';
    case Size of
      1: Result := IntToStr(PShortint(Input)^);
      2: Result := IntToStr(PSmallint(Input)^);
      4: Result := IntToStr(PInteger(Input)^);
      8: Result := IntToStr(PInt64(Input)^);
    end;
  end;

  function CustomUIntToStr(Input: PChar; Size: Integer): String;
  begin
    Result := '';
    case Size of
      1: Result := IntToStr(PByte(Input)^);
      2: Result := IntToStr(PWord(Input)^);
      4: Result := IntToStr(PCardinal(Input)^);
      8: Result := IntToStr(PInt64(Input)^);
    end;
  end;

  function EqualBeforeFirstParser(First, Second: String; Parser: Char): Boolean;
  begin
    if (ParseBeforeFirst(First, Parser) = ParseBeforeFirst(Second, Parser)) then
      Result := True
    else
      Result := False;
  end;

  function IsAddress(Input: String): Boolean;
  var
    i: Integer;
  begin
    Result := False;
    Input := TrimCharacter(Input, ' ');
    if (Input = '') then Exit;
    if not(Input[1] in ['0'..'9', '+', '-']) then Exit;
    if (Input[1] in ['+', '-']) and (Length(Input) = 1) then Exit;
    for i := 2 to Length(Input) do
      if not(Input[i] in ['0'..'9']) then Exit;
    Result := True;
  end;

  function IsHexaChar(Input: Char): Boolean;
  var
    i: Integer;
  begin
    Result := False;
    for i := 0 to (Length(CHARS_HEXA) - 1) do
      if (Input = CHARS_HEXA[i]) then
      begin
        Result := True;
        Exit;
      end;
  end;

  function FloatType(Input: PChar; Size: Integer): Integer;
  var
    lresult: String;
  begin
    Result := 1;
    lresult := CustomFloatToStr(Input, Size);
    if (lresult = '0') then Result := 0
    else if (lresult = DESC_NEG_INF) or (lresult = DESC_POS_INF)
      or (lresult = DESC_QNAN) or (lresult = DESC_SNAN) then Result := -1;
  end;

  function IsPrefixOf(Prefix, OfWhat: String): Boolean;
  var
    i: Integer;
  begin
    Result := False;
    if (Length(Prefix) > Length(OfWhat)) then Exit;
    for i := 1 to Length(Prefix) do
      if not(Prefix[i] = OfWhat[i]) then Exit;
    Result := True;
  end;

  function MaxOf(First, Second: Integer): Integer;
  begin
    if (First > Second) then Result := First
    else Result := Second;
  end;

  function MergeStringTStrings(First: String; Second: TStrings): TStrings;
  var
    i: Integer;
  begin
    SetLength(Result, Length(Second) + 1);
    Result[0] := First;
    for i := 0 to (Length(Second) - 1) do
      Result[i + 1] := Second[i];
  end;

  function MergeTLinesLine(First: TLines; Second: TLine): TLines;
  var
    i: Integer;
  begin
    SetLength(Result, Length(First) + 1);
    Result[Length(First)] := Second;
    for i := 0 to (Length(First) - 1) do
      Result[i] := First[i];
  end;

  function MergeTStringsString(First: TStrings; Second: String): TStrings;
  var
    i: Integer;
  begin
    SetLength(Result, Length(First) + 1);
    Result[Length(First)] := Second;
    for i := 0 to (Length(First) - 1) do
      Result[i] := First[i];
  end;

  function MinOf(First, Second: Integer): Integer;
  begin
    if (First < Second) then Result := First
    else Result := Second;
  end;

  function MirrorString(Input: String): String;
  var
    i: Integer;
  begin
    Result := '';
    if (Input = '') then Exit;
    for i := Length(Input) downto 1 do
      Result := Result + Input[i];
  end;

  function NeutralizeDoubles(Input: String; Character: Char): String;
  begin
    Result := OmmitAfter(Input, Character, Character);
  end;

  function OmmitAfter(Input: String; Character, What: Char): String;
  var
    i: Integer;
    llast: Char;
  begin
    Result := '';
    if (Input = '') then Exit;
    llast := Chr(Ord(Character) + 1);
    for i := 1 to Length(Input) do
      if not((llast = Character) and (Input[i] = What)) then
      begin
        Result := Result + Input[i];
        llast := Input[i];
      end;
  end;

  function OmmitBefore(Input: String; Character, What: Char): String;
  var
    i: Integer;
    llast: Char;
  begin
    Result := '';
    if (Input = '') then Exit;
    llast := Chr(Ord(Character) + 1);
    for i := Length(Input) downto 1 do
      if not((llast = Character) and (Input[i] = What)) then
      begin
        Result := Result + Input[i];
        llast := Input[i];
      end;
    Result := MirrorString(Result);
  end;

  function OmmitEverywhere(Input: String; Character, What: Char): String;
  begin
    Result := OmmitBefore(OmmitAfter(Input, Character, What), Character, What);
  end;

  function ParseAfterFirst(Input: String; Parser: Char): String;
  var
    i, lpos: Integer;
  begin
    Result := '';
    if (Input = '') then Exit;
    for i := 1 to Length(Input) do
      if (Input[i] = Parser) then Break;
    lpos := i + 1;
    for i := lpos to Length(Input) do
      Result := Result + Input[i];
  end;

  function ParseAfterLast(Input: String; Parser: Char): String;
  var
    i, lpos: Integer;
  begin
    Result := '';
    if (Input = '') then Exit;
    for i := Length(Input) downto 1 do
      if (Input[i] = Parser) then Break;
    lpos := i + 1;
    for i := lpos to Length(Input) do
      Result := Result + Input[i];
  end;

  function ParseBeforeFirst(Input: String; Parser: Char): String;
  var
    i: Integer;
  begin
    Result := '';
    if (Input = '') then Exit;
    for i := 1 to Length(Input) do
      if (Input[i] = Parser) then Break
      else Result := Result + Input[i];
  end;

  function ParseBeforeLast(Input: String; Parser: Char): String;
  var
    i, lpos: Integer;
  begin
    Result := '';
    if (Input = '') then Exit;
    for i := Length(Input) downto 1 do
      if (Input[i] = Parser) then Break;
    lpos := i - 1;
    for i := 1 to lpos do
      Result := Result + Input[i];
  end;

  function ParseFirst(var Input: String; Parser: Char): String;
  begin
    Result := ParseAfterFirst(Input, Parser);
    Input := ParseBeforeFirst(Input, Parser);
  end;

  function ParseToStrings(Input: PChar; Parser: Char): TStrings;
  var
    i: Integer;
    llast, lstring: PChar;
  begin
    SetLength(Result, 0);
    if (Length(Input) = 0) then Exit;
    i := 0;
    lstring := GetMemory(Length(Input) + 1);
    Move(Input^, lstring^, Length(Input) + 1);
    llast := lstring;
    while (i < Length(Input)) do
    begin
      if (Input[i] = Parser) then
      begin
        lstring[i] := #0;
        SetLength(Result, Length(Result) + 1);
        Result[Length(Result) - 1] := String(llast);
        llast := @lstring[i + 1];
      end;
      i := i + 1;
    end;
    SetLength(Result, Length(Result) + 1);
    Result[Length(Result) - 1] := String(llast);
    FreeMemory(lstring);
  end;

  function RemoveCharacter(Input: String): String;
  begin
    Result := '';
    if (Input = '') then Exit;
    Result := Input;
    SetLength(Result, Length(Input) - 1);
  end;

  function RemoveExactString(From: TStrings; What: String): TStrings;
  var
    i: Integer;
  begin
    SetLength(Result, 0);
    for i := 0 to (Length(From) - 1) do
      if not(From[i] = What) then
      begin
        SetLength(Result, Length(Result) + 1);
        Result[Length(Result) - 1] := From[i];
      end;
  end;

  function RemovePrefix(Prefix, From: String): String;
  var
    i: Integer;
  begin
    Result := '';
    if not IsPrefixOf(Prefix, From) then Exit;
    for i := (Length(Prefix) + 1) to Length(From) do
      Result := Result + From[i];
  end;

  function ReplaceCharacters(Input: String; Find, Replace: Char): String;
  var
    i: Integer;
  begin
    Result := '';
    if (Input = '') then Exit;
    for i := 1 to Length(Input) do
      if (Input[i] = Find) then
        Result := Result + Replace
      else
        Result := Result + Input[i];
  end;

  function SeparateFloat(Input: PChar; Size: Integer): TFloatRecord;
  begin
    case Size of
      4:
      begin
        Result.Significand := PInteger(Input)^ and $7FFFFF;
        Result.Exponent := (PWord(Integer(Input) + 2)^ shr 7) and $FF;
        Result.Sign := not((PByte(Integer(Input) + 3)^ and $80) = 0);
      end;
      8:
      begin
        Result.Significand := PInt64(Input)^ and $FFFFFFFFFFFFF;
        Result.Exponent := (PWord(Integer(Input) + 6)^ shr 4) and $7FF;
        Result.Sign := not((PByte(Integer(Input) + 7)^ and $80) = 0);
      end;
      10:
      begin
        Result.Significand := PInt64(Input)^;
        Result.Exponent := PWord(Integer(Input) + 8)^ and $7FFF;
        Result.Sign := not((PByte(Integer(Input) + 9)^ and $80) = 0);
      end;
    end;
  end;

  function StringToAddress(Input: String; Offset: Integer = 0): Integer;
  var
    i, lstart, lmul: Integer;
  begin
    Input := TrimCharacter(Input, ' ');
    lmul := 1;
    case Input[1] of
      '+':
        lstart := 2;
      '-':
      begin
        lstart := 2;
        lmul := -1;
      end;
      else
      begin
        lstart := 1;
        Offset := 0;
      end;
    end;
    Result := 0;
    for i := lstart to Length(Input) do
      Result := (10 * Result) + Ord(Input[i]) - Ord(CHARS_HEXA[0]);
    Result := (Result * lmul) + Offset;
  end;

  function StringBefore(Input: String; Before: Integer): String;
  var
    i: Integer;
  begin
    Result := '';
    for i := 1 to Before do
      Result := Result + Input[i];
  end;

  function StringCompare(First, Second: PChar; Size: Integer): Boolean;
  var
    i: Integer;
  begin
    Result := False;
    for i := 0 to (Size - 1) do
      if not(First[i] = Second[i]) then Exit;
    Result := True;
  end;

  function TrimCharacter(Input: String; Character: Char): String;
  begin
    Result := TrimCharacterLeft(TrimCharacterRight(Input, Character), Character);
  end;

  function TrimCharacterLeft(Input: String; Character: Char): String;
  var
    i, lstart: Integer;
  begin
    Result := '';
    if (Input = '') then Exit;
    for i := 1 to Length(Input) do
      if not(Input[i] = Character) then Break;
    lstart := i;
    for i := lstart to Length(Input) do
      Result := Result + Input[i];
  end;

  function TrimCharacterRight(Input: String; Character: Char): String;
  var
    i, lend: Integer;
  begin
    Result := '';
    if (Input = '') then Exit;
    for i := Length(Input) downto 1 do
      if not(Input[i] = Character) then Break;
    lend := i;
    for i := 1 to lend do
      Result := Result + Input[i];
  end;

  function ToTLine(Line: String; Number: Integer): TLine;
  begin
    Result.Line := Line;
    Result.Number := Number;
  end;

  function UpperCase(Input: String): String;
  var
    i: Integer;
  begin
    Result := '';
    if (Input = '') then Exit;
    for i := 1 to Length(Input) do
      Result := Result + UpCase(Input[i]);
  end;

  function ZeroPaddedInteger(Input, Padding: Integer): String;
  var
    i: Integer;
    lminus: Boolean;
  begin
    Result := '';
    if (Input < 0) then
    begin
      lminus := True;
      Input := -Input;
    end
    else lminus := False;
    while (Input > 0) do
    begin
      Result := Result + CHARS_HEXA[Input mod 10];
      Input := Input div 10;
    end;
    for i := Length(Result) to (Padding - 1) do
      Result := Result + CHARS_HEXA[0];
    if lminus then Result := Result + '-';
    Result := MirrorString(Result);
  end;

  procedure ConstantToFloat(Input: String; Result: PChar; Size: Integer);
  begin
    if not(Length(Input) = Size) then Exit;
    Move(PChar(Input)[0], Result^, Size);
  end;

  procedure CustomHexToData(Input: String; Data: PChar; Size: Integer);
  var
    i, j: Integer;
    lbyte: Byte;
  begin
    Input := UpperCase(TrimCharacter(Input, ' '));
    InitializeMemory(Data, Size, #0);
    if (Length(Input) > Size * 2) then Exit;
    j := (Size * 2) - Length(Input);
    for i := 1 to j do
      Input := CHARS_HEXA[0] + Input;
    Input := MirrorString(Input);
    for i := 1 to Length(Input) do
      if not IsHexaChar(Input[i]) then Exit;
    for i := 1 to Size do
    begin
      lbyte := 0;
      for j := 0 to (Length(CHARS_HEXA) - 1) do
        if (CHARS_HEXA[j] = Input[2 * i]) then
        begin
          lbyte := j;
          Break;
        end;
      lbyte := lbyte shl 4;
      for j := 0 to (Length(CHARS_HEXA) - 1) do
        if (CHARS_HEXA[j] = Input[(2 * i) - 1]) then
        begin
          lbyte := lbyte + j;
          Break;
        end;
      Data[i - 1] := Chr(lbyte);
    end;
  end;

  procedure CustomStrToBCD(Input: String; Result: PChar; Size: Integer);
  var
    i: Integer;
    lint: Int64;
  begin
    if not(Size = 10) then Exit;
    InitializeMemory(Result, 10, #0);
    lint := StrToInt64Def(Input, 0);
    if (lint < -999999999999999999) then Exit;
    if (lint > 999999999999999999) then Exit;
    with PBCDRecord(Result)^ do
    begin
      if (lint < 0) then
      begin
        Bytes[9] := $80;
        lint := Abs(lint);
      end;
      i := 0;
      while (lint > 0) do
      begin
        Bytes[i] := (((lint mod 100) div 10) shl 4) + (lint mod 10);
        i := i + 1;
        lint := lint div 100;
      end;
    end;
  end;

  procedure CustomStrToFloat(Input: String; Result: PChar; Size: Integer);
  var
    ltrans: TTranslateConstants;
  begin
    case Size of
      4: ltrans := TRANS_FP_4;
      8: ltrans := TRANS_FP_8;
      10: ltrans := TRANS_FP_10;
      else
        Exit;
    end;
    Input := UpperCase(TrimCharacter(Input, ' '));
    if (Input = '') then Exit;
    if (Input = DESC_NEG_INF) then
    begin
      ConstantToFloat(ltrans.NegInf, Result, Size);
      Exit;
    end;
    if (Input = DESC_POS_INF) or (Input = DESC_INF) then
    begin
      ConstantToFloat(ltrans.PosInf, Result, Size);
      Exit;
    end;
    if (Input = DESC_QNAN) or (Input = DESC_NAN) then
    begin
      ConstantToFloat(ltrans.QNaN, Result, Size);
      Exit;
    end;
    if (Input = DESC_SNAN) then
    begin
      ConstantToFloat(ltrans.SNaN, Result, Size);
      Exit;
    end;
    try
      case Size of
        4: PSingle(Result)^ := StrToFloat(Input);
        8: PDouble(Result)^ := StrToFloat(Input);
        10: PExtended(Result)^ := StrToFloat(Input);
      end;
    except
      on EConvertError do
        case Size of
          4: PSingle(Result)^ := 0;
          8: PDouble(Result)^ := 0;
          10: PExtended(Result)^ := 0;
        end;
    end;
  end;

  procedure CustomStrToSInt(Input: String; Result: PChar; Size: Integer);
  var
    lint: Int64;
  begin
    lint := StrToInt64Def(Input, 0);
    case Size of
      1:
        if (lint > 127) or (lint < -128) then PShortint(Result)^ := 0
        else PShortInt(Result)^ := lint;
      2:
        if (lint > 32767) or (lint < -32768) then PSmallint(Result)^ := 0
        else PSmallInt(Result)^ := lint;
      4: PInteger(Result)^ := StrToIntDef(Input, 0);
      8: PInt64(Result)^ := lint;
    end;
  end;

  procedure CustomStrToUInt(Input: String; Result: PChar; Size: Integer);
  var
    lint: Int64;
  begin
    lint := StrToInt64Def(Input, 0);
    case Size of
      1:
        if (lint > 255) or (lint < 0) then PShortint(Result)^ := 0
        else PShortInt(Result)^ := lint;
      2:
        if (lint > 65536) or (lint < 0) then PSmallint(Result)^ := 0
        else PSmallInt(Result)^ := lint;
      4:
        if (lint > 4294967295) or (lint < 0) then PInteger(Result)^ := 0
        else PInteger(Result)^ := lint;
      8:
        PInt64(Result)^ := lint;
    end;
  end;

  procedure LogClear;
  begin
    if not(@Log_OnClear = nil) then Log_OnClear(Log_Sender);
  end;

  procedure LogSystemTime;
  begin
    LogWrite('');
    LogWrite(APP_VERSION + ' [' + TimeToStr(Time) + ']');
  end;

  procedure LogWrite(Log: String; Error: Boolean = False);
  begin
    if not(@Log_OnWrite = nil) then Log_OnWrite(Log_Sender, Log, Error);
  end;

  procedure InitializeMemory(Input: PChar; Size: Integer; Character: Char);
  var
    i: Integer;
  begin
    for i := 0 to (Size - 1) do
      Input[i] := Character;
  end;

initialization
  DecimalSeparator := '.';

end.

