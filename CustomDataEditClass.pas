unit CustomDataEditClass;

interface

uses
  Classes, Forms;

type

  { TCustomDataEdit }

  TCustomDataEdit = class(TForm)
  public
    constructor Create(AOwner: TComponent); override;
    function ShowBox(Input: Pointer; Size: Integer): Boolean; virtual; abstract;
  end;

implementation

// ************************************************************************** //
// *                    TCustomDataEdit implementation                      * //
// ************************************************************************** //

  constructor TCustomDataEdit.Create(AOwner: TComponent);
  begin
    inherited CreateNew(AOwner, 0);
    Position := poDesktopCenter;
  end;

end.
