unit AsmFileIOClass;

interface

uses
  ConstantsClass, FunctionsClass, HardwareClass, PrefixTreeClass,
  ResourcesClass, SteppingClass, TypesClass,
  SysUtils;

type

  { TAsmFileIO }

  TAsmFileIO = class
  private
    pError: String;
    function pErrorSet: Boolean; virtual;
    procedure pSetError(Input: String = GLOB_NO_ERROR; FileName: String = ''; Line: PInteger = nil); virtual;
  public
    function LoadFromFile(const FileName: string; Step: TStepping): Boolean; virtual;
    function SaveToFile(const FileName: string; Step: TStepping): Boolean; virtual;
    property LastError: String read pError write pError;
  end;

implementation

// ************************************************************************** //
// *                       TAsmFileIO implementation                        * //
// ************************************************************************** //

  function TAsmFileIO.pErrorSet: Boolean;
  begin
    Result := not(pError = GLOB_NO_ERROR);
  end;

  procedure TAsmFileIO.pSetError(Input: String = GLOB_NO_ERROR;
    FileName: String = ''; Line: PInteger = nil);
  begin
    pError := Input;
    if not(FileName = '') then
      if not(Line = nil) then
        pError := FileName + '(' + IntToStr(Line^) + '): ' + pError
      else
        pError := FileName + ': ' + pError;
    if not(pError = GLOB_NO_ERROR) then LogWrite(pError, True);
  end;

  function TAsmFileIO.LoadFromFile(const FileName: string; Step: TStepping
    ): Boolean;
  var
    i, lstate: Integer;
    ldata, linst: TLines;
    lfile: TextFile;
    lhardware: THardware;
    lline: String;
    lsystem: TLine;
  begin
    pSetError;
    Result := False;
    SetLength(ldata, 0);
    SetLength(linst, 0);
    LogSystemTime;
    LogWrite(FIO_LOADING + FileName);
    try
      AssignFile(lfile, FileName);
      FileMode := fmOpenRead;
      Reset(lfile);
      lstate := 0;
      i := 0;
      while not Eof(lfile) do
      begin
        Readln(lfile, lline);
        i := i + 1;
        if ContainsCharacter(lline, FIO_COMMENT) then
        begin
          LogWrite(FIO_COMMENT + ParseAfterFirst(lline, FIO_COMMENT));
          lline := ParseBeforeFirst(lline, FIO_COMMENT);
        end;
        lline := OmmitEverywhere(TrimCharacter(lline, ' '), ' ', ' ');
        lline := UpperCase(lline);
        if (lline = '') then Continue;
        case lstate of
          0:
          begin
            if not IsPrefixOf(FIO_SYSTEM, lline) then
            begin
              pSetError(FIO_SYSTEM_NOT_FOUND, FileName);
              LogWrite(FIO_ERROR_LOADING + FileName);
              Exit;
            end;
            lsystem := ToTLine(lline, i);
            lstate := lstate + 1;
          end;
          1:
            if (lline = FIO_DATA) then lstate := lstate + 1
            else lstate := lstate + 2;
          2:
            if (lline = FIO_CODE) then lstate := lstate + 1
            else ldata := MergeTLinesLine(ldata, ToTLine(lline, i));
          3:
            linst := MergeTLinesLine(linst, ToTLine(lline, i));
        end;
      end;
    finally
      CloseFile(lfile);
    end;
    lhardware := THardware.Create;
    try
      with lhardware do
      begin
        for i := 0 to (Length(ldata) - 1) do
          if not ValidateOperand(ldata[i].Line) then
            pSetError(LastError, FileName, @ldata[i].Number);
        if pErrorSet then
        begin
          LogWrite(FIO_ERROR_LOADING);
          Exit;
        end;
        for i := 0 to (Length(ldata) - 1) do
          OperandAdd(ldata[i].Line);
        for i := 0 to (Length(linst) - 1) do
          if not ValidateInstruction(linst[i].Line) then
            pSetError(LastError, FileName, @linst[i].Number);
        if pErrorSet then
        begin
          LogWrite(FIO_ERROR_LOADING);
          Exit;
        end;
      end;
    finally
      lhardware.Free;
    end;
    with Step.Hardware do
    begin
      InitializeOperands;
      for i := 0 to (Length(ldata) - 1) do
        OperandAdd(ldata[i].Line);
      Step.InitializeBlocks;
      for i := 0 to (Length(linst) - 1) do
        Step.AddStepBlock(STEP_LAST, InstructionByName(linst[i].Line));
    end;
    Result := True;
    LogWrite(FIO_LOADED);
  end;

  function TAsmFileIO.SaveToFile(const FileName: string; Step: TStepping
    ): Boolean;
  var
    i, j: Integer;
    lfile: TextFile;
    loper: POperandRecord;
    ltrees: TPrefixTrees;
  begin
    pSetError;
    Result := True;
    LogSystemTime;
    LogWrite(FIO_SAVING + FileName);
    try
      AssignFile(lfile, FileName);
      Rewrite(lfile);
      Writeln(lfile, FIO_SYSTEM + ' ' + APP_VERSION);
      Writeln(lfile);
      Writeln(lfile, FIO_DATA);
      ltrees := Step.Hardware.Operands.GetAllDescendants;
      for i := 0 to (Length(ltrees) - 1) do
      begin
        loper := POperandRecord(ltrees[i].Data);
        if (loper^.Default = '') then Continue;
        for j := 0 to (Length(sOperandTypes) - 1) do
          with sOperandTypes[j] do
            if (OperandType = loper^.OperandType) then
            begin
              Writeln(lfile, '  ' + loper^.Name + OPER_SEP + Description);
              Break;
            end;
      end;
      Writeln(lfile);
      Writeln(lfile, FIO_CODE);
      for i := 0 to (Step.Length - 1) do
        Writeln(lfile, '  ' + Step[i].CallFunction.Name);
    finally
      CloseFile(lfile);
    end;
    LogWrite(FIO_SAVED);
  end;

end.
