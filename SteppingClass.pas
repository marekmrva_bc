unit SteppingClass;

interface

uses
  ConstantsClass, FunctionsClass, HardwareClass, ResourcesClass, TypesClass;

type

  TStepping = class;

  { TStepBlock }

  TStepBlock = class
  private
    pCall: TInstruction;
    pError: String;
    pOwner: TStepping;
    pPosition: Integer;
    pBranch: Boolean;
    function pErrorSet: Boolean; virtual;
    procedure pSetError(Input: String = GLOB_NO_ERROR); virtual;
  public
    constructor Create(Owner: TStepping; Call: TInstruction); virtual;
    function ExecuteCall: Integer; virtual;
    function Valid: Boolean; virtual;
    property CallFunction: TInstruction read pCall write pCall;
    property LastError: String read pError;
    property Position: Integer read pPosition write pPosition default -1;
    property IsBranch: Boolean read pBranch;
    destructor Destroy; override;
  end;

  { TStepping }

  TStepping = class
  private
    pError: String;
    pSteps: array of TStepBlock;
    pHardware: THardware;
    pLength, pStepBlock: Integer;
    pOldOnOperand, pOnStep: TChangeEvent;
    pValid: Boolean;
    function pBlockFromCall(Position: Integer; Call: TInstruction): TStepBlock; virtual;
    function pErrorSet: Boolean; virtual;
    function pGetBlock(Position: Integer): TStepBlock; virtual;
    function pValidPosition(var Position: Integer): Boolean; virtual;
    procedure pOnOperand(Sender: TObject); virtual;
    procedure pSetError(Input: String = GLOB_NO_ERROR; Where: String = ''); virtual;
    procedure pSetHardware(Input: THardware); virtual;
    procedure pSetStepBlock(Input: Integer); virtual;
  public
    constructor Create; virtual;
    function AddStepBlock(Position: Integer; Call: TInstruction): Boolean; virtual;
    function ChangeStepBlock(Position: Integer; Call: TInstruction): Boolean; virtual;
    function RemoveStepBlock(Position: Integer): Boolean; virtual;
    function Valid: Boolean; virtual;
    procedure InitializeBlocks; virtual;
    procedure SingleStep; virtual;
    // ? function Animate(Input: Pointer); ?
    destructor Destroy; override;
    property Block[Position: Integer]: TStepBlock read pGetBlock; default;
    property Hardware: THardware read pHardware write pSetHardware;
    property LastError: String read pError;
    property Length: Integer read pLength default 0;
    property OnStep: TChangeEvent read pOnStep write pOnStep;
    property StepBlock: Integer read pStepBlock write pSetStepBlock;
  end;


implementation

// ************************************************************************** //
// *                      TStepBlock implementation                         * //
// ************************************************************************** //

  function TStepBlock.pErrorSet: Boolean;
  begin
    Result := not(pError = GLOB_NO_ERROR);
  end;

  procedure TStepBlock.pSetError(Input: String = GLOB_NO_ERROR);
  begin
    pError := Input;
  end;

  constructor TStepBlock.Create(Owner: TStepping; Call: TInstruction);
  begin
    pOwner := Owner;
    pCall := Call;
    pBranch := (not(Call = nil) and (Call.Branch = BRANCH_BRANCH));
    pSetError;
  end;

  function TStepBlock.ExecuteCall: Integer;
  begin
    Result := -1;
    if not Valid then Exit;
    if pCall.Execute and IsBranch then
      Result := StringToAddress(pCall.BranchAddress, Position)
    else
      Result := Position + 1;
  end;

  function TStepBlock.Valid: Boolean;
  var
    lbranch: Integer;
  begin
    pSetError;
    Result := False;
    if (pCall = nil) then pSetError(STEP_NO_CALL_FUNCTION)
    else if not pCall.Hardware.ValidateInstruction(pCall) then
      pSetError(pCall.Hardware.LastError)
    else if IsBranch then
    begin
      lbranch := StringToAddress(pCall.BranchAddress, Position);
      if (lbranch < 0) or (lbranch > pOwner.Length) then
        pSetError(STEP_BRANCH_OUT_OF_RANGE);
    end;
    if not pErrorSet then Result := True;
  end;

  destructor TStepBlock.Destroy;
  begin
    CallFunction.Free;
    inherited Destroy;
  end;

// ************************************************************************** //
// *                       TStepping implementation                         * //
// ************************************************************************** //

  function TStepping.pBlockFromCall(Position: Integer; Call: TInstruction
    ): TStepBlock;
  begin
    Result := nil;
    if (Call = nil) then
    begin
      pSetError(STEP_NO_CALL_FUNCTION);
      Exit;
    end;
    Result := TStepBlock.Create(Self, Call);
    Result.Position := Position;
    pValid := False;
  end;

  function TStepping.pErrorSet: Boolean;
  begin
    Result := not(pError = GLOB_NO_ERROR);
  end;

  function TStepping.pGetBlock(Position: Integer): TStepBlock;
  begin
    if pValidPosition(Position) then Result := pSteps[Position]
    else Result := nil;
  end;

  function TStepping.pValidPosition(var Position: Integer): Boolean;
  begin
    Result := False;
    if (Position < STEP_FIRST) then Exit;
    if (Position = STEP_LAST) then Position := pLength - 1;
    if (Position < Length) then Result := True;
  end;

  procedure TStepping.pOnOperand(Sender: TObject);
  begin
    pValid := False;
    if not(@pOldOnOperand = nil) then pOldOnOperand(Sender);
  end;

  procedure TStepping.pSetError(Input: String = GLOB_NO_ERROR;
    Where: String = '');
  begin
    pError := Input;
    if not(Where = '') then pError := Where + ': ' + pError;
    if not(pError = GLOB_NO_ERROR) then LogWrite(pError, True);
  end;

  procedure TStepping.pSetHardware(Input: THardware);
  begin
    if not(pHardware = nil) then pHardware.OnOperand := pOldOnOperand;
    pHardware := Input;
    if not(Input = nil) then
    begin
      pOldOnOperand := Input.OnOperand;
      Input.OnOperand := pOnOperand;
    end;
  end;

  procedure TStepping.pSetStepBlock(Input: Integer);
  begin
    pSetError;
    if not pValidPosition(Input) then Input := -1;
    pStepBlock := Input;
    if not(@pOnStep = nil) then pOnStep(Self);
  end;

  constructor TStepping.Create;
  begin
    InitializeBlocks;
    pSetError;
  end;

  function TStepping.AddStepBlock(Position: Integer; Call: TInstruction
    ): Boolean;
  var
    i: Integer;
    lblock: TStepBlock;
  begin
    pSetError;
    Result := False;
    pLength := pLength + 1;
    if not pValidPosition(Position) then
    begin
      pLength := pLength - 1;
      Exit;
    end;
    lblock := pBlockFromCall(Position, Call);
    if pErrorSet then
    begin
      pLength := pLength - 1;
      Exit;
    end;
    SetLength(pSteps, pLength);
    for i := (pLength - 2) downto Position do
    begin
      pSteps[i + 1] := pSteps[i];
      pSteps[i + 1].Position := i + 1;
    end;
    pSteps[Position] := lblock;
    if not(Hardware = nil) then Hardware.MaxAddress := pLength;
    pValid := False;
    Result := True;
  end;

  function TStepping.ChangeStepBlock(Position: Integer; Call: TInstruction
    ): Boolean;
  var
    lblock: TStepBlock;
  begin
    pSetError;
    Result := False;
    if not pValidPosition(Position) then Exit;
    lblock := pBlockFromCall(Position, Call);
    if pErrorSet then Exit;
    pSteps[Position].Free;
    pSteps[Position] := lblock;
    pValid := False;
    Result := True;
  end;

  function TStepping.RemoveStepBlock(Position: Integer): Boolean;
  var
    i: Integer;
  begin
    pSetError;
    Result := False;
    if not pValidPosition(Position) then Exit;
    pSteps[Position].Free;
    for i := Position to (pLength - 2) do
    begin
      pSteps[i] := pSteps[i + 1];
      pSteps[i].Position := i;
    end;
    pLength := pLength - 1;
    SetLength(pSteps, pLength);
    if not(Hardware = nil) then Hardware.MaxAddress := pLength;
    pValid := False;
    Result := True;
  end;

  function TStepping.Valid: Boolean;
  var
    i: Integer;
  begin
    pSetError;
    Result := True;
    if pValid then Exit;
    for i := 0 to (pLength - 1) do
    begin
      Result := pSteps[i].Valid and Result;
      if pSteps[i].pErrorSet then
        pSetError(pSteps[i].LastError, ZeroPaddedInteger(i, CONST_PADDING));
    end;
    pValid := Result;
  end;

  procedure TStepping.InitializeBlocks;
  var
    i: Integer;
  begin
    StepBlock := -1;
    for i := 0 to (pLength - 1) do pSteps[i].Free;
    SetLength(pSteps, 0);
    pLength := 0;
    pValid := True;
  end;

  procedure TStepping.SingleStep;
  var
    lpos: Integer;
  begin
    pSetError;
    if (StepBlock < 0) then pValid := False;
    if not pValid then Valid;
    if not pValid then
    begin
      StepBlock := -1;
      Exit;
    end;
    lpos := StepBlock;
    if not pValidPosition(lpos) then StepBlock := 0
    else StepBlock := pSteps[lpos].ExecuteCall;
  end;

  destructor TStepping.Destroy;
  begin
    while (Length > 0) do RemoveStepBlock(STEP_LAST);
    inherited Destroy;
  end;

end.
