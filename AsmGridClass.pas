unit AsmGridClass;

interface

uses
  ConstantsClass, CustomStringGridClass, FunctionsClass, HardwareClass,
  ResourcesClass, SteppingClass, TypesClass,
  Classes, Controls, Dialogs, Forms, Graphics, Grids, Menus, StdCtrls, Types;

type

  TAsmGrid = class;

  { TAsmInputBox }

  TAsmInputBox = class(TForm)
  private
    pAssemble, pCancel: TButton;
    pBox: TEdit;
    pDontChange, pInsert: Boolean;
    pGrid: TAsmGrid;
    pLabel: TLabel;
    pList: TListBox;
    pPosition: Integer;
    pText: String;
    procedure pBoxOnChange(Sender: TObject); virtual;
    procedure pBoxOnKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState); virtual;
    procedure pListOnDblClick(Sender: TObject); virtual;
    procedure pOnButtonClick(Sender: TObject); virtual;
    procedure pOnChange(Sender: TObject; ARow: Integer; Text: String; Insert: Boolean); virtual;
    procedure pOnRemove(Sender: TObject; ARow: Integer); virtual;
    procedure pOnShow(Sender: TObject); virtual;
  public
    constructor CreateBox(AOwner: TComponent; AsmGrid: TAsmGrid); virtual;
  end;

  { TAsmGrid }

  TAsmGrid = class(TCustomStringGrid)
  private
    pAskReset: Boolean;
    pColorTheme: TGridTheme;
    pStep: TStepping;
    pInputBox: TAsmInputBox;
    pOldOnOperand, pOldOnStep: TChangeEvent;
    pOnChange: TInsertEvent;
    pOnRemove: TRemoveEvent;
    function pGetSelectedLine: Integer; virtual;
    function pGetWidth: Integer; virtual;
    procedure pCustomDblClick(Sender: TObject); virtual;
    procedure pCustomDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState); virtual;
    procedure pCustomKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState); virtual;
    procedure pCustomMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer); virtual;
    procedure pCustomPopupGetOrigin(Sender: TObject); virtual;
    procedure pCustomPopupInsert(Sender: TObject); virtual;
    procedure pCustomPopupRemove(Sender: TObject); virtual;
    procedure pCustomPopupReset(Sender: TObject); virtual;
    procedure pCustomPopupSetOrigin(Sender: TObject); virtual;
    procedure pCustomPopupStep(Sender: TObject); virtual;
    procedure pDrawSingleCell(Canvas: TCanvas; ACol, ARow: Integer; Rect: TRect; Colors: TColors; Highlight: Boolean); virtual;
    procedure pOnOperand(Sender: TObject); virtual;
    procedure pOnStep(Sender: TObject); virtual;
    procedure pSetColorTheme(ColorTheme: TGridTheme); virtual;
    procedure pSetSelectedLine(Line: Integer); virtual;
    procedure pSetStep(Step: TStepping); virtual;
    procedure pSetWidth(Width: Integer); virtual;
  public
    constructor CreateGrid(AOwner: TComponent; Step: TStepping); virtual;
    function ChangeInstruction(ARow: Integer; Name: String): Boolean; overload; virtual;
    function ChangeInstruction(Name: String): Boolean; overload; virtual;
    function InsertInstruction(ARow: Integer; Name: String): Boolean; overload; virtual;
    function InsertInstruction(Name: String): Boolean; overload; virtual;
    function RemoveInstruction(ARow: Integer): Boolean; overload; virtual;
    function RemoveInstruction: Boolean; overload; virtual;
    function ReloadInstructions: Boolean; virtual;
    procedure ProgramReset; virtual;
    procedure ProgramStep; virtual;
    property ColorTheme: TGridTheme read pColorTheme write pSetColorTheme;
    property InputBox: TAsmInputBox read pInputBox write pInputBox;
    property OnChangeLine: TInsertEvent read pOnChange write pOnChange;
    property OnRemoveLine: TRemoveEvent read pOnRemove write pOnRemove;
    property SelectedLine: Integer read pGetSelectedLine write pSetSelectedLine;
    property Stepping: TStepping read pStep write pSetStep;
    property Width read pGetWidth write pSetWidth;
  end;

implementation

// ************************************************************************** //
// *                      TAsmInputBox implementation                       * //
// ************************************************************************** //

  procedure TAsmInputBox.pBoxOnChange(Sender: TObject);
  var
    lstrings: TypesClass.TStrings;
    i: Integer;
    ltext: String;
  begin
    if pDontChange then Exit;
    if not(pText = pBox.Text) or not(pPosition = pBox.SelStart) then
    begin
      ltext := StringBefore(pBox.Text, pBox.SelStart);
      SetLength(lstrings, 0);
      lstrings := pGrid.Stepping.Hardware.InstructionsByPrefix(ltext);
      pList.Clear;
      for i := 0 to (Length(lstrings) - 1) do
        pList.Items.Add(lstrings[i]);
      pList.ItemIndex := 0;
    end;
    pText := pBox.Text;
    pPosition := pBox.SelStart;
  end;

  procedure TAsmInputBox.pOnButtonClick(Sender: TObject);
  var
    lrow: Integer;
  begin
    with pList do
      if (Count > 0) and (ItemIndex > 0) then
        if not(pBox.Text = Items[ItemIndex]) then
        begin
          pListOnDblClick(Sender);
          Exit;
        end;
    lrow := pGrid.SelectedLine;
    with pGrid.Stepping.Hardware do
      if not ValidateInstruction(pBox.Text) then
      begin
        if (pList.Count > 0) then
        begin
          pDontChange := True;
          pBox.Text := pList.Items[pList.ItemIndex];
          pBox.SelStart := Length(pBox.Text);
          pDontChange := False;
          pBoxOnChange(Self);
        end
        else pLabel.Caption := LastError;
        Exit;
      end;
    with pGrid do
    begin
      if pInsert then
        InsertInstruction(lrow, pBox.Text)
      else
        ChangeInstruction(lrow, pBox.Text);
      SelectedLine := SelectedLine + 1;
      lrow := SelectedLine;
      if pInsert then
        Self.Caption := ASM_INPUT_CAPTION_INSERT + ' ' + Cells[0, lrow]
      else
        Self.Caption := ASM_INPUT_CAPTION_CHANGE + ' ' + Cells[0, lrow];
      if not(VisibleRowCount + TopRow > lrow) then TopRow := TopRow + 1;
      pOnShow(Self);
    end;
  end;

  procedure TAsmInputBox.pOnChange(Sender: TObject; ARow: Integer;
    Text: String; Insert: Boolean);
  begin
    if Insert then
    begin
      Caption := ASM_INPUT_CAPTION_INSERT + ' ' + pGrid.Cells[0, ARow];
      pInsert := True;
    end
    else
    begin
      Caption := ASM_INPUT_CAPTION_CHANGE + ' ' + pGrid.Cells[0, ARow];
      pInsert := False;
    end;
    pDontChange := True;
    pBox.Text := Text;
    pBox.SelStart := Length(Text);
    pDontChange := False;
    pBoxOnChange(Self);
    Self.ShowModal;
  end;

  procedure TAsmInputBox.pBoxOnKeyDown(Sender: TObject; var Key: Word;
    Shift: TShiftState);
  var
    lkey: Word;
  begin
    lkey := Key;
    Key := 0;
    case lkey of
      KEY_HOME: pBox.SelStart := 0;
      KEY_END: pBox.SelStart := Length(pBox.Text);
      KEY_LEFT: pBox.SelStart := pBox.SelStart - 1;
      KEY_UP: pList.ItemIndex := MaxOf(pList.ItemIndex - 1, 0);
      KEY_RIGHT: pBox.SelStart := pBox.SelStart + 1;
      KEY_DOWN: pList.ItemIndex := pList.ItemIndex + 1;
      else Key := lkey;
    end;
    pBoxOnChange(Self);
  end;

  procedure TAsmInputBox.pListOnDblClick(Sender: TObject);
  begin
    if (pList.Count > 0) then
    begin
      pDontChange := True;
      pBox.Text := pList.Items[pList.ItemIndex];
      pBox.SelStart := Length(pBox.Text);
      pDontChange := False;
      pBoxOnChange(Self);
    end;
  end;

  procedure TAsmInputBox.pOnRemove(Sender: TObject; ARow: Integer);
  begin
    pGrid.RemoveInstruction(ARow);
  end;

  procedure TAsmInputBox.pOnShow(Sender: TObject);
  begin
    pLabel.Caption := '';
    pBox.SelectAll;
    pBox.SetFocus;
  end;

  constructor TAsmInputBox.CreateBox(AOwner: TComponent; AsmGrid: TAsmGrid);
  begin
    inherited CreateNew(AOwner, 0);
    Position := poDesktopCenter;
    ClientWidth := 321;
    ClientHeight := 181;
    OnShow := pOnShow;
    BorderStyle := bsDialog;
    pGrid := AsmGrid;
    with pGrid do
    begin
      OnChangeLine := Self.pOnChange;
      OnRemoveLine := Self.pOnRemove;
    end;
    pBox := TEdit.Create(Self);
    with pBox do
    begin
      Parent := Self;
      Text := '';
      Left := 8;
      Top := 8;
      Width := 309;
      Height := 21;
      OnChange := pBoxOnChange;
      OnKeyDown := pBoxOnKeyDown;
    end;
    pList := TListBox.Create(Self);
    with pList do
    begin
      Parent := Self;
      Left := 8;
      Top := 31;
      Width := 309;
      Height := 100;
      OnDblClick := pListOnDblClick;
    end;
    pAssemble := TButton.Create(Self);
    with pAssemble do
    begin
      Parent := Self;
      Caption := ASM_INPUT_BUTTON_ASSEMBLE;
      Left := 193;
      Top := 157;
      Width := 60;
      Height := 20;
      OnClick := pOnButtonClick;
      Default := True;
    end;
    pCancel := TButton.Create(Self);
    with pCancel do
    begin
      Parent := Self;
      Caption := ASM_INPUT_BUTTON_CANCEL;
      Left := 257;
      Top := 157;
      Width := 60;
      Height := 20;
      ModalResult := mrCancel;
      Cancel := True;
    end;
    pLabel := TLabel.Create(Self);
    with pLabel do
    begin
      Parent := Self;
      Caption := '';
      Left := 8;
      Top := 135;
      Width := 309;
      Height := 21;
      Font.Color := clRed;
    end;
  end;

// ************************************************************************** //
// *                        TAsmGrid implementation                         * //
// ************************************************************************** //

  function TAsmGrid.pGetSelectedLine: Integer;
  begin
    Result := Selection.Top;
  end;

  function TAsmGrid.pGetWidth: Integer;
  begin
    Result := inherited Width;
  end;

  procedure TAsmGrid.pCustomDblClick(Sender: TObject);
  var
    lkey: Word;
  begin
    lkey := KEY_RETURN;
    pCustomKeyDown(Self, lkey, []);
  end;

  procedure TAsmGrid.pCustomDrawCell(Sender: TObject; ACol, ARow: Integer;
    Rect: TRect; State: TGridDrawState);
  var
    lcolors: TColors;
  begin
    if (Stepping.StepBlock = ARow) and (ACol = 0) then
    begin
      pDrawSingleCell(Canvas, ACol, ARow, Rect, ColorTheme.Step, False);
      Exit;
    end;
    if (gdSelected in State) and Selected then
      if (ACol = 0) then lcolors := ColorTheme.Address.Selected
      else lcolors := ColorTheme.Assembly.Selected
    else
      if (ACol = 0) then lcolors := ColorTheme.Address.None
      else lcolors := ColorTheme.Assembly.None;
    if not(Objects[Acol, Arow] = nil) and
      (TStepBlock(Objects[ACol, ARow]).IsBranch) then
      pDrawSingleCell(Canvas, ACol, ARow, Rect, lcolors, True)
    else
      pDrawSingleCell(Canvas, ACol, ARow, Rect, lcolors, False);
  end;

  procedure TAsmGrid.pCustomKeyDown(Sender: TObject; var Key: Word;
    Shift: TShiftState);
  var
    lselect, ltop: Integer;
    ltext: String;
  begin
    lselect := SelectedLine;
    ltext := Cells[1, lselect];
    if (Key = KEY_RETURN) and (lselect = RowCount - 1) then Key := KEY_INSERT;
    if (Key = KEY_ASTERISK_SFT) and (ssShift in Shift) then Key := KEY_ASTERISK;
    case Key of
      KEY_BACKSPACE:
        begin
          if not(@pOnRemove = nil) then pOnRemove(Self, lselect - 1);
          SelectedLine := SelectedLine - 1;
        end;
      KEY_RETURN, KEY_SPACE:
        if not(@pOnChange = nil) then pOnChange(Self, lselect, ltext, False);
      KEY_INSERT:
        if not(@pOnChange = nil) then pOnChange(Self, lselect, ltext, True);
      KEY_DELETE:
        if not(@pOnRemove = nil) then pOnRemove(Self, lselect);
      KEY_ASTERISK:
        if (ssCtrl in Shift) then
          Stepping.StepBlock := lselect
        else
        begin
          SelectedLine := Stepping.StepBlock;
          ltop := Stepping.StepBlock - (VisibleRowCount div 2);
          if (ltop < 0) then ltop := 0;
          TopRow := ltop;
        end;
      KEY_F2: if (ssCtrl in Shift) then ProgramReset;
      KEY_F8: ProgramStep;
    end;
  end;

  procedure TAsmGrid.pCustomMouseDown(Sender: TObject; Button: TMouseButton;
    Shift: TShiftState; X, Y: Integer);
  var
    ldummy, lrow: Integer;
  begin
    MouseToCell(X, Y, ldummy, lrow);
    if not(lrow < 0) then SelectedLine := lrow;
  end;

  procedure TAsmGrid.pCustomPopupGetOrigin(Sender: TObject);
  var
    lkey: Word;
  begin
    lkey := KEY_ASTERISK;
    pCustomKeyDown(Sender, lkey, []);
  end;

  procedure TAsmGrid.pCustomPopupInsert(Sender: TObject);
  var
    lkey: Word;
  begin
    lkey := KEY_INSERT;
    pCustomKeyDown(Sender, lkey, []);
  end;

  procedure TAsmGrid.pCustomPopupRemove(Sender: TObject);
  var
    lkey: Word;
  begin
    lkey := KEY_DELETE;
    pCustomKeyDown(Sender, lkey, []);
  end;

  procedure TAsmGrid.pCustomPopupReset(Sender: TObject);
  var
    lkey: Word;
  begin
    lkey := KEY_F2;
    pCustomKeyDown(Sender, lkey, [ssCtrl]);
  end;

  procedure TAsmGrid.pCustomPopupSetOrigin(Sender: TObject);
  var
    lkey: Word;
  begin
    lkey := KEY_ASTERISK;
    pCustomKeyDown(Sender, lkey, [ssCtrl]);
  end;

  procedure TAsmGrid.pCustomPopupStep(Sender: TObject);
  var
    lkey: Word;
  begin
    lkey := KEY_F8;
    pCustomKeyDown(Sender, lkey, []);
  end;

  procedure TAsmGrid.pDrawSingleCell(Canvas: TCanvas; ACol, ARow: Integer;
    Rect: TRect; Colors: TColors; Highlight: Boolean);
  var
    lsize: TSize;
    lx, ly: Integer;
  begin
    with Rect do
    begin
      Canvas.Brush.Color := Colors.BG;
      Canvas.FillRect(Rect);
      lsize := Canvas.TextExtent(Cells[ACol, ARow]);
      if (ACol = 0) then
      begin
        lx := (Right - Left - lsize.cx) div 2 + Left;
        Canvas.Pen.Color := Colors.FG;
        Canvas.MoveTo(Right - 1, Top);
        Canvas.LineTo(Right - 1, Bottom);
      end
      else
        lx := Left + Canvas.TextWidth('  ');
      ly := (Bottom - Top - lsize.cy) div 2 + Top;
      if Highlight then
      begin
        Canvas.Brush.Color := ColorTheme.Highlight.BG;
        Canvas.Font.Color := ColorTheme.Highlight.FG;
      end
      else
        Canvas.Font.Color := Colors.FG;
      {$IFDEF UNIX}
      // For compatibility with Lazarus :-(
      Canvas.FillRect(lx, ly + 1, lx + lsize.cx - 1, ly + lsize.cy - 1);
      // ...since Lazarus doesn't use Brush for text background...
      {$ENDIF}
      Canvas.TextOut(lx, ly, Cells[ACol, ARow]);
    end;
  end;

  procedure TAsmGrid.pOnOperand(Sender: TObject);
  begin
    if not(Stepping.StepBlock < 0) then pAskReset := True;
    if not(@pOldOnOperand = nil) then pOldOnOperand(Sender);
  end;

  procedure TAsmGrid.pOnStep(Sender: TObject);
  var
    ltop: Integer;
  begin
    if not(Stepping.StepBlock < 0) then
    begin
      SelectedLine := Stepping.StepBlock;
      ltop := Stepping.StepBlock;
      if (ltop < TopRow) or not(ltop < (TopRow + VisibleRowCount)) then
      begin
        ltop := ltop - (VisibleRowCount div 2);
        if (ltop < 0) then ltop := 0;
        TopRow := ltop;
      end;
    end;
    Self.Repaint;
  end;

  procedure TAsmGrid.pSetColorTheme(ColorTheme: TGridTheme);
  begin
    pColorTheme := ColorTheme;
    Color := ColorTheme.Assembly.None.BG;
  end;

  procedure TAsmGrid.pSetSelectedLine(Line: Integer);
  var
    lselect: TGridRect;
  begin
    if (Line < 0) then Line := 0;
    if (Line > RowCount) then Line := RowCount - 1;
    {$IFDEF UNIX}
    SetColRow(0, Line);
    // Lazarus is really nice, by having incompatible calls :-(
    {$ELSE}
    lselect.Top := Line;
    lselect.Bottom := Line;
    lselect.Left := 0;
    lselect.Right := 1;
    Selection := lselect;
    {$ENDIF}
  end;

  procedure TAsmGrid.pSetStep(Step: TStepping);
  begin
    if not(pStep = nil) then
    begin
      pStep.OnStep := pOldOnStep;
      if not(pStep.Hardware = nil) then
        pStep.Hardware.OnOperand := pOldOnOperand;
    end;
    pStep := Step;
    if not(Step = nil) then
    begin
      pOldOnStep := Step.OnStep;
      Step.OnStep := pOnStep;
      if not(Step.Hardware = nil) then
      begin
        pOldOnOperand := Step.Hardware.OnOperand;
        Step.Hardware.OnOperand := pOnOperand;
      end;
    end;
  end;

  procedure TAsmGrid.pSetWidth(Width: Integer);
  begin
    inherited Width := Width;
    ColWidths[1] := ClientWidth - ColWidths[0];
  end;

  constructor TAsmGrid.CreateGrid(AOwner: TComponent; Step: TStepping);
  begin
    inherited Create(AOwner);
    ColorTheme := THEME_GRID_DEFAULT;
    RowCount := 1;
    ColCount := 2;
    ScrollBars := ssVertical;
    {$IFDEF UNIX}
    Options := [goRangeSelect, goRowSelect, goThumbTracking];
    // Behavior of goRangeSelect differ in Lazarus :-(
    {$ELSE}
    Options := [goRowSelect, goThumbTracking];
    {$ENDIF}
    DefaultDrawing := False;
    OnDrawCell := pCustomDrawCell;
    OnDblClick := pCustomDblClick;
    OnKeyDown := pCustomKeyDown;
    OnMouseDown := pCustomMouseDown;
    InputBox := TAsmInputBox.CreateBox(Self, Self);
    PopupMenu := TPopupMenu.Create(Self);
    with PopupMenu do
    begin
      Items.Add(TMenuItem.Create(PopupMenu));
      with Items[Items.Count - 1] do
      begin
        Caption := POP_ASM_CHANGE;
        OnClick := pCustomDblClick;
        Default := True;
      end;
      Items.Add(TMenuItem.Create(PopupMenu));
      with Items[Items.Count - 1] do
      begin
        Caption := POP_ASM_INSERT;
        OnClick := pCustomPopupInsert;
      end;
      Items.Add(TMenuItem.Create(PopupMenu));
      with Items[Items.Count - 1] do
      begin
        Caption := POP_ASM_REMOVE;
        OnClick := pCustomPopupRemove;
      end;
      Items.Add(TMenuItem.Create(PopupMenu));
      Items[Items.Count - 1].Caption := '-';
      Items.Add(TMenuItem.Create(PopupMenu));
      with Items[Items.Count - 1] do
      begin
        Caption := POP_ASM_STEP;
        OnClick := pCustomPopupStep;
      end;
      Items.Add(TMenuItem.Create(PopupMenu));
      with Items[Items.Count - 1] do
      begin
        Caption := POP_ASM_RESET;
        OnClick := pCustomPopupReset;
      end;
      Items.Add(TMenuItem.Create(PopupMenu));
      Items[Items.Count - 1].Caption := '-';
      Items.Add(TMenuItem.Create(PopupMenu));
      with Items[Items.Count - 1] do
      begin
        Caption := POP_ASM_GET_ORIGIN;
        OnClick := pCustomPopupGetOrigin;
      end;
      Items.Add(TMenuItem.Create(PopupMenu));
      with Items[Items.Count - 1] do
      begin
        Caption := POP_ASM_SET_ORIGIN;
        OnClick := pCustomPopupSetOrigin;
      end;
    end;
    Stepping := Step;
    ReloadInstructions;
  end;

  function TAsmGrid.ChangeInstruction(ARow: Integer; Name: String): Boolean;
  var
    linst: TInstruction;
  begin
    if not(Stepping.StepBlock < 0) then pAskReset := True;
    if (ARow = RowCount - 1) then
    begin
      Result := Self.InsertInstruction(ARow, Name);
      Exit;
    end;
    Stepping.Hardware.Address := ARow;
    linst := Stepping.Hardware.InstructionByName(Name);
    Result := Stepping.ChangeStepBlock(ARow, linst);
    if Result then
    begin
      Objects[1, ARow] := Stepping[ARow];
      Cells[1, ARow] := linst.Name;
    end;
  end;

  function TAsmGrid.ChangeInstruction(Name: String): Boolean;
  begin
    Result := ChangeInstruction(SelectedLine, Name);
  end;

  function TAsmGrid.InsertInstruction(ARow: Integer; Name: String): Boolean;
  var
    i: Integer;
    linst: TInstruction;
  begin
    if not(Stepping.StepBlock < 0) then pAskReset := True;
    Stepping.Hardware.Address := ARow;
    linst := Stepping.Hardware.InstructionByName(Name);
    Result := Stepping.AddStepBlock(ARow, linst);
    if Result then
    begin
      RowCount := RowCount + 1;
      for i := (RowCount - 2) downto ARow do
      begin
        Cells[1, i + 1] := Cells[1, i];
        Cells[0, i + 1] := ZeroPaddedInteger(i + 1, CONST_PADDING);
        Objects[1, i + 1] := Objects[1, i];
      end;
      Objects[1, ARow] := Stepping[ARow];
      Cells[1, ARow] := linst.Name;
      Cells[0, ARow] := ZeroPaddedInteger(ARow, CONST_PADDING);
    end;
  end;

  function TAsmGrid.InsertInstruction(Name: String): Boolean;
  begin
    Result := InsertInstruction(SelectedLine, Name);
  end;

  function TAsmGrid.RemoveInstruction(ARow: Integer): Boolean;
  var
    i: Integer;
  begin
    if not(Stepping.StepBlock < 0) then pAskReset := True;
    Result := Stepping.RemoveStepBlock(ARow);
    if Result then
    begin
      for i := ARow to (RowCount - 2) do
      begin
        Cells[1, i] := Cells[1, i + 1];
        Objects[1, i] := Objects[1, i + 1];
      end;
      RowCount := RowCount - 1;
    end;
  end;

  function TAsmGrid.RemoveInstruction: Boolean;
  begin
    Result := RemoveInstruction(SelectedLine);
  end;

  function TAsmGrid.ReloadInstructions: Boolean;
  var
    i: Integer;
  begin
    RowCount := Stepping.Length + 1;
    for i := 0 to (RowCount - 1) do
    begin
      Cells[0, i] := ZeroPaddedInteger(i, CONST_PADDING);
      Objects[1, i] := Stepping[i];
      if (Objects[1, i] = nil) then Cells[1, i] := ''
      else Cells[1, i] := TStepBlock(Objects[1, i]).CallFunction.Name;
    end;
    Result := True;
  end;

  procedure TAsmGrid.ProgramReset;
  begin
    Stepping.StepBlock := -1;
    pAskReset := False;
  end;

  procedure TAsmGrid.ProgramStep;
  var
    lanswer: Integer;
  begin
    if pAskReset then
    begin
      lanswer := MessageDlg(TEXT_RESET, mtInformation, mbYesNoCancel, 0);
      case lanswer of
        mrYes: ProgramReset;
        mrCancel: Exit;
      end;
      pAskReset := False;
    end;
    if (Stepping.StepBlock < 0) then
      begin
        LogSystemTime;
        LogWrite(POP_ASM_CHECKING);
        if Stepping.Valid then
        begin
          LogWrite(POP_ASM_DONE_RUNNING);
          Stepping.SingleStep;
          if (Stepping.StepBlock < 0) then LogWrite(POP_ASM_EXEC_STOPPED);
        end
        else LogWrite(POP_ASM_ERRORS_FOUND);
        Exit;
      end;
    Stepping.SingleStep;
    if (Stepping.StepBlock < 0) then LogWrite(POP_ASM_EXEC_STOPPED);
  end;

end.
