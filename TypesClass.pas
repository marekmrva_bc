unit TypesClass;

interface

uses
  Graphics;

type

  PLine = ^TLine;
  TLine = record
    Line: String;
    Number: Integer;
  end;
  TLines = array of TLine;

  TFloatRecord = record
    Significand: Int64;
    Exponent: Word;
    Sign: Boolean;
  end;

  TSeparateConstants = record
    ExponCmp: Word;
    SigniCmp, SigniAnd: Int64;
  end;

  TTranslateConstants = record
    PosInf, NegInf, QNaN, SNaN: String;
  end;

  PSingle = ^Single;
  PDouble = ^Double;
  PExtended = ^Extended;

  PBCDRecord = ^TBCDRecord;
  TBCDRecord = packed record
    Bytes: array [0..9] of Byte;
  end;

  TFlags = (Carry, Reserved1, Parity, Reserved3, AuxiliaryCarry, Reserved5,
    Zero, Sign, Trap, InterruptEnable, Direction, Overflow, IOPL_0, IOPL_1,
    NestedTask, Reserved15, Resume, Virtual8086, Alignment, VirtualInterrupt,
    VirtualInterruptPending, ID, Reserved22, Reserved23, Reserved24, Reserved25,
    Reserved26, Reserved27, Reserved28, Reserved29, Reserved30, Reserved31);
  TEFlags = set of TFlags;

  TFPUState = packed record
    ControlWord, dumm1: Word;
    StatusWord, dummy2: Word;
    TagWord, dummy3: Word;
    IP_Offset: Cardinal;
    IP_Selector, Opcode: Word;
    OP_Offset: Cardinal;
    OP_Selector, dummy4: Word;
    ST: array[0..7] of Extended;
  end;

  THardwareState = record
    EFlags: Cardinal;
    Reg_EAX: Cardinal;
    FPUState: TFPUState;
  end;

  TColors = record
    FG, BG: TColor;
  end;

  TColorLine = record
    None, Selected: TColors;
  end;

  TGridTheme = record
    Address, Assembly: TColorLine;
    Highlight, Step: TColors;
  end;

  TViewTheme = record
    Line: TColorLine;
    Change: TColor;
  end;

  TChangeEvent = procedure(Sender: TObject) of object;
  TInsertEvent = procedure(Sender: TObject; ARow: Integer; Text: String; Insert: Boolean) of object;
  TRemoveEvent = procedure(Sender: TObject; ARow: Integer) of object;
  TLogWriteEvent = procedure(Sender: TObject; Log: String; Error: Boolean = False) of object;
  TLogClearEvent = TChangeEvent;

  TStrings = array of String;
  TOperandType = Char;
  TBranchType = Char;

  PInstructionRecord = ^TInstructionRecord;
  TInstructionRecord = record
    Name, Code, Description: String;
    Branch: TBranchType;
  end;

  POperandRecord = ^TOperandRecord;
  TOperandRecord = record
    Name, Code: String;
    OperandType: TOperandType;
    Default: String;
    Data: PChar;
  end;

  TOperandTypeRecord = record
    OperandType: TOperandType;
    Description: String;
  end;

implementation

end.
