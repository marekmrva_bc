unit OperandEditClass;

interface

uses
  ConstantsClass, HardwareClass, ResourcesClass,
  Classes, Controls, Forms, Graphics, StdCtrls;

type

  { TOperandEdit }

    TOperandEdit = class(TForm)
    private
      pBox: TEdit;
      pChange, pCancel: TButton;
      pDesc, pLabel: TLabel;
      pHardware: THardware;
      procedure pOnButtonClick(Sender: TObject); virtual;
      procedure pOnShow(Sender: TObject); virtual;
    public
      constructor CreateBox(AOwner: TComponent); virtual;
      function ShowBox(Hw: THardware): Boolean; virtual;
    end;

implementation

// ************************************************************************** //
// *                      TOperandEdit implementation                       * //
// ************************************************************************** //

  procedure TOperandEdit.pOnButtonClick(Sender: TObject);
  begin
    if pHardware.ValidateOperand(pBox.Text) then
    begin
      pHardware.OperandAdd(pBox.Text);
      ModalResult := mrOk;
    end
    else
    begin
      pLabel.Caption := pHardware.LastError;
    end;
  end;

  procedure TOperandEdit.pOnShow(Sender: TObject);
  begin
    pLabel.Caption := '';
    pBox.Text := '';
    pBox.SetFocus;
  end;

  constructor TOperandEdit.CreateBox(AOwner: TComponent);
  begin
    inherited CreateNew(AOwner, 0);
    Caption := OP_EDIT_CAPTION;
    Position := poDesktopCenter;
    ClientWidth := 321;
    ClientHeight := 101;
    OnShow := pOnShow;
    BorderStyle := bsDialog;
    pBox := TEdit.Create(Self);
    with pBox do
    begin
      Parent := Self;
      Text := '';
      Left := 8;
      Top := 30;
      Width := 309;
      Height := 21;
    end;
    pChange := TButton.Create(Self);
    with pChange do
    begin
      Parent := Self;
      Caption := OP_EDIT_OK;
      Left := 193;
      Top := 77;
      Width := 60;
      Height := 20;
      OnClick := pOnButtonClick;
      Default := True;
    end;
    pCancel := TButton.Create(Self);
    with pCancel do
    begin
      Parent := Self;
      Caption := OP_EDIT_CANCEL;
      Left := 257;
      Top := 77;
      Width := 60;
      Height := 20;
      ModalResult := mrCancel;
      Cancel := True;
    end;
    pLabel := TLabel.Create(Self);
    with pLabel do
    begin
      Parent := Self;
      Caption := '';
      Left := 8;
      Top := 57;
      Width := 309;
      Height := 21;
      Font.Color := clRed;
    end;
    pDesc := TLabel.Create(Self);
    with pDesc do
    begin
      Parent := Self;
      Caption := OP_EDIT_DESCRIPTION;
      Left := 8;
      Top := 8;
      Width := 309;
      Height := 21;
    end;
  end;

  function TOperandEdit.ShowBox(Hw: THardware): Boolean;
  begin
    pHardware := Hw;
    Result := (Self.ShowModal = mrOk);
  end;

end.

