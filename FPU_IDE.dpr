program FPU_IDE;

uses
  Forms,
  FPUFormClass in 'FPUFormClass.pas' {ViewFPU};

{$R *.res}

begin
  Application.Initialize;
  Application.Title := 'FPU IDE';
  Application.CreateForm(TViewFPU, ViewFPU);
  Application.Run;
end.
