unit HardwareClass;

interface

uses
  ConstantsClass, FunctionsClass, PrefixTreeClass, ResourcesClass, TypesClass;

type

  TInstruction = class;

  { THardwareState }

  THardware = class
  private
    pAddress, pMaxAddress: Integer;
    pError: String;
    pInstructions, pOperands: TPrefixTree;
    pOldState, pState: THardwareState;
    pOnOperand, pOnState: TChangeEvent;
    function pErrorSet: Boolean; virtual;
    function pInstFromName(Name: String; var Care: Boolean): TInstruction; virtual;
    function pOperandAt(Input: String; Position: Integer): TOperandType; virtual;
    function pOperandToType(Input: String): TOperandType; virtual;
    function pParseInstr(var Input: String; InType: Integer): TStrings; virtual;
    function pParseOperand(Input: String): TStrings; virtual;
    function pTranslateOperands(Input: String): String; virtual;
    function pTranslateInput(Input: TStrings; var Tree: TPrefixTree; var Position: Integer): Boolean; virtual;
    function pTypeToDesc(Input: TOperandType): TStrings; virtual;
    function pTypeToOperands(Input: TOperandType): TStrings; virtual;
    function pTStringsToInst(Input: TStrings; Last: Integer): String; virtual;
    procedure pAddOperandByRecord(Input: POperandRecord); virtual;
    procedure pSetError(Input: String = GLOB_NO_ERROR; Description: String = ''); virtual;
    procedure pSetState(Input: THardwareState); virtual;
  public
    constructor Create; virtual;
    function DescriptionsByPrefix(Prefix: String): TStrings; virtual;
    function InstructionByName(Name: String): TInstruction; virtual;
    function InstructionsByPrefix(Prefix: String): TStrings; virtual;
    function OperandAdd(Operand: String): Boolean; virtual;
    function OperandRemove(Operand: String): Boolean; virtual;
    function ValidateInstruction(Instruction: TInstruction): Boolean; overload; virtual;
    function ValidateInstruction(Name: String): Boolean; overload; virtual;
    function ValidateOperand(Operand: String): Boolean; virtual;
    procedure InitializeState; virtual;
    procedure InitializeOperands; virtual;
    destructor Destroy; override;
    property LastError: String read pError;
    property Address: Integer read pAddress write pAddress default 0;
    property MaxAddress: Integer read pMaxAddress write pMaxAddress default 0;
    property OldState: THardwareState read pOldState write pOldState;
    property OnOperand: TChangeEvent read pOnOperand write pOnOperand;
    property OnState: TChangeEvent read pOnState write pOnState;
    property Operands: TPrefixTree read pOperands;
    property State: THardwareState read pState write pSetState;
  end;

  { TInstruction }

  TInstruction = class
  private
    pHardware: THardware;
    pName, pCode, pDescription: String;
    pBranch: TBranchType;
    pAddr: String;
    function pGetCode: String;
    procedure pSetCode(Code: String);
  public
    constructor Create; virtual;
    function Execute: Boolean; virtual;
    property Hardware: THardware read pHardware write pHardware;
    property Name: String read pName write pName;
    property Code: String read pGetCode write pSetCode;
    property Branch: TBranchType read pBranch write pBranch;
    property BranchAddress: String read pAddr write pAddr;
    property Description: String read pDescription write pDescription;
  end;

  { Static Functions }

  function CompareStateCondition(Hardware: THardware; Position: Integer): Boolean;
  function CompareStateException(Hardware: THardware; Position: Integer): Boolean;
  function CompareStateFlag(Hardware: THardware; Position: Integer): Boolean;
  function CompareStateMask(Hardware: THardware; Position: Integer): Boolean;
  function CompareStatePrecision(Hardware: THardware): Boolean;
  function CompareStateRound(Hardware: THardware): Boolean;
  function CompareStateStack(Hardware: THardware; Position: Integer): Boolean;
  function CompareStateTag(Hardware: THardware; Position: Integer): Boolean;
  function GetCondition(State: THardwareState; Position: Integer): Boolean;
  function GetException(State: THardwareState; Position: Integer): Boolean;
  function GetFlag(State: THardwareState; Position: Integer): Boolean;
  function GetMask(State: THardwareState; Position: Integer): Boolean;
  function GetPrecision(State: THardwareState): Integer;
  function GetRound(State: THardwareState): Integer;
  function GetTag(State: THardwareState; Position: Integer): Integer;
  procedure SetCondition(var State: THardwareState; Position: Integer; Condition: Boolean);
  procedure SetException(var State: THardwareState; Position: Integer; Exception: Boolean);
  procedure SetFlag(var State: THardwareState; Position: Integer; Flag: Boolean);
  procedure SetMask(var State: THardwareState; Position: Integer; Mask: Boolean);
  procedure SetPrecision(var State: THardwareState; Precision: Integer);
  procedure SetRound(var State: THardwareState; Round: Integer);
  procedure SetTag(var State: THardwareState; Position, Tag: Integer);

implementation

// ************************************************************************** //
// *                     THardwareState implementation                      * //
// ************************************************************************** //

  function THardware.pErrorSet: Boolean;
  begin
    Result := not(pError = GLOB_NO_ERROR);
  end;

  function THardware.pInstFromName(Name: String; var Care: Boolean
    ): TInstruction;
  var
    i, llength: Integer;
    lstrings: TStrings;
    lcode: String;
    ltree: TPrefixTree;
    linst: PInstructionRecord;
    loper: POperandRecord;
    lcare: Boolean;
  begin
    lcare := Care;
    Care := False;
    pSetError;
    Result := nil;
    lstrings := pParseInstr(Name, INTYPE_INSTRUCTION);
    if pErrorSet then Exit;
    i := 0;
    while pTranslateInput(lstrings, ltree, i) do;
    if pErrorSet then Exit;
    if lcare then
    begin
      linst :=  PInstructionrecord(ltree.Data);
      Result := TInstruction.Create;
      Result.Name := Name;
      Result.Code := linst^.Code;
      Result.Description := linst^.Description;
      Result.Branch := linst^.Branch;
      Result.Hardware := Self;
      for i := 1 to (Length(lstrings) - 1) do
      begin
        if IsAddress(lstrings[i]) then
        begin
          if (Result.Branch = BRANCH_BRANCH) then
            Result.BranchAddress := lstrings[i];
          Continue;
        end;
        ltree := pOperands.GetDescendant(lstrings[i]);
        loper := POperandRecord(ltree.Data);
        lcode := loper^.Code;
        llength := Length(Result.Code);
        if (llength > 0) and (Length(lcode) > 0) then
        begin
          lcode[1] := Chr(Ord(lcode[1]) xor Ord(Result.Code[llength]));
          Result.Code := RemoveCharacter(Result.Code);
        end;
        Result.Code := Result.Code + lcode;
      end;
    end;
    Care := True;
  end;


  function THardware.pOperandAt(Input: String; Position: Integer): TOperandType;
  var
    i: Integer;
  begin
    Result := #0;
    if not(Position > 0) then Exit;
    for i := 1 to Length(Input) do
    begin
      if (Position = 0) then
      begin
        Result := Input[i];
        Break;
      end;
      if (Input[i] = FPU_SPACE) then Position := Position - 1;
    end;
  end;

  function THardware.pOperandToType(Input: String): TOperandType;
  var
    ltree: TPrefixTree;
  begin
    if IsAddress(Input) then
    begin
      Result := FPU_OPERAND_ADDR;
      Exit;
    end;
    ltree := pOperands.GetDescendant(Input);
    if ValidPrefixTree(ltree) then
      Result := POperandRecord(ltree.Data)^.OperandType
    else
    begin
      pSetError(INST_OPER_UNKNOWN, Input);
      Result := FPU_OPERAND_ERROR;
    end;
  end;

  function THardware.pParseInstr(var Input: String; InType: Integer): TStrings;
  var
    lname: String;
    lspace: Boolean;
    i: Integer;
  begin
    SetLength(Result, 1);
    Result[0] := '';
    if (Input = '') then
    begin
      pSetError(INST_NONE);
      Exit;
    end;
    lspace := (Input[Length(Input)] = ' ');
    if not(InType = INTYPE_DESCRIPTION) then
      for i := 1 to Length(Input) do
        if (Input[i] in CHARS_CONTROL) then
        begin
          pSetError(INST_CHAR_INVALID, Input[i]);
          Exit;
        end;
    Input := TrimCharacter(Input, ' ');
    Input := OmmitEverywhere(Input, '(', ' ');
    Input := OmmitEverywhere(Input, ')', ' ');
    Input := OmmitEverywhere(Input, ',', ' ');
    Input := NeutralizeDoubles(Input, ' ');
    Input := UpperCase(Input);
    if (Input = '') then
    begin
      pSetError(INST_NONE);
      Exit;
    end;
    lname := Input;
    Result := MergeStringTStrings(
                lname, ParseToStrings(PChar(ParseFirst(lname, ' ')), ','));
    if ((Intype = INTYPE_DESCRIPTION) or (InType = INTYPE_PREFIX)) and
      (Length(Result) = 1) and lspace then
      Result := MergeTStringsString(Result, '');
  end;

  function THardware.pParseOperand(Input: String): TStrings;
  var
    i: Integer;
  begin
    Input := TrimCharacter(Input, ' ');
    Input := OmmitEverywhere(Input, OPER_SEP, ' ');
    Input := UpperCase(Input);
    SetLength(Result, 2);
    Result := ParseToStrings(PChar(Input), OPER_SEP);
    if not(Length(Result) = 2) then
    begin
      pSetError(INST_OPER_TYPE_INVALID, Input);
      Exit;
    end;
    for i := 0 to (Length(Result[0]) - 1) do
      if (Result[0][i] in CHARS_CONTROL) then
      begin
        pSetError(INST_CHAR_INVALID, Result[0][i]);
        Exit;
      end;
    if ValidPrefixTree(pOperands.GetDescendant(Result[0])) then
    begin
      pSetError(INST_OPER_NAME_EXISTS, Result[0]);
      Exit;
    end;
    for i := 0 to (Length(sOperandOrdinals) - 1) do
      if (sOperandOrdinals[i].Name = Result[1]) then Exit;
    pSetError(INST_OPER_TYPE_UNKNOWN, Result[1]);
  end;

  function THardware.pTranslateOperands(Input: String): String;
  var
    i: Integer;
    ltype: TOperandType;
  begin
    Result := ParseBeforeFirst(Input, FPU_SPACE);
    i := 1;
    ltype := pOperandAt(Input, i);
    if not(ltype = #0) then Result := Result + ' ' + pTypeToDesc(ltype)[0];
    repeat
      i := i + 1;
      ltype := pOperandAt(Input, i);
      if (ltype = #0) then Break;
      Result := Result + ',' + pTypeToDesc(ltype)[0];
    until False;
  end;

  function THardware.pTranslateInput(Input: TStrings; var Tree: TPrefixTree;
    var Position: Integer): Boolean;
  var
    ltype: TOperandType;
    ltree: TPrefixTree;
  begin
    Result := False;
    if not(Length(Input) > 0) then Exit;
    if (Position < 0) then Exit;
    if (Position = 0) then
    begin
      ltree := pInstructions;
      Tree := ltree;
    end
    else ltree := Tree;
    if (Position = Length(Input)) then
    begin
      if not ValidPrefixTree(ltree) then
      begin
        pSetError(INST_OPER_NOT_ENOUGH);
        Exit;
      end;
      Position := Position + 1;
    end;
    if not(Position < Length(Input)) then Exit;
    if (ltree = nil) then Exit;
    if ValidPrefixTree(ltree) and (Length(ltree.GetAllDescendants) = 1) then
    begin
      pSetError(INST_OPER_TOO_MANY);
      Exit;
    end;
    if not(Position = 0) then
    begin
      ltype := pOperandToType(Input[Position]);
      if (ltype = FPU_OPERAND_ERROR) then
      begin
        pSetError(INST_OPER_UNKNOWN, Input[Position]);
        Exit;
      end;
      if (Position = 1) then ltree := ltree.GetDescendant(ltype)
      else ltree := ltree.GetDescendant(FPU_SPACE + ltype);
      if (ltree = nil) then
      begin
        pSetError(INST_OPER_INVALID);
        Exit;
      end;
    end
    else
    begin
      ltree := ltree.GetDescendant(Input[0] + FPU_SPACE);
      if (ltree = nil) then
      begin
        pSetError(INST_INST_UNKNOWN, Input[0]);
        Exit;
      end;
      if (Length(Input) = 1) then
      begin
        ltree := ltree.GetDescendant(FPU_OPERAND_NONE);
        if not ValidPrefixTree(ltree) then
        begin
          pSetError(INST_OPER_NOT_ENOUGH);
          Exit;
        end;
      end;
    end;
    Position := Position + 1;
    Tree := ltree;
    Result := True;
  end;

  function THardware.pTypeToDesc(Input: TOperandType): TStrings;
  var
    i: Integer;
  begin
    SetLength(Result, 1);
    Result[0] := '<' + sOperandTypes[0].Description + '>';
    for i := 1 to (Length(sOperandTypes) - 1) do
      if (sOperandTypes[i].OperandType = Input) then
      begin
        Result[0] := '<' + sOperandTypes[i].Description + '>';
        Break;
      end;
  end;

  function THardware.pTypeToOperands(Input: TOperandType): TStrings;
  var
    i: Integer;
    ltrees: TPrefixTrees;
    loperand: POperandRecord;
  begin
    SetLength(Result, 0);
    ltrees := pOperands.GetAllDescendants;
    for i := 0 to (Length(ltrees) - 1) do
    begin
      loperand := POperandRecord(ltrees[i].Data);
      if (loperand^.OperandType = Input) then
        Result := MergeTStringsString(Result, loperand^.Name);
    end;
    if (Input = FPU_OPERAND_ADDR) then Result := MergeTStringsString(Result,
      '<0 - ' + ZeroPaddedInteger(MaxAddress) + '>');
    if (Length(Result) = 0) then pSetError(INST_OPER_UNKNOWN);
  end;

  function THardware.pTStringsToInst(Input: TStrings; Last: Integer): String;
  var
    i: Integer;
  begin
    Result := '';
    if not(Length(Input) > 0) then Exit;
    if not(Last < Length(Input)) then Exit;
    Result := Input[0];
    if (Last < 0) then Exit;
    Result := Result + ' ';
    for i := 1 to Last do
      if not(Input[i] = '') then
        Result := Result + Input[i] + ',';
  end;

  procedure THardware.pAddOperandByRecord(Input: POperandRecord);
  begin
    if not(Input^.Default = '') then
    with Input^ do
    begin
      Data := GetMemory(Length(Default));
      Move(PChar(Default)[0], Data^, Length(Default));
      Code := Code + AddressToString(Data);
    end;
    pOperands.Add(Input^.Name, Input);
  end;

  procedure THardware.pSetError(Input, Description: String);
  begin
    pError := Input;
    if not(Description = '') then pError := pError + ': "' + Description + '"';
  end;

  procedure THardware.pSetState(Input: THardwareState);
  begin
    pState := Input;
    if not(@OnState = nil) then OnState(Self);
  end;

  constructor THardware.Create;
  var
    i: Integer;
  begin
    pSetError;
    InitializeState;
    pInstructions := TPrefixTree.Create;
    for i := 0 to (Length(sInstructions) - 1) do
      pInstructions.Add(sInstructions[i].Name, @sInstructions[i]);
    InitializeOperands;
  end;

  function THardware.DescriptionsByPrefix(Prefix: String): TStrings;
  var
    i, j, lpos: Integer;
    linst: PInstructionRecord;
    linstructions, lnames, lstrings: TStrings;
    lname: String;
    ltree: TPrefixTree;
    ltrees: TPrefixTrees;
  begin
    SetLength(Result, 0);
    SetLength(lnames, 0);
    SetLength(lstrings, 0);
    SetLength(ltrees, 0);
    linstructions := InstructionsByPrefix(Prefix);
    for i := 0 to (Length(linstructions) - 1) do
    begin
      lstrings := pParseInstr(linstructions[i], INTYPE_DESCRIPTION);
      lpos := 0;
      while pTranslateInput(lstrings, ltree, lpos) do;
      ltrees := ltree.GetAllDescendants;
      for j := 0 to (Length(ltrees) - 1) do
      begin
        lname := PInstructionRecord(ltrees[j].Data)^.Name;
        lnames := RemoveExactString(lnames, lname);
        lnames := MergeTStringsString(lnames, lname);
      end;
    end;
    for i := 0 to (Length(lnames) - 1) do
    begin
      linst := PInstructionRecord(pInstructions.GetDescendant(lnames[i]).Data);
      lname := pTranslateOperands(linst^.Name) + ' - ' + linst^.Description;
      RemoveExactString(Result, lname);
      Result := MergeTStringsString(Result, lname);
    end;
    pSetError;
  end;

  function THardware.InstructionByName(Name: String): TInstruction;
  var
    lcare: Boolean;
  begin
    lcare := True;
    Result := pInstFromName(Name, lcare);
  end;

  function THardware.InstructionsByPrefix(Prefix: String): TStrings;
  var
    i, j, lpos: Integer;
    lstrings, loperands: TStrings;
    lprefix, lopers: String;
    ltree: TPrefixTree;
    ltrees: TPrefixTrees;
    linst: PInstructionRecord;
  begin
    try
      SetLength(Result, 0);
      SetLength(loperands, 0);
      SetLength(ltrees, 0);
      lstrings := pParseInstr(Prefix, INTYPE_PREFIX);
      if not(lstrings[Length(lstrings)] = '') then
        lstrings[Length(lstrings) - 1] := '';
      lpos := 0;
      while pTranslateInput(lstrings, ltree, lpos) do;
      if (lpos = 0) and (Length(lstrings) > 1) then Exit;
      ltrees := ltree.GetAllDescendants;
      lprefix := pTStringsToInst(lstrings, lpos - 1);
      for i := 0 to (Length(ltrees) - 1) do
      begin
        linst := PInstructionRecord(ltrees[i].Data);
        if (lpos = 0) then
        begin
          if IsPrefixOf(Prefix, linst^.Name) then
          begin
            lopers := ParseBeforeFirst(linst^.Name, FPU_SPACE) + ' ';
            Result := RemoveExactString(Result, lopers);
            Result := MergeTStringsString(Result, lopers);
          end;
        end
        else
        begin
          loperands := pTypeToOperands(pOperandAt(linst^.Name, lpos));
          if (Length(loperands) = 0) then
            loperands := pTypeToDesc(pOperandAt(linst^.Name, lpos));
          loperands := CartesianOfStrings(lprefix, loperands);
          for j := 0 to (Length(loperands) - 1) do
            if IsPrefixOf(Prefix, loperands[j]) then
            begin
              if not(pOperandAt(linst^.Name, lpos + 1) = #0) then
                loperands[j] := loperands[j] + ',';
              Result := RemoveExactString(Result, loperands[j]);
              Result := MergeTStringsString(Result, loperands[j]);
          end;
        end;
      end;
    finally
      pSetError;
    end;
  end;

  function THardware.OperandAdd(Operand: String): Boolean;
  var
    i: Integer;
    lstrings: TStrings;
    lrecord: POperandRecord;
  begin
    pSetError;
    Result := False;
    lstrings := pParseOperand(Operand);
    if pErrorSet then Exit;
    for i := 0 to (Length(sOperandOrdinals) - 1) do
      with sOperandOrdinals[i] do
      if (Name = lstrings[1]) and not(Default = '') then
      begin
        New(lrecord);
        lrecord^ := sOperandOrdinals[i];
        lrecord^.Name := lstrings[0];
        pAddOperandByRecord(lrecord);
        Result := True;
        Break;
      end;
    if Result and not(@OnOperand = nil) then OnOperand(Self);
  end;

  function THardware.OperandRemove(Operand: String): Boolean;
  var
    i: Integer;
    ltree: TPrefixTree;
  begin
    pSetError;
    Result := False;
    Operand := UpperCase(TrimCharacter(Operand, ' '));
    for i := 0 to (Length(Operand) - 1) do
      if (Operand[i] in CHARS_CONTROL) then
      begin
        pSetError(INST_CHAR_INVALID, Operand[i]);
        Exit;
      end;
    ltree := pOperands.GetDescendant(Operand);
    if ValidPrefixTree(ltree) then
    begin
      Dispose(POperandRecord(ltree.Data));
      pOperands.Remove(Operand);
      Result := True;
    end
    else
      pSetError(INST_OPER_NAME_UNKNOWN, Operand);
    if Result and not(@OnOperand = nil) then OnOperand(Self);
  end;

  function THardware.ValidateInstruction(Instruction: TInstruction): Boolean;
  var
    linst: TInstruction;
  begin
    Result := ValidateInstruction(Instruction.Name);
    if Result then
    begin
      linst := InstructionByName(Instruction.Name);
      if not(Instruction.Code = linst.Code) then Instruction.Code := linst.Code;
      linst.Free;
    end;
  end;

  function THardware.ValidateInstruction(Name: String): Boolean;
  var
    lcare: Boolean;
  begin
    lcare := False;
    pInstFromName(Name, lcare);
    Result := lcare;
  end;

  function THardware.ValidateOperand(Operand: String): Boolean;
  begin
    pSetError;
    pParseOperand(Operand);
    Result := not pErrorSet;
  end;

  procedure THardware.InitializeState;
  var
    ldummy: TFPUState;
    lstate: THardwareState;
    i: Integer;
  begin
    asm
      fnsave [ldummy]
      fnsave [lstate.FPUState]
      frstor [ldummy]
      pushfd
      pop dword ptr [lstate.EFlags]
    end;
    lstate.Reg_EAX := 0;
    for i := 0 to 7 do
      lstate.FPUState.ST[i] := 0;
    State := lstate;
    OldState := lstate;
  end;

  procedure THardware.InitializeOperands;
  var
    i: Integer;
  begin
    pOperands.Free;
    pOperands := TPrefixTree.Create;
    for i := 0 to (Length(sOperands) - 1) do
      pAddOperandByRecord(@sOperands[i]);
    if not(@OnOperand = nil) then OnOperand(Self);
  end;

  destructor THardware.Destroy;
  begin
    pInstructions.Free;
    pOperands.Free;
  end;

// ************************************************************************** //
// *                     TInstruction implementation                        * //
// ************************************************************************** //

  function TInstruction.pGetCode: String;
  begin
    Result := RemoveCharacter(pCode);
  end;

  procedure TInstruction.pSetCode(Code: String);
  begin
    pCode := Code + INST_OPCODE_RET;
  end;

  constructor TInstruction.Create;
  begin
    Name := '';
    Code := '';
    Description := '';
    Branch := BRANCH_NORMAL;
  end;

  function TInstruction.Execute: Boolean;
  var
    lstate: THardwareState;
  begin
    if (Branch = BRANCH_UNSUPPORTED) then
    begin
      Result := False;
      LogWrite(INST_INST_UNSUPPORTED, True);
      Exit;
    end;
    lstate := pHardware.State;
    asm
      push 0
      push offset @return
      mov eax, Self
      push [TInstruction(eax).pCode]
      frstor [lstate.FPUState]
      push dword ptr [lstate.EFlags]
      push dword ptr [lstate.Reg_EAX]
      pop eax
      popfd
      ret
      @return:
      pushfd
      push eax
      pop dword ptr [lstate.Reg_EAX]
      pop dword ptr [lstate.EFlags]
      fnsave [lstate.FPUState]
      pop eax
      mov Result, al
    end;
    pHardware.OldState := pHardware.State;
    pHardware.State := lstate;
  end;

// ************************************************************************** //
// *                     Static Functions implementation                    * //
// ************************************************************************** //

  function CompareStateCondition(Hardware: THardware; Position: Integer
    ): Boolean;
  var
    lcondition: Boolean;
  begin
    lcondition := GetCondition(Hardware.State, Position);
    Result := (lcondition = GetCondition(Hardware.pOldState, Position));
  end;

  function CompareStateException(Hardware: THardware; Position: Integer
    ): Boolean;
  var
    lexception: Boolean;
  begin
    lexception := GetException(Hardware.State, Position);
    Result := (lexception = GetException(Hardware.OldState, Position));
  end;

  function CompareStateFlag(Hardware: THardware; Position: Integer): Boolean;
  begin
    with Hardware do
      Result := (GetFlag(State, Position) = GetFlag(pOldState, Position));
  end;

  function CompareStateMask(Hardware: THardware; Position: Integer): Boolean;
  var
    lexception: Boolean;
  begin
    lexception := GetMask(Hardware.State, Position);
    Result := (lexception = GetMask(Hardware.OldState, Position));
  end;

  function CompareStatePrecision(Hardware: THardware): Boolean;
  var
    lprecision: Integer;
  begin
    lprecision := GetPrecision(Hardware.State);
    Result := (lprecision = GetPrecision(Hardware.OldState));
  end;

  function CompareStateRound(Hardware: THardware): Boolean;
  begin
    Result := (GetRound(Hardware.State) = GetRound(Hardware.OldState));
  end;

  function CompareStateStack(Hardware: THardware; Position: Integer): Boolean;
  var
    lsecond: PChar;
  begin
    Result := False;
    if (Position < 0) or (Position > 7) then Exit;
    lsecond := @Hardware.OldState.FPUState.ST[Position];
    with Hardware.State.FPUState do
      Result := StringCompare(@ST[Position], lsecond, SizeOf(ST[Position]));
  end;

  function CompareStateTag(Hardware: THardware; Position: Integer): Boolean;
  var
    ltag: Integer;
  begin
    ltag := GetTag(Hardware.State, Position);
    Result := (ltag = GetTag(Hardware.OldState, Position));
  end;

  function GetCondition(State: THardwareState; Position: Integer): Boolean;
  begin
    Result := False;
    if (Position < 0) or (Position > 3) then Exit;
    if (Position = 3) then Position := 14
    else Position := Position + 8;
    Result := not((State.FPUState.StatusWord and (1 shl Position)) = 0);
  end;

  function GetException(State: THardwareState; Position: Integer): Boolean;
  begin
    Result := False;
    if (Position < 0) or (Position > 7) then Exit;
    Result := not((State.FPUState.StatusWord and (1 shl Position)) = 0);
  end;

  function GetFlag(State: THardwareState; Position: Integer): Boolean;
  begin
    Result := False;
    if (Position < 0) or (Position > 2) then Exit;
    case Position of
      1: Position := 2;
      2: Position := 6;
    end;
    Result := not((State.EFlags and (1 shl Position)) = 0);
  end;

  function GetMask(State: THardwareState; Position: Integer): Boolean;
  begin
    Result := False;
    if (Position < 0) or (Position > 5) then Exit;
    Result := not((State.FPUState.ControlWord and (1 shl Position)) = 0);
  end;

  function GetPrecision(State: THardwareState): Integer;
  begin
    Result := (State.FPUState.ControlWord shr 8) and 3;
  end;

  function GetRound(State: THardwareState): Integer;
  begin
    Result := (State.FPUState.ControlWord shr 10) and 3;
  end;

  function GetTag(State: THardwareState; Position: Integer): Integer;
  var
    ltop: Integer;
  begin
    Result := -1;
    if (Position < 0) or (Position > 7) then Exit;
    with State.FPUState do
    begin
      ltop := (Position + ((StatusWord shr 11) and 7)) mod 8;
      Result := (TagWord shr (2 * ltop)) and 3;
    end;
  end;

  procedure SetCondition(var State: THardwareState; Position: Integer;
    Condition: Boolean);
  begin
    if (Position < 0) or (Position > 7) then Exit;
    if (Position = 3) then Position := 14
    else Position := Position + 8;
    with State.FPUState do
      if Condition then StatusWord := StatusWord or (1 shl Position)
      else StatusWord := StatusWord and not(1 shl Position);
  end;

  procedure SetException(var State: THardwareState; Position: Integer;
    Exception: Boolean);
  begin
    if (Position < 0) or (Position > 7) then Exit;
    with State.FPUState do
      if Exception then StatusWord := StatusWord or (1 shl Position)
      else StatusWord := StatusWord and not(1 shl Position);
  end;

  procedure SetFlag(var State: THardwareState; Position: Integer; Flag: Boolean
    );
  begin
    if (Position < 0) or (Position > 2) then Exit;
    case Position of
      1: Position := 2;
      2: Position := 6;
    end;
    if Flag then State.EFlags := State.EFlags or (1 shl Position)
    else State.EFlags := State.EFlags and not(1 shl Position);
  end;

  procedure SetMask(var State: THardwareState; Position: Integer; Mask: Boolean
    );
  begin
    if (Position < 0) or (Position > 5) then Exit;
    with State.FPUState do
      if Mask then ControlWord := ControlWord or (1 shl Position)
      else ControlWord := ControlWord and not(1 shl Position);
  end;

  procedure SetPrecision(var State: THardwareState; Precision: Integer);
  var
    lcw: Word;
  begin
    if (Precision < 0) or (Precision > 3) then Exit;
    with State.FPUState do
    begin
      lcw := ControlWord and not(3 shl 8);
      ControlWord := lcw or (Precision shl 8);
    end;
  end;

  procedure SetRound(var State: THardwareState; Round: Integer);
  var
    lcw: Word;
  begin
    if (Round < 0) or (Round > 3) then Exit;
    with State.FPUState do
    begin
      lcw := ControlWord and not(3 shl 10);
      ControlWord := lcw or (Round shl 10);
    end;
  end;

  procedure SetTag(var State: THardwareState; Position, Tag: Integer);
  var
    ltop, ltag: Integer;
  begin
    if (Position < 0) or (Position > 7) or (Tag < 0) or (Tag > 3) then Exit;
    with State.FPUState do
    begin
      ltop := (Position + ((StatusWord shr 11) and 7)) mod 8;
      ltag := TagWord and not(3 shl (2 * ltop));
      TagWord := ltag or (Tag shl (2 * ltop));
    end;
  end;
end.

