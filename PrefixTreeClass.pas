unit PrefixTreeClass;

interface

type

  TPrefixTree = class;
  TPrefixTrees = array of TPrefixTree;

  { TPrefixTree }

  TPrefixTree = class
  private
    pName: String;
    pData: Pointer;
    pValid: Boolean;
    pDescendants: array [0..255] of TPrefixTree;
    function pGetDescendant(Letter: Char): TPrefixTree; virtual;
    function pJoinTrees(First, Second: TPrefixTrees): TPrefixTrees; virtual;
    procedure pSetDescendant(Letter: Char; Descendant: TPrefixTree); virtual;
  public
    constructor Create; virtual;
    function GetAllDescendants: TPrefixTrees; virtual;
    function GetDescendant(Name: String): TPrefixTree; virtual;
    function GetDirectDescendants: TPrefixTrees; virtual;
    procedure Add(Name: String; Data: Pointer); virtual;
    procedure Remove(Name: String); virtual;
    destructor Destroy; override;
    property Name: String read pName write pName;
    property Data: Pointer read pData write pData;
    property Valid: Boolean read pValid write pValid;
    property Descendants[Letter: Char]: TPrefixTree read pGetDescendant write pSetDescendant; default;
  end;

  { Static Functions }

  function ValidPrefixTree(PrefixTree: TPrefixTree): Boolean;

implementation

// ************************************************************************** //
// *                      TPrefixTree implementation                        * //
// ************************************************************************** //

  function TPrefixTree.pGetDescendant(Letter: Char): TPrefixTree;
  begin
    Result := pDescendants[Ord(Letter)];
  end;

  function TPrefixTree.pJoinTrees(First, Second: TPrefixTrees): TPrefixTrees;
  var
    i, lpos: Integer;
  begin
    SetLength(Result, Length(First) + Length(Second));
    lpos := 0;
    for i := 0 to (Length(First) - 1) do
    begin
      Result[lpos] := First[i];
      lpos := lpos + 1;
    end;
    for i := 0 to (Length(Second) - 1) do
    begin
      Result[lpos] := Second[i];
      lpos := lpos + 1;
    end;
  end;

  procedure TPrefixTree.pSetDescendant(Letter: Char; Descendant: TPrefixTree);
  begin
    pDescendants[Ord(Letter)] := Descendant;
  end;

  constructor TPrefixTree.Create;
  begin
    Name := '';
    Data := nil;
    Valid := False;
  end;

  function TPrefixTree.GetAllDescendants: TPrefixTrees;
  var
    i: Char;
  begin
    if Self.Valid then
    begin
      SetLength(Result, 1);
      Result[0] := Self;
    end
    else SetLength(Result, 0);
    for i := #0 to #255 do
      if not(Self[i] = nil) then
        Result := pJoinTrees(Result, Self[i].GetAllDescendants);
  end;

  function TPrefixTree.GetDescendant(Name: String): TPrefixTree;
  var
    i: Integer;
  begin
    Result := Self;
    i := 1;
    while not(Result = nil) and not(i > Length(Name)) do
    begin
      Result := Result[Name[i]];
      i := i + 1;
    end;
  end;

  function TPrefixTree.GetDirectDescendants: TPrefixTrees;
  var
    i: Char;
  begin
    SetLength(Result, 0);
    for i := #0 to #255 do
      if not(Self[i] = nil) then
      begin
        SetLength(Result, Length(Result) + 1);
        Result[Length(Result) - 1] := Self[i];
      end;
  end;

  procedure TPrefixTree.Add(Name: String; Data: Pointer);
  var
    i: Integer;
    ltree: TPrefixTree;
  begin
    ltree := Self;
    for i := 1 to Length(Name) do
    begin
      if (ltree[Name[i]] = nil) then ltree[Name[i]] := TPrefixTree.Create;
      ltree := ltree[Name[i]];
    end;
    ltree.Name := Self.Name + Name;
    ltree.Data := Data;
    ltree.Valid := True;
  end;

  procedure TPrefixTree.Remove(Name: String);
  var
    i: Integer;
    llast, ltree: TPrefixTree;
  begin
    i := 1;
    ltree := Self;
    llast := nil;
    while not(ltree = nil) and not(i > Length(Name)) do
    begin
      if (llast = nil) then
      begin
        if (Length(ltree.GetDirectDescendants) = 1) and not ltree.Valid then
          llast := ltree;
      end
      else
        if (Length(ltree.GetDirectDescendants) > 1) or ltree.Valid then
          llast := nil;
      ltree := ltree[Name[i]];
      i := i + 1;
    end;
    if (ltree = nil) then Exit;
    ltree.Name := '';
    ltree.Data := nil;
    ltree.Valid := False;
    if (Length(ltree.GetDirectDescendants) > 0) then Exit;
    if (llast = nil) or (llast = Self) then llast := ltree;
    i := 1;
    ltree := Self;
    while not(ltree = nil) and not(i > Length(Name)) do
    begin
      if (ltree[Name[i]] = llast) then Break;
      ltree := ltree[Name[i]];
      i := i + 1;
    end;
    ltree[Name[i]] := nil;
    llast.Free;
  end;

  destructor TPrefixTree.Destroy;
  var
    i: Integer;
    ldescendants: TPrefixTrees;
  begin
    ldescendants := Self.GetDirectDescendants;
    for i := 0 to (Length(ldescendants) - 1) do
      ldescendants[i].Free;
    inherited Destroy;
  end;

// ************************************************************************** //
// *                     Static Functions implementation                    * //
// ************************************************************************** //

  function ValidPrefixTree(PrefixTree: TPrefixTree): Boolean;
  begin
    if not(PrefixTree = nil) and PrefixTree.Valid then Result := True
    else Result := False;
  end;

end.
