unit FloatEditClass;

interface

uses
  CustomDataEditClass, FunctionsClass, ResourcesClass,
  Classes, Controls, Forms, Graphics, StdCtrls;

type

  { TFloatEdit }

  TFLoatEdit = class(TCustomDataEdit)
  private
    pChange, pCancel: TButton;
    pCheckBox: TCheckBox;
    pFloatEdit, pHexEdit: TEdit;
    pFloatLabel, pHexLabel: TLabel;
    pFP: Pointer;
    pSize: Integer;
    pUserChange: Boolean;
    procedure pFloatChange(Sender: TObject); virtual;
    procedure pHexChange(Sender: TObject); virtual;
    procedure pOnShow(Sender: TObject); virtual;
  public
    constructor Create(AOwner: TComponent); override;
    function ShowBox(Input: Pointer; Size: Integer): Boolean; overload; override;
    function ShowBox(Input: Pointer; Size: Integer; var Check: Boolean): Boolean; reintroduce; overload; virtual;
  end;

implementation

// ************************************************************************** //
// *                       TFLoatEdit implementation                        * //
// ************************************************************************** //

  procedure TFLoatEdit.pFloatChange(Sender: TObject);
  begin
    if not pUserChange then Exit;
    pUserChange := False;
    CustomStrToFloat(pFloatEdit.Text, pFP, pSize);
    pHexEdit.Text := CustomDataToHex(pFP, pSize);
    pUserChange := True;
  end;

  procedure TFLoatEdit.pHexChange(Sender: TObject);
  begin
    if not pUserChange then Exit;
    pUserChange := False;
    CustomHexToData(pHexEdit.Text, pFP, pSize);
    pFloatEdit.Text := CustomFloatToStr(pFP, pSize);
    pUserChange := True;
  end;

  procedure TFLoatEdit.pOnShow(Sender: TObject);
  begin
    pUserChange := False;
    pFloatEdit.Text := CustomFloatToStr(pFP, pSize);
    pHexEdit.Text := CustomDataToHex(pFP, pSize);
    pFloatEdit.SelectAll;
    pFloatEdit.SetFocus;
    pUserChange := True;
  end;

  constructor TFLoatEdit.Create(AOwner: TComponent);
  begin
    inherited Create(AOwner);
    ClientWidth := 231;
    ClientHeight := 105;
    BorderStyle := bsDialog;
    OnShow := pOnShow;
    pChange := TButton.Create(Self);
    pFloatEdit := TEdit.Create(Self);
    with pFloatEdit do
    begin
      Parent := Self;
      Left := 41;
      Top := 7;
      Width := 186;
      Height := 20;
      OnChange := pFloatChange;
    end;
    pHexEdit := TEdit.Create(Self);
    with pHexEdit do
    begin
      Parent := Self;
      Left := 41;
      Top := 31;
      Width := 186;
      Height := 20;
      OnChange := pHexChange;
    end;
    pCheckBox := TCheckBox.Create(Self);
    with pCheckBox do
    begin
      Parent := Self;
      Caption := FL_EDIT_MODIFY_TAG;
      Left := 9;
      Top := 57;
      Width := 200;
      Height := 19;
    end;
    with pChange do
    begin
      Parent := Self;
      Caption := FL_EDIT_OK;
      Left := 77;
      Top := 81;
      Width := 72;
      Height := 20;
      ModalResult := mrOk;
      Default := True;
    end;
    pCancel := TButton.Create(Self);
    with pCancel do
    begin
      Parent := Self;
      Caption := FL_EDIT_CANCEL;
      Left := 153;
      Top := 81;
      Width := 72;
      Height := 20;
      ModalResult := mrCancel;
      Cancel := True;
    end;
    pFloatLabel := TLabel.Create(Self);
    with pFloatLabel do
    begin
      Parent := Self;
      Caption := FL_EDIT_FLOAT;
      Left := 9;
      Top := 10;
      Width := 309;
      Height := 21;
    end;
    pHexLabel := TLabel.Create(Self);
    with pHexLabel do
    begin
      Parent := Self;
      Caption := FL_EDIT_HEX;
      Left := 9;
      Top := 34;
      Width := 309;
      Height := 21;
    end;
    pUserChange := True;
  end;

  function TFLoatEdit.ShowBox(Input: Pointer; Size: Integer): Boolean;
  var
    lcheck: Boolean;
  begin
    lcheck := False;
    Result := ShowBox(Input, Size, lcheck);
  end;

  function TFLoatEdit.ShowBox(Input: Pointer; Size: Integer; var Check: Boolean
    ): Boolean;
  begin
    Result := False;
    if Check then
    begin
      ClientHeight := 105;
      pChange.Top := 81;
      pCancel.Top := 81;
      pCheckBox.Checked := True;
      pCheckBox.Visible := True;
    end
    else
    begin
      ClientHeight := 85;
      pChange.Top := 61;
      pCancel.Top := 61;
      pCheckBox.Visible := False;
    end;
    pFP := Input;
    if (Size in [4, 8, 10]) then
    begin
      pSize := Size;
      if (ShowModal = mrOk) then Result := True;
      Check := pCheckBox.Checked;
    end;
  end;

end.
