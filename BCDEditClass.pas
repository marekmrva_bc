unit BCDEditClass;

interface

uses
  CustomDataEditClass, FunctionsClass, ResourcesClass,
  Classes, Controls, Forms, Graphics, StdCtrls;

type

  { TBCDEdit }

  TBCDEdit = class(TCustomDataEdit)
  private
    pChange, pCancel: TButton;
    pBCDEdit, pHexEdit: TEdit;
    pBCDLabel, pHexLabel: TLabel;
    pBCD: Pointer;
    pSize: Integer;
    pUserChange: Boolean;
    procedure pBCDChange(Sender: TObject); virtual;
    procedure pHexChange(Sender: TObject); virtual;
    procedure pOnShow(Sender: TObject); virtual;
  public
    constructor Create(AOwner: TComponent); override;
    function ShowBox(Input: Pointer; Size: Integer): Boolean; override;
  end;

implementation

// ************************************************************************** //
// *                        TBCDEdit implementation                         * //
// ************************************************************************** //

  procedure TBCDEdit.pBCDChange(Sender: TObject);
  begin
    if not pUserChange then Exit;
    pUserChange := False;
    CustomStrToBCD(pBCDEdit.Text, pBCD, pSize);
    pHexEdit.Text := CustomDataToHex(pBCD, pSize);
    pUserChange := True;
  end;

  procedure TBCDEdit.pHexChange(Sender: TObject);
  begin
    if not pUserChange then Exit;
    pUserChange := False;
    CustomHexToData(pHexEdit.Text, pBCD, pSize);
    pBCDEdit.Text := CustomBCDToStr(pBCD, pSize);
    pUserChange := True;
  end;

  procedure TBCDEdit.pOnShow(Sender: TObject);
  begin
    pUserChange := False;
    pBCDEdit.Text := CustomBCDToStr(pBCD, pSize);
    pHexEdit.Text := CustomDataToHex(pBCD, pSize);
    pBCDEdit.SelectAll;
    pBCDEdit.SetFocus;
    pUserChange := True;
  end;

  constructor TBCDEdit.Create(AOwner: TComponent);
  begin
    inherited Create(AOwner);
    ClientWidth := 231;
    ClientHeight := 85;
    BorderStyle := bsDialog;
    OnShow := pOnShow;
    pChange := TButton.Create(Self);
    pBCDEdit := TEdit.Create(Self);
    with pBCDEdit do
    begin
      Parent := Self;
      Left := 41;
      Top := 7;
      Width := 186;
      Height := 20;
      OnChange := pBCDChange;
    end;
    pHexEdit := TEdit.Create(Self);
    with pHexEdit do
    begin
      Parent := Self;
      Left := 41;
      Top := 31;
      Width := 186;
      Height := 20;
      OnChange := pHexChange;
    end;
    with pChange do
    begin
      Parent := Self;
      Caption := FL_EDIT_OK;
      Left := 77;
      Top := 61;
      Width := 72;
      Height := 20;
      ModalResult := mrOk;
      Default := True;
    end;
    pCancel := TButton.Create(Self);
    with pCancel do
    begin
      Parent := Self;
      Caption := FL_EDIT_CANCEL;
      Left := 153;
      Top := 61;
      Width := 72;
      Height := 20;
      ModalResult := mrCancel;
      Cancel := True;
    end;
    pBCDLabel := TLabel.Create(Self);
    with pBCDLabel do
    begin
      Parent := Self;
      Caption := BC_EDIT_BCD;
      Left := 9;
      Top := 10;
      Width := 309;
      Height := 21;
    end;
    pHexLabel := TLabel.Create(Self);
    with pHexLabel do
    begin
      Parent := Self;
      Caption := BC_EDIT_HEX;
      Left := 9;
      Top := 34;
      Width := 309;
      Height := 21;
    end;
    pUserChange := True;
  end;

  function TBCDEdit.ShowBox(Input: Pointer; Size: Integer): Boolean;
  begin
    Result := False;
    if not(Size = 10) then Exit;
    pBCD := Input;
    pSize := Size;
    if (ShowModal = mrOk) then Result := True;
  end;

end.
