object ViewFPU: TViewFPU
  Left = 192
  Top = 114
  Width = 750
  Height = 500
  Caption = 'ViewFPU'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Icon.Data = {
    0000010002002020100000000000E80200002600000010101000000000002801
    00000E0300002800000020000000400000000100040000000000800200000000
    0000000000000000000000000000000000000000800000800000008080008000
    0000800080008080000080808000C0C0C0000000FF0000FF000000FFFF00FF00
    0000FF00FF00FFFF0000FFFFFF00000000000000000033007700000000000000
    0000000000003B077070000000000000000000000000BB807007000000000000
    000000000300B000700070000000000000000000330070070700070000000000
    000000003B070070007000700000000000000000BB8007000007000700000000
    00000300B0007000000070007000000000003300700700000000070007000000
    00003B070070000000000070070000000000BB80070000000000000707000000
    0300B000700FF00000000000770000003300700700FFFF000000000080330000
    3B0700700FF00FF00000000800330000BB800700FF000FF000000080BBBB0300
    B0007000FF00FF00000008000BB03300700700F00FFFF00FF000803300003B07
    00700FFF00FF00FFF00800330000BB80070000FFF00FFFFF0080BBBB0000B000
    7000000FFF00FFF008000BB00000700700000000FFF000008033000000007070
    000000000FFF000800330000000087000000000FFFFF0080BBBB000000000800
    00000000FF0008000BB000000000008000000000000080330000000000000008
    00000000000800330000000000000000800000000080BBBB0000000000000000
    0800000008000BB0000000000000000000800000803300000000000000000000
    0008000800330000000000000000000000008080BBBB00000000000000000000
    000008000BB00000000000000000FFFF33FFFFFF21FFFFFF00FFFFFB007FFFF3
    003FFFF2001FFFF0000FFFB00007FF300003FF200003FF000003FB000003F300
    0000F2000000F0000010B00000393000000F2000000F0000010F0000039F0000
    00FF000000FF000010FF800039FFC0000FFFE0000FFFF0010FFFF8039FFFFC00
    FFFFFE00FFFFFF10FFFFFFB9FFFF280000001000000020000000010004000000
    0000C00000000000000000000000000000000000000000000000000080000080
    00000080800080000000800080008080000080808000C0C0C0000000FF0000FF
    000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0000000000B37700000000
    00B0B0707000000000B3077007000000B0B0700700700000B3070000700700B0
    B0700000070700B30700F0000077B0B0700F0F0000BBB307000FF0700300B070
    0FF0FFF0BBB0070000FF070300008000007F70BBB00008000000030000000080
    0000BBB000000008000300000000000080BBB0000000FF0F0000FD070000FC03
    0000F4010000F0000000D0000000C0000000400000000003000000010000000F
    000000070000803F0000C01F0000E0FF0000F07F0000}
  Menu = pMainMenu
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Pages: TPageControl
    Left = 0
    Top = 0
    Width = 742
    Height = 446
    ActivePage = HardwarePage
    TabIndex = 0
    TabOrder = 0
    object HardwarePage: TTabSheet
      Caption = 'Hardware'
    end
    object InstructionsPage: TTabSheet
      Caption = 'Instructions'
      ImageIndex = 1
      OnShow = InstructionsPageShow
      object InstQuery: TEdit
        Left = 8
        Top = 8
        Width = 120
        Height = 21
        TabOrder = 0
        OnChange = InstQueryChange
        OnKeyDown = InstQueryKeyDown
      end
      object InstSearch: TButton
        Left = 136
        Top = 8
        Width = 50
        Height = 21
        Caption = 'Search'
        TabOrder = 1
        OnClick = InstSearchClick
      end
      object InstList: TListBox
        Left = 8
        Top = 40
        Width = 120
        Height = 120
        ItemHeight = 13
        TabOrder = 2
        OnDblClick = InstListDblClick
        OnKeyDown = InstListKeyDown
      end
      object InstDescription: TListBox
        Left = 136
        Top = 40
        Width = 120
        Height = 120
        ItemHeight = 13
        TabOrder = 3
      end
    end
  end
  object pMainMenu: TMainMenu
    Left = 24
    Top = 40
    object pFile: TMenuItem
      Caption = '&File'
      object pNew: TMenuItem
        Caption = '&New'
        ShortCut = 16462
        OnClick = pNewClick
      end
      object pDelim_0: TMenuItem
        Caption = '-'
      end
      object pOpen: TMenuItem
        Caption = '&Open'
        ShortCut = 16463
        OnClick = pOpenClick
      end
      object pSave: TMenuItem
        Caption = '&Save'
        ShortCut = 16467
        OnClick = pSaveClick
      end
      object pDelim_1: TMenuItem
        Caption = '-'
      end
      object pExit: TMenuItem
        Caption = 'E&xit'
        ShortCut = 16472
        OnClick = pExitClick
      end
    end
    object pProgram: TMenuItem
      Caption = '&Program'
      object pReset: TMenuItem
        Caption = '&Reset'
        ShortCut = 16497
        OnClick = pResetClick
      end
      object pDelim_2: TMenuItem
        Caption = '-'
      end
      object pOperand: TMenuItem
        Caption = '&Operands'
        object pOperandAdd: TMenuItem
          Caption = '&Add'
          OnClick = pOperandAddClick
        end
        object pOperandRemove: TMenuItem
          Caption = '&Remove'
          Enabled = False
        end
      end
      object pDelim_3: TMenuItem
        Caption = '-'
      end
      object pStep: TMenuItem
        Caption = '&Step'
        ShortCut = 119
        OnClick = pStepClick
      end
    end
    object pEditor: TMenuItem
      Caption = '&Editor'
      object pTheme: TMenuItem
        Caption = '&Theme'
        object pDefault: TMenuItem
          Caption = 'Default'
          Checked = True
          RadioItem = True
          OnClick = pDefaultClick
        end
        object pDark: TMenuItem
          Caption = 'Dark'
          RadioItem = True
          OnClick = pDarkClick
        end
      end
      object pLog: TMenuItem
        Caption = '&Log'
        object pClear: TMenuItem
          Caption = '&Clear'
          OnClick = pClearClick
        end
      end
    end
    object pHelp: TMenuItem
      Caption = '&Help'
      object pAbout: TMenuItem
        Caption = '&About'
        OnClick = pAboutClick
      end
    end
  end
  object pOpenDialog: TOpenDialog
    Filter = 'Assembly Files (*.asm)|*.asm|All Files|*'
    Options = [ofHideReadOnly, ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Left = 64
    Top = 40
  end
  object pSaveDialog: TSaveDialog
    Filter = 'Assembly Files (*.asm)|*.asm|All Files|*'
    Options = [ofHideReadOnly, ofPathMustExist, ofCreatePrompt, ofNoReadOnlyReturn, ofEnableSizing]
    Left = 104
    Top = 40
  end
end
