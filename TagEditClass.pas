unit TagEditClass;

interface

uses
  ConstantsClass, ResourcesClass,
  Classes, Controls, Forms, StdCtrls;

type

  { TTagEdit }

  TTagEdit = class(TForm)
  private
    pButtons: array [0..3] of TRadioButton;
    pChange, pCancel: TButton;
  public
    constructor Create(AOwner: TComponent); override;
    function ShowBox(var Input: Integer): Boolean; virtual;
  end;

implementation

// ************************************************************************** //
// *                        TTagEdit implementation                         * //
// ************************************************************************** //

  constructor TTagEdit.Create(AOwner: TComponent);
  var
    i: Integer;
  begin
    inherited CreateNew(AOwner, 0);
    Position := poDesktopCenter;
    ClientWidth := 181;
    ClientHeight := 95;
    BorderStyle := bsDialog;
    pChange := TButton.Create(Self);
    with pChange do
    begin
      Parent := Self;
      Caption := FL_EDIT_OK;
      Left := 17;
      Top := 66;
      Width := 72;
      Height := 20;
      ModalResult := mrOk;
      Default := True;
    end;
    pCancel := TButton.Create(Self);
    with pCancel do
    begin
      Parent := Self;
      Caption := FL_EDIT_CANCEL;
      Left := 93;
      Top := 66;
      Width := 72;
      Height := 20;
      ModalResult := mrCancel;
      Cancel := True;
    end;
    for i := 0 to 1 do
    begin
      pButtons[i] := TRadioButton.Create(Self);
      with pButtons[i] do
      begin
        Parent := Self;
        Caption := DESC_TAG[i];
        Left := 9;
        Top := (23 * i) + 7;
        Width := 80;
        Height := 19;
      end;
    end;
    for i := 2 to 3 do
    begin
      pButtons[i] := TRadioButton.Create(Self);
      with pButtons[i] do
      begin
        Parent := Self;
        Caption := DESC_TAG[i];
        Left := 99;
        Top := (23 * (i - 2)) + 7;
        Width := 80;
        Height := 19;
      end;
    end;
  end;

  function TTagEdit.ShowBox(var Input: Integer): Boolean;
  var
    i: Integer;
  begin
    Result := False;
    if (Input < 0) or not(Input < Length(pButtons)) then Exit;
    pButtons[Input].Checked := True;
    if (Self.ShowModal = mrOk) then
    begin
      for i := 0 to (Length(pButtons) - 1) do
        if pButtons[i].Checked then
        begin
          Input := i;
          Break;
        end;
      Result := True;
    end;
  end;

end.
