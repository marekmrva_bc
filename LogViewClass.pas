unit LogViewClass;

interface

uses
  ConstantsClass, CustomViewClass, FunctionsClass, HardwareClass,
  OperandEditClass, MemoryEditClass, PrefixTreeClass, ResourcesClass,
  TypesClass,
  Classes, Controls, Graphics, Grids, StdCtrls, Types, Menus;

type

  { TLogView }

  TLogView = class(TCustomView)
  private
    pMinWidth: Integer;
    function pGetSelectedLine: Integer; virtual;
    function pGetWidth: Integer; virtual;
    procedure pCustomDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState); virtual;
    procedure pCustomKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState); virtual;
    procedure pCustomPopupClear(Sender: TObject); virtual;
    procedure pDrawSingleCell(Canvas: TCanvas; ACol, ARow: Integer; Rect: TRect; Colors: TColors); virtual;
    procedure pOnSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean); virtual;
    procedure pSetSelectedLine(Line: Integer); virtual;
    procedure pSetWidth(Width: Integer); virtual;
  public
    constructor CreateLogView(AOwner: TComponent; Hw: THardware); virtual;
    procedure Clear(Sender: TObject); virtual;
    procedure Write(Sender: TObject; Log: String; Error: Boolean = False); virtual;
    property SelectedLine: Integer read pGetSelectedLine write pSetSelectedLine;
    property Width read pGetWidth write pSetWidth;
  end;

implementation

uses CustomStringGridClass;

// ************************************************************************** //
// *                         TLogView implementation                        * //
// ************************************************************************** //

  function TLogView.pGetSelectedLine: Integer;
  begin
    Result := Selection.Top;
  end;

  function TLogView.pGetWidth: Integer;
  begin
    Result := inherited Width;
  end;

  procedure TLogView.pCustomDrawCell(Sender: TObject; ACol, ARow: Integer;
    Rect: TRect; State: TGridDrawState);
  begin
    if (gdSelected in State) and Selected then
      pDrawSingleCell(Canvas, ACol, ARow, Rect, ColorTheme.Line.Selected)
    else
      pDrawSingleCell(Canvas, ACol, ARow, Rect, ColorTheme.Line.None)
  end;

  procedure TLogView.pCustomKeyDown(Sender: TObject; var Key: Word;
    Shift: TShiftState);
  begin
    case Key of
      KEY_DELETE:
        Clear(Sender);
    end;
  end;

  procedure TLogView.pCustomPopupClear(Sender: TObject);
  var
    lkey: Word;
  begin
    lkey := KEY_DELETE;
    pCustomKeyDown(Sender, lkey, []);
  end;

  procedure TLogView.pDrawSingleCell(Canvas: TCanvas; ACol, ARow: Integer;
    Rect: TRect; Colors: TColors);
  var
    lx, ly: Integer;
  begin
    with Rect do
    begin
      Canvas.Brush.Color := Colors.BG;
      if Changes[ACol, ARow] then Canvas.Font.Color := ColorTheme.Change
      else Canvas.Font.Color := Colors.FG;
      lx := Left + Canvas.TextWidth(' ');
      ly := (Bottom - Top - Canvas.TextHeight(Cells[ACol, ARow])) div 2 + Top;
      Canvas.FillRect(Rect);
      Canvas.TextOut(lx, ly, Cells[ACol, ARow]);
    end;
  end;

  procedure TLogView.pOnSelectCell(Sender: TObject; ACol, ARow: Integer;
    var CanSelect: Boolean);
  begin
    CanSelect := True;
  end;

  procedure TLogView.pSetSelectedLine(Line: Integer);
  var
    lselect: TGridRect;
  begin
    if (Line < 0) then Line := 0;
    if (Line > RowCount) then Line := RowCount - 1;
    {$IFDEF UNIX}
    SetColRow(0, Line);
    // Lazarus is really nice, by having incompatible calls :-(
    {$ELSE}
    lselect.Top := Line;
    lselect.Bottom := Line;
    lselect.Left := 0;
    lselect.Right := 1;
    Selection := lselect;
    {$ENDIF}
  end;

  procedure TLogView.pSetWidth(Width: Integer);
  begin
    inherited Width := Width;
    pMinWidth := MaxOf(pMinWidth, ClientWidth);
    ColWidths[0] := pMinWidth;
  end;

  constructor TLogView.CreateLogView(AOwner: TComponent; Hw: THardware);
  begin
    inherited CreateView(AOwner, Hw);
    ScrollBars := ssBoth;
    RowCount := 1;
    ColCount := 1;
    OnDrawCell := pCustomDrawCell;
    OnKeyDown := pCustomKeyDown;
    Log_Sender := Self;
    Log_OnClear := Clear;
    Log_OnWrite := Write;
    OnSelectCell := pOnSelectCell;
    PopupMenu := TPopupMenu.Create(Self);
    with PopupMenu do
    begin
      Items.Add(TMenuItem.Create(PopupMenu));
      with Items[Items.Count - 1] do
      begin
        Caption := LOG_VIEW_CLEAR;
        OnClick := pCustomPopupClear;
        Default := True;
      end;
    end;
    pMinWidth := 0;
  end;

  procedure TLogView.Clear(Sender: TObject);
  begin
    RowCount := 1;
    Selected := False;
    Cells[0, 0] := '';
  end;

  procedure TLogView.Write(Sender: TObject; Log: String; Error: Boolean = False
    );
  begin
    Log := TrimCharacterLeft(Log, ' ');
    if Error then Log := 'Error: ' + Log
    else if not(Log = '') then Log := '- ' + Log;
    Cells[0, RowCount - 1] := Log;
    Changes[0, RowCount - 1] := Error;
    pMinWidth := MaxOf(pMinWidth, Canvas.TextWidth(Log));
    ColWidths[0] := pMinWidth;
    SelectedLine := RowCount - 1;
    Selected := True;
    RowCount := RowCount + 1;
    Cells[0, RowCount - 1] := '';
  end;

end.
