unit CPUViewClass;

interface

uses
  ConstantsClass, CustomViewClass, FunctionsClass, HardwareClass, HexEditClass,
  IntEditClass, ResourcesClass, TypesClass,
  Classes, Grids, Menus;

type

  { TCPUView }

  TCPUView = class(TCustomView)
  private
    pIntEdit: TIntEdit;
    function pGetWidth: Integer; virtual;
    procedure pCustomKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState); virtual;
    procedure pOnState(Sender: TObject); virtual;
    procedure pSetWidth(Width: Integer); virtual;
  public
    constructor CreateCPUView(AOwner: TComponent; Hw: THardware); virtual;
    procedure ChangeAX; virtual;
    procedure ToggleFlag(Line: Integer); virtual;
    property Width read pGetWidth write pSetWidth;
  end;

implementation

// ************************************************************************** //
// *                         TCPUView implementation                        * //
// ************************************************************************** //

  function TCPUView.pGetWidth: Integer;
  begin
    Result := inherited Width;
  end;

  procedure TCPUView.pCustomKeyDown(Sender: TObject; var Key: Word;
    Shift: TShiftState);
  var
    lselect: TGridCoord;
  begin
    lselect := Selection.TopLeft;
    if not(lselect.X = 1) then Exit;
    case Key of
      KEY_RETURN, KEY_SPACE:
        case lselect.Y of
          0: ChangeAX;
          1..3: ToggleFlag(lselect.Y);
        end;
    end;
  end;

  procedure TCPUView.pOnState(Sender: TObject);
  var
    i: Integer;
  begin
    with Hardware.State do
    begin
      Changes[1, 0] := not(Reg_EAX = Hardware.OldState.Reg_EAX);
      Changes[0, 0] := Changes[1, 0];
      Cells[0, 0] := CPU_VIEW_AX;
      Cells[1, 0] := CustomDataToHex(@Reg_EAX, SizeOf(Word));
    end;
    for i := 0 to 2 do
    begin
      Changes[1, i + 1] := not CompareStateFlag(Hardware, i);
      Changes[0, i + 1] := Changes[1, i + 1];
      Cells[0, i + 1] := DESC_FLAG[i];
      if GetFlag(Hardware.State, i) then Cells[1, i + 1] := CPU_VIEW_TRUE
      else Cells[1, i + 1] := CPU_VIEW_FALSE;
    end;
  end;

  procedure TCPUView.pSetWidth(Width: Integer);
  begin
    inherited Width := Width;
    ColWidths[1] := ClientWidth div 2;
    ColWidths[0] := ClientWidth - ColWidths[1];
  end;

  constructor TCPUView.CreateCPUView(AOwner: TComponent; Hw: THardware);
  begin
    inherited CreateView(AOwner, Hw);
    RowCount := 4;
    ColCount := 2;
    OnKeyDown := pCustomKeyDown;
    OnState := pOnState;
    pIntEdit := TIntEdit.Create(Self);
    PopupMenu := TPopupMenu.Create(Self);
    with PopupMenu do
    begin
      Items.Add(TMenuItem.Create(PopupMenu));
      with Items[Items.Count - 1] do
      begin
        Caption := CPU_VIEW_CHANGE;
        OnClick := OnDblClick;
        Default := True;
      end;
    end;
    pOnState(Self);
  end;

  procedure TCPUView.ChangeAX;
  var
    lstate: THardwareState;
  begin
    lstate := Hardware.State;
    pIntEdit.Caption := CPU_VIEW_CHG_AX;
    if pIntEdit.ShowBox(@lstate.Reg_EAX, SizeOf(Word)) then
      Hardware.State := lstate;
  end;

  procedure TCPUView.ToggleFlag(Line: Integer);
  var
    lstate: THardwareState;
  begin
    if (Line < 1) or (Line > 3) then Exit;
    Line := Line - 1;
    lstate := Hardware.State;
    SetFlag(lstate, Line, not GetFlag(lstate, Line));
    Hardware.State := lstate;
  end;

end.
