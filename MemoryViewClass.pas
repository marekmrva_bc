unit MemoryViewClass;

interface

uses
  ConstantsClass, CustomViewClass, FunctionsClass, HardwareClass,
  OperandEditClass, MemoryEditClass, PrefixTreeClass, ResourcesClass,
  TypesClass,
  Classes, Controls, Graphics, Grids, StdCtrls, Menus;

type

  { TMemoryView }

  TMemoryView = class(TCustomView)
  private
    pOperandEdit: TOperandEdit;
    procedure pCustomKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState); virtual;
    procedure pCustomPopupAdd(Sender: TObject); virtual;
    procedure pCustomPopupRemove(Sender: TObject); virtual;
    procedure pOnSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean); virtual;
    procedure pOnOperand(Sender: TObject); virtual;
    procedure pOnState(Sender: TObject); virtual;
  public
    constructor CreateMemoryView(AOwner: TComponent; Hw: THardware); virtual;
    procedure ChangeMemory(Line: Integer); virtual;
    procedure OperandAdd; virtual;
    procedure OperandRemove(Line: Integer); virtual;
  end;

implementation

// ************************************************************************** //
// *                       TMemoryView implementation                       * //
// ************************************************************************** //

  procedure TMemoryView.pCustomKeyDown(Sender: TObject; var Key: Word;
    Shift: TShiftState);
  var
    lselect: TGridCoord;
  begin
    lselect := Selection.TopLeft;
    case Key of
      KEY_RETURN, KEY_SPACE:
        ChangeMemory(lselect.X);
      KEY_INSERT:
        OperandAdd;
      KEY_DELETE:
        OperandRemove(lselect.X);
    end;
  end;

  procedure TMemoryView.pCustomPopupAdd(Sender: TObject);
  var
    lkey: Word;
  begin
    lkey := KEY_INSERT;
    pCustomKeyDown(Sender, lkey, []);
  end;

  procedure TMemoryView.pCustomPopupRemove(Sender: TObject);
  var
    lkey: Word;
  begin
    lkey := KEY_DELETE;
    pCustomKeyDown(Sender, lkey, []);
  end;

  procedure TMemoryView.pOnSelectCell(Sender: TObject; ACol, ARow: Integer;
    var CanSelect: Boolean);
  begin
    if (ACol = 0) or not(ARow = 2) then CanSelect := False
    else CanSelect := True;
  end;

  procedure TMemoryView.pOnOperand(Sender: TObject);
  var
    i: Integer;
    ltrees: TPrefixTrees;
    loperand: POperandRecord;
  begin
    for i := 1 to (ColCount - 1) do Objects[i, 0].Free;
    ColCount := 1;
    ltrees := Hardware.Operands.GetAllDescendants;
    for i := 0 to (Length(ltrees) - 1) do
    begin
      loperand := POperandRecord(ltrees[i].Data);
      if not(loperand^.Default = '') then
      begin
        ColCount := ColCount + 1;
        Objects[ColCount - 1, 0] := TMemoryEdit.Create(Self, loperand);
      end;
    end;
    pOnState(Self);
  end;

  procedure TMemoryView.pOnState(Sender: TObject);
  var
    i, j, lwidth: Integer;
  begin
    Cells[0, 0] := MEM_VIEW_NAME;
    Cells[0, 1] := MEM_VIEW_TYPE;
    Cells[0, 2] := MEM_VIEW_VALUE;
    for i := 1 to (ColCount - 1) do
      with TMemoryEdit(Objects[i, 0]) do
      begin
        Cells[i, 0] := Name;
        for j := 0 to (Length(sOperandTypes) - 1) do
          if (Operand.OperandType = sOperandTypes[j].OperandType) then
          begin
            Cells[i, 1] := sOperandTypes[j].Description;
            Break;
          end;
        Cells[i, 2] := Value;
        with Canvas do
        begin
          lwidth := MaxOf(TextWidth(Cells[i, 0]), TextWidth(Cells[i, 1]));
          lwidth := MaxOf(lwidth, TextWidth(Cells[i, 2]));
          ColWidths[i] := lwidth + TextWidth(' ');
        end;
      end;
  end;

  constructor TMemoryView.CreateMemoryView(AOwner: TComponent; Hw: THardware);
  begin
    inherited CreateView(AOwner, Hw);
    ScrollBars := ssHorizontal;
    RowCount := 3;
    ColCount := 1;
    pOnOperand(Self);
    OnKeyDown := pCustomKeyDown;
    OnOperand := pOnOperand;
    OnSelectCell := pOnSelectCell;
    OnState := pOnState;
    pOperandEdit := TOperandEdit.CreateBox(AOwner);
    PopupMenu := TPopupMenu.Create(Self);
    with PopupMenu do
    begin
      Items.Add(TMenuItem.Create(PopupMenu));
      with Items[Items.Count - 1] do
      begin
        Caption := MEM_VIEW_EDIT;
        OnClick := OnDblClick;
        Default := True;
      end;
      Items.Add(TMenuItem.Create(PopupMenu));
      Items[Items.Count - 1].Caption := '-';
      Items.Add(TMenuItem.Create(PopupMenu));
      with Items[Items.Count - 1] do
      begin
        Caption := MEM_VIEW_ADD;
        OnClick := pCustomPopupAdd;
      end;
      Items.Add(TMenuItem.Create(PopupMenu));
      with Items[Items.Count - 1] do
      begin
        Caption := MEM_VIEW_REMOVE;
        OnClick := pCustomPopupRemove;
      end;
    end;
    pOnState(Self);
  end;

  procedure TMemoryView.ChangeMemory(Line: Integer);
  begin
    if (Line < 1) or not(Line < ColCount) then Exit;
    with TMemoryEdit(Objects[Line, 0]) do
    begin
      Edit.Caption := MEM_VIEW_EDIT + ' ' + Name;
      Change;
      pOnState(Self);
    end;
  end;

  procedure TMemoryView.OperandAdd;
  begin
    pOperandEdit.ShowBox(Hardware);
  end;

  procedure TMemoryView.OperandRemove(Line: Integer);
  begin
    if (Line < 1) or not(Line < ColCount) then Exit;
    Hardware.OperandRemove(TMemoryEdit(Objects[Line, 0]).Name);
  end;

end.
