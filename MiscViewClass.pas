unit MiscViewClass;

interface

uses
  ConstantsClass, CustomViewClass, FunctionsClass, HardwareClass, HexEditClass,
  ResourcesClass, TypesClass,
  Classes, Grids, Menus;

type

  { TMiscView }

  TMiscView = class(TCustomView)
  private
    pHexEdit: THexEdit;
    function pGetWidth: Integer; virtual;
    procedure pCustomKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState); virtual;
    procedure pOnState(Sender: TObject); virtual;
    procedure pSetWidth(Width: Integer); virtual;
  public
    constructor CreateMiscView(AOwner: TComponent; Hw: THardware); virtual;
    procedure ChangeCW; virtual;
    procedure ChangeSW; virtual;
    procedure CyclePrecision; virtual;
    procedure CycleRound; virtual;
    procedure ToggleCondition(Line: Integer); virtual;
    property Width read pGetWidth write pSetWidth;
  end;

implementation

// ************************************************************************** //
// *                        TMiscView implementation                        * //
// ************************************************************************** //

  function TMiscView.pGetWidth: Integer;
  begin
    Result := inherited Width;
  end;

  procedure TMiscView.pCustomKeyDown(Sender: TObject; var Key: Word;
    Shift: TShiftState);
  var
    lselect: TGridCoord;
  begin
    lselect := Selection.TopLeft;
    if not(lselect.X = 1) then Exit;
    case Key of
      KEY_RETURN, KEY_SPACE:
        case lselect.Y of
          0: ChangeSW;
          1: ChangeCW;
          2..5: ToggleCondition(lselect.Y);
          6: CyclePrecision;
          7: CycleRound;
        end;
    end;
  end;

  procedure TMiscView.pOnState(Sender: TObject);
  var
    i: Integer;
  begin
    with Hardware.State.FPUState do
    begin
      Changes[1, 0] := not(StatusWord = Hardware.OldState.FPUState.StatusWord);
      Changes[0, 0] := Changes[1, 0];
      Cells[0, 0] := MSC_VIEW_FSW;
      Cells[1, 0] := CustomDataToHex(@StatusWord, SizeOf(StatusWord));
      Changes[1, 1] := not(ControlWord = Hardware.OldState.FPUState.ControlWord);
      Changes[0, 1] := Changes[1, 1];
      Cells[0, 1] := MSC_VIEW_FCW;
      Cells[1, 1] := CustomDataToHex(@ControlWord, SizeOf(ControlWord));
      Changes[1, 6] := not CompareStatePrecision(Hardware);
      Changes[0, 6] := Changes[1, 6];
      Cells[0, 6] := MSC_VIEW_PREC;
      Cells[1, 6] := DESC_PREC[GetPrecision(Hardware.State)];
      Changes[1, 7] := not CompareStateRound(Hardware);
      Changes[0, 7] := Changes[1, 7];
      Cells[0, 7] := MSC_VIEW_ROUND;
      Cells[1, 7] := DESC_ROUN[GetRound(Hardware.State)];
    end;
    for i := 0 to 3 do
    begin
      Changes[1, i + 2] := not CompareStateCondition(Hardware, i);
      Changes[0, i + 2] := Changes[1, i + 2];
      Cells[0, i + 2] := DESC_COND[i];
      if GetCondition(Hardware.State, i) then Cells[1, i + 2] := MSC_VIEW_TRUE
      else Cells[1, i + 2] := MSC_VIEW_FALSE;
    end;
  end;

  procedure TMiscView.pSetWidth(Width: Integer);
  begin
    inherited Width := Width;
    ColWidths[1] := ClientWidth div 2;
    ColWidths[0] := ClientWidth - ColWidths[1];
  end;

  constructor TMiscView.CreateMiscView(AOwner: TComponent; Hw: THardware);
  begin
    inherited CreateView(AOwner, Hw);
    RowCount := 8;
    ColCount := 2;
    OnKeyDown := pCustomKeyDown;
    OnState := pOnState;
    pHexEdit := THexEdit.Create(Self);
    PopupMenu := TPopupMenu.Create(Self);
    with PopupMenu do
    begin
      Items.Add(TMenuItem.Create(PopupMenu));
      with Items[Items.Count - 1] do
      begin
        Caption := MSC_VIEW_CHANGE;
        OnClick := OnDblClick;
        Default := True;
      end;
    end;
    pOnState(Self);
  end;

  procedure TMiscView.ChangeCW;
  var
    lstate: THardwareState;
  begin
    lstate := Hardware.State;
    pHexEdit.Caption := MSC_VIEW_CHG_CW;
    with lstate.FPUState do
      if pHexEdit.ShowBox(@ControlWord, SizeOf(ControlWord)) then
        Hardware.State := lstate;
  end;

  procedure TMiscView.ChangeSW;
  var
    lstate: THardwareState;
  begin
    lstate := Hardware.State;
    pHexEdit.Caption := MSC_VIEW_CHG_SW;
    with lstate.FPUState do
      if pHexEdit.ShowBox(@StatusWord, SizeOf(StatusWord)) then
        Hardware.State := lstate;
  end;

  procedure TMiscView.CyclePrecision;
  var
    lstate: THardwareState;
  begin
    lstate := Hardware.State;
    SetPrecision(lstate, (GetPrecision(Hardware.State) + 1) mod 4);
    Hardware.State := lstate;
  end;

  procedure TMiscView.CycleRound;
  var
    lstate: THardwareState;
  begin
    lstate := Hardware.State;
    SetRound(lstate, (GetRound(Hardware.State) + 1) mod 4);
    Hardware.State := lstate;
  end;

  procedure TMiscView.ToggleCondition(Line: Integer);
  var
    lstate: THardwareState;
  begin
    if (Line < 2) or (Line > 5) then Exit;
    Line := Line - 2;
    lstate := Hardware.State;
    SetCondition(lstate, Line, not GetCondition(lstate, Line));
    Hardware.State := lstate;
  end;

end.
