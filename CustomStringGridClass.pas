unit CustomStringGridClass;

interface

uses
  ResourcesClass,
  Classes, Grids;

type

  { TCustomStringGrid }

  TCustomStringGrid = class(TStringGrid)
  private
    pSelected: Boolean;
    procedure pOnEnter(Sender: TObject); virtual;
    procedure pOnExit(Sender: TObject); virtual;
  protected
    property Selected: Boolean read pSelected write pSelected;
  public
    constructor Create(AOwner: TComponent); override;
  end;

implementation

// ************************************************************************** //
// *                    TCustomStringGrid implementation                    * //
// ************************************************************************** //

  procedure TCustomStringGrid.pOnEnter(Sender: TObject);
  begin
    pSelected := True;
  end;

  procedure TCustomStringGrid.pOnExit(Sender: TObject);
  begin
    pSelected := False;
  end;

 constructor TCustomStringGrid.Create(AOwner: TComponent);
  begin
    inherited Create(AOwner);
    DoubleBuffered := True;
    FixedCols := 0;
    FixedRows := 0;
    OnEnter := pOnEnter;
    OnExit := pOnExit;
    Canvas.Font.Name := FONT_DEFAULT_NAME;
    Canvas.Font.Size := FONT_DEFAULT_SIZE;
    DefaultRowHeight := FONT_DEFAULT_HEIGHT;
  end;

end.
