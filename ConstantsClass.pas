unit ConstantsClass;

interface

uses
  TypesClass;

const
  APP_VERSION = 'ViewFPU v1.0.0';

  FIO_COMMENT = '#';
  FIO_SYSTEM = 'SYSTEM';
  FIO_DATA = 'DATA';
  FIO_CODE = 'CODE';

  OPER_SEP = ':';

  FPU_SPACE = '.';
  FPU_OPERAND_ERROR = '!';
  FPU_OPERAND_NONE = '*';
  FPU_OPERAND_ST_0 = '+';
  FPU_OPERAND_ST_I = '-';
  FPU_OPERAND_AX = '0';
  FPU_OPERAND_M16_INT = '2';
  FPU_OPERAND_M32_INT = '4';
  FPU_OPERAND_M64_INT = '8';
  FPU_OPERAND_ADDR = '@';
  FPU_OPERAND_M80_BCD = 'B';
  FPU_OPERAND_M32_FP = 'C';
  FPU_OPERAND_M64_FP = 'D';
  FPU_OPERAND_M80_FP = 'E';
  FPU_OPERAND_M2BYTE_CW = 'c';
  FPU_OPERAND_M2BYTE_SW = 'd';
  FPU_OPERAND_M14BYTE_ENV = 'e';
  FPU_OPERAND_M28BYTE_ENV = 'f';
  FPU_OPERAND_M94BYTE_STAT = 'g';
  FPU_OPERAND_M108BYTE_STAT = 'h';

  FP_ER = FPU_SPACE + FPU_OPERAND_ERROR;
  FP_X = FPU_SPACE + FPU_OPERAND_NONE;
  FP_T = FPU_SPACE + FPU_OPERAND_ST_0;
  FP_I = FPU_SPACE + FPU_OPERAND_ST_I;
  FP_2 = FPU_SPACE + FPU_OPERAND_M16_INT;
  FP_4 = FPU_SPACE + FPU_OPERAND_M32_INT;
  FP_8 = FPU_SPACE + FPU_OPERAND_M64_INT;
  FP_ADDR = FPU_SPACE + FPU_OPERAND_ADDR;
  FP_0 = FPU_SPACE + FPU_OPERAND_M80_BCD;
  FP_S = FPU_SPACE + FPU_OPERAND_M32_FP;
  FP_D = FPU_SPACE + FPU_OPERAND_M64_FP;
  FP_E = FPU_SPACE + FPU_OPERAND_M80_FP;
  FP_C = FPU_SPACE + FPU_OPERAND_M2BYTE_CW;
  FP_W = FPU_SPACE + FPU_OPERAND_M2BYTE_SW;
  FP_14 = FPU_SPACE + FPU_OPERAND_M14BYTE_ENV;
  FP_28 = FPU_SPACE + FPU_OPERAND_M28BYTE_ENV;
  FP_94 = FPU_SPACE + FPU_OPERAND_M94BYTE_STAT;
  FP_108 = FPU_SPACE + FPU_OPERAND_M108BYTE_STAT;
  FP_AX = FPU_SPACE + FPU_OPERAND_AX;

  SEP_FP_4: TSeparateConstants = (
    ExponCmp: $FF;
    SigniCmp: 0;
    SigniAnd: $400000;
  );

  SEP_FP_8: TSeparateConstants = (
    ExponCmp: $7FF;
    SigniCmp: 0;
    SigniAnd: $8000000000000;
  );

  SEP_FP_10: TSeparateConstants = (
    ExponCmp: $7FFF;
    SigniCmp: $8000000000000000;
    SigniAnd: $4000000000000000;
  );

  DESC_NAN = 'NAN';
  DESC_NAN_Q = 'Q';
  DESC_NAN_S = 'S';
  DESC_NEG = '-';
  DESC_POS = '+';
  DESC_INF = 'INF';
  DESC_NEG_INF = DESC_NEG + DESC_INF;
  DESC_POS_INF = DESC_POS + DESC_INF;
  DESC_QNAN = DESC_NAN_Q + DESC_NAN;
  DESC_SNAN = DESC_NAN_S + DESC_NAN;

  TRANS_FP_4: TTranslateConstants = (
    PosInf: #$00#$00#$80#$7F;
    NegInf: #$00#$00#$80#$FF;
    QNaN: #$00#$00#$C0#$7F;
    SNaN: #$01#$00#$80#$7F;
  );

  TRANS_FP_8: TTranslateConstants = (
    PosInf: #$00#$00#$00#$00#$00#$00#$F0#$7F;
    NegInf: #$00#$00#$00#$00#$00#$00#$F0#$FF;
    QNaN: #$00#$00#$00#$00#$00#$00#$F8#$7F;
    SNaN: #$01#$00#$00#$00#$00#$00#$F0#$7F;
  );

  TRANS_FP_10: TTranslateConstants = (
    PosInf: #$00#$00#$00#$00#$00#$00#$00#$80#$FF#$7F;
    NegInf: #$00#$00#$00#$00#$00#$00#$00#$80#$FF#$FF;
    QNaN: #$00#$00#$00#$00#$00#$00#$00#$C0#$FF#$7F;
    SNaN: #$01#$00#$00#$00#$00#$00#$00#$80#$FF#$7F;
  );

  STEP_FIRST = 0;
  STEP_LAST = MaxInt;

  BRANCH_NORMAL = 'N';
  BRANCH_BRANCH = 'B';
  BRANCH_UNSUPPORTED = '$';

  CHARS_CONTROL = [FPU_SPACE, '<', '>'];
  CHARS_ADDRESS = ['0'..'9', '+', '-'];
  CHARS_HEXA: array [0..15] of Char = ('0', '1', '2', '3', '4', '5', '6', '7',
                                       '8', '9', 'A', 'B', 'C', 'D', 'E', 'F');

  INST_OPCODE_RET: Char = #$C3;

  INTYPE_DESCRIPTION = 0;
  INTYPE_INSTRUCTION = 1;
  INTYPE_PREFIX = 2;

  KEY_BACKSPACE = 8;
  KEY_RETURN = 13;
  KEY_SPACE = 32;
  KEY_END = 35;
  KEY_HOME = 36;
  KEY_LEFT = 37;
  KEY_UP = 38;
  KEY_RIGHT = 39;
  KEY_DOWN = 40;
  KEY_INSERT = 45;
  KEY_DELETE = 46;
  KEY_ASTERISK = 106;
  KEY_ASTERISK_SFT = 56;
  KEY_F2 = 113;
  KEY_F8 = 119;

  CONST_PADDING = 4;

implementation

end.
